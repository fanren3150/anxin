//自动定位
$(document).ready(function(){
    var citysearch = new AMap.CitySearch();
    citysearch.getLocalCity(function(status, result) {
        if (status === 'complete' && result.info === 'OK') {
            if (result && result.city && result.bounds) {
                var cityinfo = result.city;
                var citybounds = result.bounds;
                $('.head-city span').text(result.city);
                var lat=result.bounds.eb.lat;
                var lng=result.bounds.eb.lng;
                // console.log(result);
                // console.log(lat+","+lng);
            }
        } else {
            alert("定位失败");
        }
    });
});

//滚动消息
function timer(opj){
    $(opj).animate({
        marginTop : "-57px"
    },500,function(){
        $(this).css({marginTop : "0"}).find("li:first").appendTo(this);
    })
}
$(function(){
    var num = $('.newest-content').find('li').length;
    if(num > 1){
        var time=setInterval('timer(".newest-content")',3500);
        $('.newest-content li').mousemove(function(){
            clearInterval(time);
        }).mouseout(function(){
            time = setInterval('timer(".newest-content")',3500);
        });
    }
});