<?php include template('header','admin');?>
<body>
	<div class="fixed-nav layout">
		<ul>
			<li class="first">学校设置</li>
			<li class="spacer-gray"></li>
		</ul>
		<div class="hr-gray"></div>
	</div>
	
	<div class="content padding-big have-fixed-nav">
		<form action="" method="POST" enctype="multipart/form-data">
		<div class="form-box clearfix" id="form">
			<?php echo form::input('text', 'year', $info['year'], '年份：', '设置年份。',array('validate' => 'required')); ?>
			<?php foreach($info['alldays'] as $k => $v){?>
			<?php echo form::input('text', 'alldays['.$k.']', $v, $days[$k].'：', '',array('validate' => 'required')); ?>
			<?php }?>
		</div>
		<div class="padding">
			<input type="hidden" name="id" value="<?php echo $info['id']?>">
			<input type="submit" name="dosubmit" class="button bg-main" value="确定" />
			<input type="button" class="button margin-left bg-gray" value="返回" />
		</div>
		</form>
	</div>
	<script type="text/javascript">
		$(window).otherEvent();
		$(function(){
			var $val=$("input[type=text]").first().val();
			$("input[type=text]").first().focus().val($val);
		})
	</script>
<?php include template('footer','admin');?>
