<?php include template('header','admin');?>
<body>
<div class="fixed-nav layout">
	<ul>
		<li class="first">班级设置</li>
		<li class="spacer-gray"></li>
	</ul>
	<div class="hr-gray"></div>
</div>

<div class="content padding-big have-fixed-nav">
	<form action="" method="POST" enctype="multipart/form-data">
	<div class="form-box clearfix" id="form">
		<div class="form-group" style="z-index: 2;">
			<span class="label">所在学校：</span>
			<div class="box" style="width: 256px;">
				<div class="form-select-edit select-search-text-box">
					<div class="form-buttonedit-popup">
						<input class="input" type="text" value="<?php echo $info['school_name'] ?>" id="_school" readonly data-reset="false">
						<span class="ico_buttonedit"></span>
						<input type="hidden" name="school_id" value="<?php echo $info['school_id'] ?>" data-reset="false">
					</div>
					<div class="select-search-field border border-main">
						<input class="input border-none" autocomplete="off" type="text" id="schoolname" value="" placeholder="请输入学校名称" data-reset="false" />
						<i class="ico_search"></i>
					</div>
					<div class="listbox-items brand-list school-list">
						<?php foreach ($schools AS $school) { ?>
							<span class="listbox-item" data-val="<?php echo $school['id'] ?>" id="school"><?php echo $school['name'] ?></span>
						<?php } ?>
					</div>
				</div>
			</div>
			<p class="desc">为班级关联所在学校</p>
		</div>
		<?php echo form::input('text', 'name', $info['name'], '班级名称：', '请填写班级的名称；例如：一年级一班；二年级一班等。',array('validate' => 'required')); ?>
                <?php echo form::input('file','logo', $info['logo'], '班级标识：','请上传班级LOGO，用于前台展示。',array('preview'=>$info['logo'])); ?>
		<div class="form-group" style="z-index: 2;">
			<span class="label">老师：</span>
			<div class="box" style="width: 256px;">
				<div class="form-select-edit select-search-text-box">
					<div class="form-buttonedit-popup">
						<input class="input" type="text" value="<?php echo $info['teacher1_name'] ?>" id="_teacher1" readonly data-reset="false">
						<span class="ico_buttonedit"></span>
						<input type="hidden" name="teacher1_id" value="<?php echo $info['teacher1_id'] ?>" data-reset="false">
					</div>
					<div class="select-search-field border border-main">
						<input class="input border-none" autocomplete="off" type="text" id="teacher1name" value="" placeholder="请输入老师名称" data-reset="false" />
						<i class="ico_search"></i>
					</div>
					<div class="listbox-items brand-list teacher1-list">
						<?php foreach ($members AS $member) { ?>
							<span class="listbox-item" data-val="<?php echo $member['id'] ?>" id="teacher1"><?php echo $member['username'] ?></span>
						<?php } ?>
					</div>
				</div>
			</div>
			<p class="desc">为班级关联老师</p>
		</div>
		
		<div class="form-group" style="z-index: 2;">
			<span class="label">副老师：</span>
			<div class="box" style="width: 256px;">
				<div class="form-select-edit select-search-text-box">
					<div class="form-buttonedit-popup">
						<input class="input" type="text" value="<?php echo $info['teacher2_name'] ?>" id="_teacher2" readonly data-reset="false">
						<span class="ico_buttonedit"></span>
						<input type="hidden" name="teacher2_id" value="<?php echo $info['teacher2_id'] ?>" data-reset="false">
					</div>
					<div class="select-search-field border border-main">
						<input class="input border-none" autocomplete="off" type="text" id="teacher2name" value="" placeholder="请输入老师名称" data-reset="false" />
						<i class="ico_search"></i>
					</div>
					<div class="listbox-items brand-list teacher2-list">
						<?php foreach ($members AS $member) { ?>
							<span class="listbox-item" data-val="<?php echo $member['id'] ?>" id="teacher2"><?php echo $member['username'] ?></span>
						<?php } ?>
					</div>
				</div>
			</div>
			<p class="desc">为班级关联副老师</p>
		</div>
	</div>
	<div class="padding">
		<input type="hidden" name="id" value="<?php echo $info['id']?>">
		<input type="submit" name="dosubmit" class="button bg-main" value="确定" />
		<input type="button" class="button margin-left bg-gray" value="返回" />
	</div>
	</form>
</div>
<script type="text/javascript">
$(window).otherEvent();
$('.select-search-field').click(function (e) {
	e.stopPropagation();
});
$('.select-search-text-box .form-buttonedit-popup').click(function () {
	if (!$(this).hasClass('buttonedit-popup-hover')) {
		$(this).parent().find('.select-search-field').show();
		$(this).parent().find('.select-search-field').children('.input').focus();
		$(this).parent().find('.listbox-items').show();
	} else {
		$(this).parent().find('.select-search-field').hide();
		$(this).parent().find('.listbox-items').hide();
	}
});

$('#schoolname').live('keyup', function () {
	var schoolname = this.value;
	var _this = $(this);
	$.post("<?php echo url('ajax_school') ?>", {schoolname: schoolname}, function (data) {
		_this.parents('.brand-list').children('.listbox-item').remove();
		_this.parents('.brand-list').show();
		if (data.status == 1) {
			var html = '';
			$.each(data.result, function (i, item) {
				html += '<span class="listbox-item" data-val="' + i + '" id="school">' + item + '</span>';
			})
			_this.parents('.brand-list').append(html);
		} else {
			var html = '<span class="listbox-item">未搜索到结果</span>';
			_this.parents('.brand-list').append(html);
		}
	}, 'json');
});

$('#teacher1name').live('keyup', function () {
	var schoolname = this.value;
	var _this = $(this);
	$.post("<?php echo url('ajax_school') ?>", {schoolname: schoolname}, function (data) {
		$('.teacher1-list').children('.listbox-item').remove();
		$('.teacher1-list').show();
		if (data.status == 1) {
			var html = '';
			$.each(data.result, function (i, item) {
				html += '<span class="listbox-item" data-val="' + i + '" id="teacher1">' + item + '</span>';
			})
			$('.teacher1-list').append(html);
		} else {
			var html = '<span class="listbox-item" id="teacher1">未搜索到结果</span>';
			$('.teacher1-list').append(html);
		}
	}, 'json');
});

$('#teacher2name').live('keyup', function () {
	var schoolname = this.value;
	var _this = $(this);
	$.post("<?php echo url('ajax_school') ?>", {schoolname: schoolname}, function (data) {
		$('.teacher2-list').children('.listbox-item').remove();
		$('.teacher2-list').show();
		if (data.status == 1) {
			var html = '';
			$.each(data.result, function (i, item) {
				html += '<span class="listbox-item" data-val="' + i + '" id="teacher2">' + item + '</span>';
			})
			$('.teacher2-list').append(html);
		} else {
			var html = '<span class="listbox-item" id="teacher2">未搜索到结果</span>';
			$('.teacher2-list').append(html);
		}
	}, 'json');
});

$(".select-search-text-box .listbox-items .listbox-item").live('click', function () {
	$(this).parent().prev('.select-search-field').children('.input').val();
	$(this).parent().prev('.select-search-field').hide();
	if($(this).attr('id') == "school"){
		$('input[name="school_id').val($(this).attr('data-val'));
		$('#_school').val($(this).html());
	}
	if($(this).attr('id') == "teacher1"){
		$('input[name="teacher1_id').val($(this).attr('data-val'));
		$('#_teacher1').val($(this).html());
	}
	
	if($(this).attr('id') == "teacher2"){
		$('input[name="teacher2_id').val($(this).attr('data-val'));
		$('#_teacher2').val($(this).html());
	}	
});
$(function(){
	var $val=$("input[type=text]").first().val();
	$("input[type=text]").first().focus().val($val);
})
</script>
<?php include template('footer','admin');?>
