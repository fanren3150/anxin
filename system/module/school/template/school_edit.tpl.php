<?php include template('header','admin');?>
<body>
	<div class="fixed-nav layout">
		<ul>
			<li class="first">学校设置</li>
			<li class="spacer-gray"></li>
		</ul>
		<div class="hr-gray"></div>
	</div>
	
	<div class="content padding-big have-fixed-nav">
		<form action="" method="POST" enctype="multipart/form-data">
		<div class="form-box clearfix" id="form">
			<?php echo form::input('text', 'name', $info['name'], '学校名称：', '请填写学校的名称；例如：实验小学；实验一小等。',array('validate' => 'required')); ?>
			<?php echo form::input('text', 'area', $info['area'], '区域：', '请填写学校所在区域；例如：罗湖区；福田区等。',array('validate' => 'required')); ?>
			<?php echo form::input('text','months_price', $info['months_price'], '收费（天计）：','设置托班一天收费标准。',array('validate'=> 'required')); ?>
                        <?php echo form::input('text','price', $info['price'], '请假退款（天计）：','用于孩子请假时返还的金额。',array('validate'=> 'required')); ?>
                        <?php echo form::input('text','year_price', $info['year_price'], '邀请有礼：','用户家长邀请成功后获取的奖励。',array('validate'=> 'required')); ?>
			<?php echo form::input('file','logo', $info['logo'], '学校标识：','请上传学校LOGO，用于前台展示。',array('preview'=>$info['logo'])); ?>
			<?php echo form::input('text', 'sort', $info['sort'] ? $info['sort'] : 100, '学校排序：', '请填写自然数。学校列表将会根据排序进行由小到大排列显示。'); ?>
		</div>
		<div class="padding">
			<input type="hidden" name="id" value="<?php echo $info['id']?>">
			<input type="submit" name="dosubmit" class="button bg-main" value="确定" />
			<input type="button" class="button margin-left bg-gray" value="返回" />
		</div>
		</form>
	</div>
	<script type="text/javascript">
		$(window).otherEvent();
		$(function(){
			var $val=$("input[type=text]").first().val();
			$("input[type=text]").first().focus().val($val);
		})
	</script>
<?php include template('footer','admin');?>
