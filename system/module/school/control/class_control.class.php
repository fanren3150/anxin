<?php
hd_core::load_class('init', 'admin');
class class_control extends init_control {
	protected $service = '';
	protected $brand;
	public function _initialize() {
		parent::_initialize();
		$this->service = $this->load->service('school/class');
		$this->schoolService = $this->load->service('school/school');
		$this->attachment_service = $this->load->service('attachment/attachment');
		helper('attachment');
	}
	
	public function index(){
		$sqlmap = $info = array();
		$limit = (isset($_GET['limit']) && is_numeric($_GET['limit'])) ? $_GET['limit'] : 20;
		if($_GET['school_id'] >0){
			$sqlmap['school_id'] = $_GET['school_id'];
		}
		$keyword = $_GET['keyword'];
		if (isset($keyword) && !empty($keyword)) {
			$sids = $this->load->table('school/school')->where(array('name' => array('LIKE','%'.$keyword.'%')))->getField('id',TRUE);
			if($sids){
				$sqlmap['school_id'] = array('IN',$sids);
			}
			
			if (!$sids) {
				$sqlmap['name'] = array('LIKE','%'.$keyword.'%');
			}
		}
		
		$brands = $this->service->get_lists($_GET['page'],$limit,$sqlmap);
		$count = $this->service->count($sqlmap);
		$pages = $this->admin_pages($count, $limit);
		$lists = array(
			'th' => array('name' => array('title' => '午托班位置名称','length' => 15,'style' => 'ident'),'school_name' => array('title' => '学校名','length' => 20),'student_number' => array('title' => '学生人数','length' => 15),'teacher1_name' => array('length' => 15,'title' => '老师'),'teacher2_name' => array('length' => 15,'title' => '副老师')),
			'lists' => $brands,
			'pages' => $pages,
		);
		$this->load->librarys('View')->assign('lists',$lists)->display('class_list');
	}


	public function add(){
		if(checksubmit('dosubmit')) {
			if(!empty($_FILES['logo']['name'])) {
				$code = attachment_init(array('path' => 'school','mid' => 1,'allow_exts' => array('gif','jpg','jpeg','bmp','png')));
				$_GET['logo'] = $this->attachment_service->setConfig($code)->upload('logo');
				if(!$_GET['logo']){
					showmessage($this->attachment_service->error);
				}
			}
			$result = $this->service->add_class($_GET);
			if(!$result){
				showmessage($this->service->error);
			}else{
				$this->attachment_service->attachment($_GET['logo'],'',false);
				showmessage(lang('_operation_success_'),url('index'));
			}
		}else{
			$schools = $this->schoolService->get_lists();
			$members = $this->load->service('member/member')->get_lists(array('group_id'=>2));
			$this->load->librarys('View')->assign('schools',$schools)->assign('members',$members)->display('class_edit');
		}
	}
	
	public function edit(){
		$info = $this->service->get_class_by_id($_GET['id']);
		if(checksubmit('dosubmit')) {
			if(!empty($_FILES['logo']['name'])) {
				$code = attachment_init(array('path' => 'goods','mid' => 1,'allow_exts' => array('gif','jpg','jpeg','bmp','png')));
				$_GET['logo'] = $this->attachment_service->setConfig($code)->upload('logo');
				if(!$_GET['logo']){
					showmessage($this->attachment_service->error);
				}
			}
			$result = $this->service->edit_class($_GET);
			if($result === FALSE){
				showmessage($this->service->error);
			}else{
				$this->attachment_service->attachment($_GET['logo'], $info['logo'],false);
				showmessage(lang('_operation_success_'),url('index'));
			}
		}else{
			$info['school_name'] = $this->load->table('school/school')->where(array('id'=>$info['school_id']))->getField('name');
			$info['teacher1_name'] = $this->load->table('member/member')->where(array('id'=>$info['teacher1_id']))->getField('username');
			$info['teacher2_name'] = $this->load->table('member/member')->where(array('id'=>$info['teacher2_id']))->getField('username');
			$schools = $this->schoolService->get_lists();
			$members = $this->load->service('member/member')->get_lists(array('group_id'=>2));
			$this->load->librarys('View')->assign('info',$info)->assign('schools',$schools)->assign('members',$members)->display('class_edit');
		}
	}
	
	/**
	 * [ajax_del 删除品牌，可批量删除]
	 */
	public function ajax_del(){
		$result = $this->service->delete_class($_GET['id']);
		if(!$result){
			showmessage($this->service->error);
		}else{
			showmessage(lang('_operation_success_'),url('index'),1);
		}
	}
	
	public function ajax_name(){
		$result = $this->service->change_name($_GET);
		if(!$result){
			showmessage($this->service->error,'',0,'','json');
		}else{
			showmessage(lang('_operation_success_'),'',1,'','json');
		}
	}
	
	public function ajax_sort(){
		$result = $this->service->change_sort($_GET);
		if(!$result){
			showmessage($this->service->error,'',0,'','json');
		}else{
			showmessage(lang('_operation_success_'),'',1,'','json');
		}
	}
	
	public function ajax_school(){
		$result = $this->schoolService->ajax_school($_GET['schoolname']);
		if(!$result){
			showmessage($this->spu_service->error,'',0,'','json');
		}else{
			showmessage(lang('_operation_success_'),'',1,$result,'json');
		}
	}
}