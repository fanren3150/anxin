<?php
hd_core::load_class('init', 'admin');
class admin_control extends init_control {
	protected $service = '';
	protected $brand;
	public function _initialize() {
		parent::_initialize();
		$this->service = $this->load->service('school/school');
		$this->attachment_service = $this->load->service('attachment/attachment');
		helper('attachment');
	}
	
	public function index(){
		$sqlmap = $info = array();
		$sqlmap['name|area'] = array("LIKE", '%'.$_GET['keyword'].'%');
		$limit = (isset($_GET['limit']) && is_numeric($_GET['limit'])) ? $_GET['limit'] : 20;
		$brands = $this->service->get_lists($_GET['page'],$limit,$sqlmap);
		$count = $this->service->count($sqlmap);
		$pages = $this->admin_pages($count, $limit);
		$lists = array(
			'th' => array('sort' => array('style' => 'double_click','length' => 10,'title' => '排序'),'name' => array('title' => '学校名称','length' => 20,'style' => 'ident'),'area' => array('title' => '所在区域','length' => 15),'months_price' => array('title' => '收费（天计）','length' => 10),'price' => array('title' => '请假退款（天计）','length' => 15),'year_price' => array('title' => '邀请有礼','length' => 10)),
			'lists' => $brands,
			'pages' => $pages,
		);
		$this->load->librarys('View')->assign('lists',$lists)->display('school_list');
	}


	public function add(){
		if(checksubmit('dosubmit')) {
			if(!empty($_FILES['logo']['name'])) {
				$code = attachment_init(array('path' => 'school','mid' => 1,'allow_exts' => array('gif','jpg','jpeg','bmp','png')));
				$_GET['logo'] = $this->attachment_service->setConfig($code)->upload('logo');
				if(!$_GET['logo']){
					showmessage($this->attachment_service->error);
				}
			}
			$result = $this->service->add_school($_GET);
			if(!$result){
				showmessage($this->service->error);
			}else{
				$this->attachment_service->attachment($_GET['logo'],'',false);
				showmessage(lang('_operation_success_'),url('index'));
			}
		}else{
			$this->load->librarys('View')->display('school_edit');
		}
	}
	
	public function edit(){
		$info = $this->service->get_school_by_id($_GET['id']);
		if(checksubmit('dosubmit')) {
			if(!empty($_FILES['logo']['name'])) {
				$code = attachment_init(array('path' => 'goods','mid' => 1,'allow_exts' => array('gif','jpg','jpeg','bmp','png')));
				$_GET['logo'] = $this->attachment_service->setConfig($code)->upload('logo');
				if(!$_GET['logo']){
					showmessage($this->attachment_service->error);
				}
			}
			$result = $this->service->edit_school($_GET);
			if($result === FALSE){
				showmessage($this->service->error);
			}else{
				$this->attachment_service->attachment($_GET['logo'], $info['logo'],false);
				showmessage(lang('_operation_success_'),url('index'));
			}
		}else{
			$this->load->librarys('View')->assign('info',$info)->display('school_edit');
		}
	}
	
	/**
	 * [ajax_del 删除品牌，可批量删除]
	 */
	public function ajax_del(){
		$result = $this->service->delete_school($_GET['id']);
		if(!$result){
			showmessage($this->service->error);
		}else{
			showmessage(lang('_operation_success_'),url('index'),1);
		}
	}
	
	public function ajax_name(){
		$result = $this->service->change_name($_GET);
		if(!$result){
			showmessage($this->service->error,'',0,'','json');
		}else{
			showmessage(lang('_operation_success_'),'',1,'','json');
		}
	}
	
	public function ajax_sort(){
		$result = $this->service->change_sort($_GET);
		if(!$result){
			showmessage($this->service->error,'',0,'','json');
		}else{
			showmessage(lang('_operation_success_'),'',1,'','json');
		}
	}
}