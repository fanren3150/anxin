<?php
hd_core::load_class('init', 'admin');
class days_control extends init_control {
	protected $service = '';
	protected $brand;
	public function _initialize() {
		parent::_initialize();
		$this->service = $this->load->service('school/school');
		$this->attachment_service = $this->load->service('attachment/attachment');
		helper('attachment');
	}
	
	public function index(){
		$sqlmap = $info = array();
		$limit = (isset($_GET['limit']) && is_numeric($_GET['limit'])) ? $_GET['limit'] : 20;
		$brands = model('holidays')->select();
		$lists = array(
			'th' => array('yaer' => array('title' => '年份','length' => 80)),
			'lists' => $brands,
			'pages' => $pages,
		);
		$this->load->librarys('View')->assign('lists',$lists)->display('days_list');
	}


	public function add(){
		if(checksubmit('dosubmit')) {
			
			$_GET['alldays'] = serialize($_GET['alldays']);
			$result = model('holidays')->update($_GET);
			if($result === FALSE){
				showmessage('设置失败');
			}else{
				showmessage(lang('_operation_success_'),url('index'));
			}
		}else{
			$info['alldays'] = array('1'=>'','2'=>'','3'=>'','4'=>'','5'=>'','6'=>'','7'=>'','8'=>'','9'=>'','10'=>'','11'=>'','12'=>'','up'=>'','next'=>'');
			
			$days = array('1'=>'一月假期(输入格式如:1,12,28)','2'=>'二月假期(输入格式如:1,12,28)','3'=>'三月假期(输入格式如:1,12,28)','4'=>'四月假期(输入格式如:1,12,28)','5'=>'五月假期(输入格式如:1,12,28)','6'=>'六月假期(输入格式如:1,12,28)','7'=>'七月假期(输入格式如:1,12,28)','8'=>'八月假期(输入格式如:1,12,28)','9'=>'九月假期(输入格式如:1,12,28)','10'=>'十月假期(输入格式如:1,12,28)','11'=>'十一月假期(输入格式如:1,12,28)','12'=>'十二月假期(输入格式如:1,12,28)','up'=>'上半学期开始/结束时间(输入格式如：2017-2-15|2017-6-30)','next'=>'下半学期开始/结束时间(输入格式如：2017-9-1|2018-1-15)');
			$this->load->librarys('View')->assign('info',$info)->assign('days',$days)->display('days_edit');
		}
	}
	
	public function edit(){
		$info = model('holidays')->find($_GET['id']);
		if(checksubmit('dosubmit')) {
			$_GET['alldays'] = serialize($_GET['alldays']);
			$result = model('holidays')->update($_GET);
			if($result === FALSE){
				showmessage('设置失败');
			}else{
				showmessage(lang('_operation_success_'),url('index'));
			}
		}else{
			$info['alldays'] = unserialize($info['alldays']);
			$days = array('1'=>'一月假期(输入格式如:1,12,28)','2'=>'二月假期(输入格式如:1,12,28)','3'=>'三月假期(输入格式如:1,12,28)','4'=>'四月假期(输入格式如:1,12,28)','5'=>'五月假期(输入格式如:1,12,28)','6'=>'六月假期(输入格式如:1,12,28)','7'=>'七月假期(输入格式如:1,12,28)','8'=>'八月假期(输入格式如:1,12,28)','9'=>'九月假期(输入格式如:1,12,28)','10'=>'十月假期(输入格式如:1,12,28)','11'=>'十一月假期(输入格式如:1,12,28)','12'=>'十二月假期(输入格式如:1,12,28)','up'=>'上半学期开始/结束时间(输入格式如：2017-2-15|2017-6-30)','next'=>'下半学期开始/结束时间(输入格式如：2017-9-1|2018-1-15)');
			$this->load->librarys('View')->assign('info',$info)->assign('days',$days)->display('days_edit');
		}
	}
	
	/**
	 * [ajax_del 删除品牌，可批量删除]
	 */
	public function ajax_del(){
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$_GET['id']);
		$result = model('holidays')->where($sqlmap)->delete();		
		if(!$result){
			showmessage('删除失败');
		}else{
			showmessage(lang('_operation_success_'),url('index'),1);
		}
	}
}