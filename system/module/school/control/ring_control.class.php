<?php
class ring_control extends init_control
{
    public function _initialize()
    {
        parent::_initialize();
		$this->circleService = $this->load->service('school/circle');
        $this->cmService = $this->load->service('school/circle_comment');
    }

    public function index()
    {
        $SEO = seo('托班圈');
        helper('attachment');
		if($this->member['group_id'] <= 2){
			$class_ids = $this->load->table('member/member_student')->where(array('mid' => $this->member['id'],'class_id' =>array('neq',0)))->getField('class_id',TRUE);
			$t_ids1 = $this->load->table('school/class')->where(array('id' => array('IN',$class_ids)))->getField('teacher1_id',TRUE);
			$t_ids2 = $this->load->table('school/class')->where(array('id' => array('IN',$class_ids)))->getField('teacher2_id',TRUE);
			$t_ids = array_merge($t_ids1,$t_ids2);
			$x_ids = $this->load->table('member/member')->where(array('group_id' => 3))->getField('id',TRUE);
			$m_ids = array_merge($t_ids,$x_ids);
			$m_ids[count($m_ids)] = $this->member['id'];
			$circles = $this->circleService->get_lists(array('mid'=>array('IN',$m_ids)));
			if(!$circles){
				$circles = $this->circleService->get_lists(array('recommend'=>1));
			}
		}else{
			$circles = $this->circleService->get_lists();	
		}
        foreach($circles as $k => $v){
            $circles[$k]['mylike'] = $this->cmService->count(array('cid'=>$v['id'],'mid'=>$this->member['id'],'type'=>1));
        }
        $newMagessCount = $this->cmService->count(array('_string'=>'(status=0 and uid='.$this->member['id'].') or (rstatus=0 and rid='.$this->member['id'].')'));
        $newMagess = $this->cmService->find(array('_string'=>'(status=0 and uid='.$this->member['id'].') or (rstatus=0 and rid='.$this->member['id'].')'));
        $attachment_init = attachment_init(array('module'=>'member', 'path' => 'circle', 'mid' => $this->member['id'],'allow_exts'=>array('bmp','jpg','jpeg','gif','png','mp4')));
		
		$commList = $this->cmService->get_lists(array('_string'=>'(status=0 and uid='.$this->member['id'].') or (rstatus=0 and rid='.$this->member['id'].')'));
		$ids = '';
		foreach($commList as $k => $v){
			if($ids){
				$ids .= ",".$v['id'];
			}else{
				$ids = $v['id'];	
			}
			if($this->member['id'] == $v['uid']){
				$this->cmService->update(array('id'=>$v['id'],'status'=>1));
			}
			
			if($this->member['id'] == $v['rid']){
				$this->cmService->update(array('id'=>$v['id'],'rstatus'=>1));
			}
		}
		$setting = $this->load->service("admin/setting")->get();
        $this->load->librarys('View')->assign('newMagessCount',$newMagessCount)->assign('circle_banner',$setting['circle_banner'])->assign('newMagess',$newMagess)->assign('member',$this->member)->assign('attachment_init',$attachment_init)->assign('circles',$circles)->assign('SEO',$SEO)->assign('ids',$ids)->display('ring_index');
    }
	
	public function add(){
		if(IS_POST){
			$data = $_POST;
			$data['material'] = serialize($data['material']);
			$data['mid'] = $this->member['id'];
			$result = $this->circleService->add_circle($data);
			if(!$result){
				showmessage("发表失败");
			}else{
				showmessage("发表成功",url('index'),1);
			}
		}else{
			$SEO = seo('发表图文');
			helper('attachment');
			$img = trim($_GET['img']);
			$attachment_init = attachment_init(array('module'=>'member', 'path' => 'circle', 'mid' => $this->member['id'],'allow_exts'=>array('bmp','jpg','jpeg','gif','png','mp4')));
			$this->load->librarys('View')->assign('attachment_init',$attachment_init)->assign('img',$img)->assign('SEO',$SEO)->display('ring_add');
		}
	}
	
	public function likes(){
            $id = intval($_POST['id']);
            $uid = intval($_POST['uid']);
            $mid = $this->member['id'];
            if($this->cmService->count(array('cid'=>$id,'mid'=>$this->member['id'],'type'=>1)) > 0){
                $commInfo = $this->cmService->find(array('cid'=>$id,'mid'=>$this->member['id'],'type'=>1));
                $result = $this->cmService->delete_by_id($commInfo['id']);
                if(!$result){
                    showmessage("取消点赞失败");
                }else{
                    showmessage("取消点赞成功",url('index'),1,array('type'=>1,'mid'=>$mid,'id'=>$id,'cid'=>$commInfo['id']));
                }
            }else{
                $data['uid'] = $uid;
                $data['cid'] = $id;
                $data['mid'] = $mid;
                $data['type'] = 1;
                $result = $this->cmService->add_comment($data);
                if(!$result){
                        showmessage("点赞失败");
                }else{
                        showmessage("点赞成功",url('index'),1,array('type'=>0,'mid'=>$mid,'id'=>$id,'cid'=>$result,'truename' => $this->load->table('member/member')->fetch_by_id($mid, 'truename')));
                }
            }
	}
        
        public function message(){
            $id = intval($_POST['id']);
            $uid = intval($_POST['uid']);
            $content = trim($_POST['content']);
            $mid = $this->member['id'];
            $rid = intval($_POST['rid']);
            
            $data['uid'] = $uid;
            $data['cid'] = $id;
            $data['mid'] = $mid;
            $data['rid'] = $rid;
            $data['content'] = $content;
            $data['rtime'] = time();
            $data['rstatus'] = 0;
            if($uid == $mid){
                $data['status'] = 1;
            }
            $data['type'] = 0;
            $result = $this->cmService->add_comment($data);
            if(!$result){
                    showmessage("评论失败");
            }else{
                    showmessage("评论成功",url('index'),1,array('type'=>0,'mid'=>$mid,'id'=>$id,'rid'=>$rid,'cid'=>$result,'truename' => $this->load->table('member/member')->fetch_by_id($mid, 'truename'),'rtruename' => $this->load->table('member/member')->fetch_by_id($rid, 'truename')));
            }
        }
        
        public function detail(){
            $SEO = seo('托班圈');
            $info = $this->circleService->find(array('id'=>intval($_GET['id'])));
            $info['mylike'] = $this->cmService->count(array('cid'=>$info['id'],'mid'=>$this->member['id'],'type'=>1));
            $this->load->librarys('View')->assign('SEO',$SEO)->assign('info',$info)->assign('member',$this->member)->display('ring_detail');
        }
        
        public function news(){
            $SEO = seo('最新消息');
			$ids = $_GET['ids'];
            $commList = $this->cmService->get_lists(array('id'=>array('IN',$ids)));
            foreach($commList as $k => $v){
                $commList[$k]['time_tran'] = time_tran($v['addtime']);
                if($this->member['id'] == $v['uid']){
                    $this->cmService->update(array('id'=>$v['id'],'status'=>1));
                }
                
                if($this->member['id'] == $v['rid']){
                    $this->cmService->update(array('id'=>$v['id'],'rstatus'=>1));
                }
            }
            $this->load->librarys('View')->assign('SEO',$SEO)->assign('commList',$commList)->display('ring_news');
        }
        
        public function delete(){
            $result = $this->circleService->delete_by_id(intval($_POST['id']));
            if(!$result){
                showmessage("删除失败");
            }else{
                showmessage("删除成功",url('index'),1);
            }
        }
}