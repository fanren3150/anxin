<?php
class index_control extends init_control
{
    public function _initialize()
    {
        parent::_initialize();
        $this->schoolService = $this->load->service('school/school');
		$this->memberService = $this->load->service('member/member');
    }

    public function index()
    {
        if($this->member['mobilestatus'] == 1){
                redirect(url('member/index/index'));
        }else{
			$SEO = seo('注册');
            $setting = model('admin/setting','service')->get();
            $seos = $setting['seos'];
            $site_title = $setting['site_name'] . ' - ' . $seos['header_title_add'];
            $site_keywords = $seos['header_keywords'];
            $site_description = $seos['header_description'];
            $site_rewrite_other = $seos['header_other'];
            $SEO = seo('注册');
            $schools = $this->schoolService->getlists();
            $areas = $this->schoolService->get_area();
			$setting = $this->load->service("admin/setting")->get();
            $this->load->librarys('View')->assign('SEO',$SEO)->assign('reg_banner',$setting['reg_banner'])->assign('setting',$setting)->assign('schools',$schools)->assign('areas',$areas)->assign('site_rewrite_other',$site_rewrite_other)->display('index');
        }
    }
	
	public function qrcode(){
		$SEO = seo('我的推广海报');
        include_once APP_PATH.'library/phpqrcode/phpqrcode.php';
        $matrixPointSize = '5';
        $url = 'http://'.$_SERVER['HTTP_HOST'].'/?store_id='.$this->member['id'];
        $path = "./uploadfile/qrcode/".$this->member['id']."/";
        $png = $path.'Extension'.$this->member['id'].'.png';
        if (is_dir($path)){
            if(!file_exists($png)){
                $fileName = $path.'Extension'.$this->member['id'].'.png';
                QRcode::png($url, $fileName, '3', $matrixPointSize);
            }
        }else{
            $res=mkdir(iconv("UTF-8", "GBK", $path),0777,true);
            if ($res){
                $fileName = $path.'Extension'.$this->member['id'].'.png';
                QRcode::png($url, $fileName, '3', $matrixPointSize);
            }else{
                echo "生成失败";
            }
        }
        if ($_GET['down']==1){
            header("Content-type: octet/stream");
            header("Content-disposition:attachment;filename=Extension.png;");
            header("Content-Length:".filesize($png));
            readfile($png);
            exit;
        }
		$setting = $this->load->service("admin/setting")->get();
		$this->load->librarys('View')->assign('SEO',$SEO)->assign('qrcode_bgimg',$setting['qrcode_bgimg'])->assign('png',$png)->display('haibao');
	}
	public function checkmobile(){
		$data = $_POST;
		$member = $this->memberService->find(array('mobile'=>$data['mobile'],'id'=>array('neq',$this->member['id'])));
		if($member){
			showmessage("手机号码已存在！");
		}else{
			$sqlmap = array();
			$sqlmap['mobile'] = $data['mobile'];
			$sqlmap['dateline'] = array('EGT',time()-1800);
			$vcode = model('vcode')->where($sqlmap)->order('dateline desc')->getField('vcode');
			if($vcode != $data['vcode']){
				showmessage("验证码错误！");
			}else{
				$ret = $this->load->table('member/member')->where(array('id'=>$this->member['id']))->save(array('mobile'=>$data['mobile'],'city'=>$data['city'],'school'=>$data['school'],'mobilestatus'=>1));
				if($ret){
					showmessage("注册成功",url('member/index/index'),1);	
				}else{
					showmessage("注册失败，请稍后再试！");
				}
			}
		}
	}
	
	//发送验证码
	public function checkansend(){
		$this->load->service('member/member')->vcode_delete(array('mid' => $this->member['id'],'action' =>'resetmobile','dateline'=>array('LT',TIMESTAMP)));
		$member = $_GET;
		$member['mid'] = $this->member['id'];
		$result = model('member/sms','service')->post_vcode($member,'resetmobile');
		if($result){
			showmessage(lang('send_msg_success','member/language'),'',1);
		}else{
			showmessage(lang('send_msg_error','member/language'),'',0);
		}
	}
	
	public function getSchool(){
		$area = trim($_GET['area']);
		$schools = $this->schoolService->getlists(array('area'=>$area));
		if($schools){
			showmessage('获取学校信息成功','',1,$schools);
		}else{
			showmessage('所选区域没有学校信息','',0);
		}
	}
	
	/**
     * [html_load html加载完毕后执行]
     * @return [type] [description]
     */
    public function html_load()
    {
		/*推荐有理*/
		$member = $this->load->table('member/member')->where(array('source_id'=>array('GT',0)))->select();
		foreach($member as $k => $v){
			$morder = $this->load->table('order/order')->where(array('buyer_id'=>$v['id'],'status'=>1,'pay_status'=>1))->order("id asc")->find();
			$sorder = $this->load->table('order/order')->where(array('buyer_id'=>$v['source_id'],'status'=>1,'pay_status'=>1))->order("id asc")->find();
			if(count($morder) > 0 && count($sorder) > 0){
				$mlog = $this->load->table('member_log')->where(array('mid'=>$v['id'],'uid'=>$v['source_id'],'ltype'=>3))->count();
				
				$school = $this->load->table('school/school')->where(array('id'=>$morder['school_id']))->find();
				$smember = $this->load->table('member/member')->where(array('id'=>$v['source_id']))->find();
				$money = '+'.$school['year_price'];
				$field = "money";
				/*被邀请的人*/
				if($mlog == 0){
					$msg = "受".$smember['username']."邀请奖励";
					$this->load->table('member')->where(array('id' => $v['id']))->setField($field, array("exp", $field.$money));
					$_member = $this->load->table('member')->setid($v['id'])->output();
					$log_info = array(
						'mid'      => $v['id'],
						'uid'      => $v['source_id'],
						'value'    => $money,
						'ltype'     => 3,
						'type'     => $field,
						'msg'      => $msg,
						'dateline' => TIMESTAMP,
						'admin_id' => (defined('IN_ADMIN')) ? ADMIN_ID : 0,
						'money_detail' => json_encode(array($field => sprintf('%.2f' ,$_member[$field])))
					);
					$this->load->table('member_log')->update($log_info);
				}
				
				/*邀请的人*/								
				$slog = $this->load->table('member_log')->where(array('mid'=>$v['source_id'],'uid'=>$v['id'],'ltype'=>3))->count();
				if($slog == 0){
					$msg = "邀请".$v['username']."奖励";
					$this->load->table('member')->where(array('id' => $v['source_id']))->setField($field, array("exp", $field.$money));
					$_member = $this->load->table('member')->setid($v['source_id'])->output();
					$log_info = array(
						'mid'      => $v['source_id'],
						'uid'      => $v['id'],
						'value'    => $money,
						'ltype'     => 3,
						'type'     => $field,
						'msg'      => $msg,
						'dateline' => TIMESTAMP,
						'admin_id' => (defined('IN_ADMIN')) ? ADMIN_ID : 0,
						'money_detail' => json_encode(array($field => sprintf('%.2f' ,$_member[$field])))
					);
					$this->load->table('member_log')->update($log_info);
				}
			}
		}
		
		/*请假退款*/
		$t = time();
		$start = mktime(0,0,0,date("m",$t),date("d",$t),date("Y",$t));
		$point = $this->load->table('member/point')->where(array('type'=>1,'ispay'=>0,'_string'=>'end_time < '.$start))->select();
		foreach($point as $k => $v){
			if(date("Y-m-d",$v['end_time']) == date("Y-m-d",$v['addtime'])){//当天请假不退款
				$this->load->table('point')->where(array('id' => $v['id']))->setField("ispay", 1);
			}elseif(date("Y-m-d",$v['end_time']) == date("Y-m-d",$v['addtime']+86400) && date("H",$v['addtime']) > 17){//隔天18点后请假，不退款
				$this->load->table('point')->where(array('id' => $v['id']))->setField("ispay", 1);
			}else{
				$student = $this->load->table('member/member_student')->where(array('id'=>$v['student_id']))->find();
				if($student){
					$money = '+'.$student['school']['price'];
					$field = "money";
					$msg = date("Y-m-d",$v['end_time']).$student['truename']."请假退款";
					$this->load->table('member')->where(array('id' => $v['mid']))->setField($field, array("exp", $field.$money));
					$_member = $this->load->table('member')->setid($v['mid'])->output();
					$log_info = array(
						'mid'      => $v['mid'],
						'value'    => $money,
						'ltype'     => 1,
						'type'     => $field,
						'msg'      => $msg,
						'dateline' => TIMESTAMP,
						'admin_id' => (defined('IN_ADMIN')) ? ADMIN_ID : 0,
						'money_detail' => json_encode(array($field => sprintf('%.2f' ,$_member[$field])))
					);
					$this->load->table('member_log')->update($log_info);
					$this->load->table('point')->where(array('id' => $v['id']))->setField("ispay", 1);
				}else{
					$this->load->table('point')->where(array('id' => $v['id']))->setField("ispay", 1);	
				}
			}
		}		
        runhook('html_load');
    }
}