<?php
class init_control extends control
{
	public function _initialize() {
		defined('IN_PLUGIN') OR define('IN_PLUGIN', TRUE);
		parent::_initialize();
		$this->member = $this->load->service('member/member')->init();
		$this->load->librarys('View')->assign('member',$this->member);
		
		$pra = '&url_forward='.urlencode($_SERVER['REQUEST_URI']);
		if(intval($_GET['store_id']) > 0){
			$pra .= "&store_id=".intval($_GET['store_id']);
		}
		//&& CONTROL_NAME != 'ring'
		if (!$this->member['id'] && MODULE_NAME != 'weixin' && get_client_ip() != "127.0.0.1") {			
			//if(!$_GET['code'] && checkmobile() == 'weixin'){
			if(!$_GET['code']){
				$this->token = Token;
				$this->AppId = AppId;
				$this->AppSecret = AppSecret;
				$bizString ="appid=".$this->AppId."&redirect_uri=".urlencode("http://".$_SERVER['HTTP_HOST']."/index.php?m=weixin&c=weixin&a=aouth".$pra)."&response_type=code&scope=snsapi_userinfo&state=1"."#wechat_redirect";
				redirect("https://open.weixin.qq.com/connect/oauth2/authorize?".$bizString);			
			}
		}
		define('SKIN_PATH', __ROOT__.(str_replace(DOC_ROOT, '', TPL_PATH)).config('TPL_THEME').'/');
		define('SITE_AUTHORIZE', (int)$cloud['authorize']);
		define('COPYRIGHT', '');
		/* 检测商城运营状态 */
		runhook('site_isclosed');
	}
}