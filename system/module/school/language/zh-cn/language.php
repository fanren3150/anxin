<?php
return array(
    'no_found_school'                => '未找到学校',
	'no_found_class'                => '未找到班级',
	'sort_require'                  => '排序必须是数字',
);