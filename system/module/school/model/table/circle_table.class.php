<?php
class circle_table extends table {
	protected $result;
	protected $_validate = array(
		array('title', 'require', '发布内容不能为空'),
    );
    protected $_auto = array(
		array('addtime', TIMESTAMP, 1, 'string'),
		array('addip', 'get_client_ip', 1, 'function'),
    );
    
    public function _after_select($result, $options) {
        $circles = array();
        foreach($result AS $r) {
            $r['comments'] = $this->load->table('school/circle_comment')->where(array('cid'=>$r['id'],'type'=>0))->order('id asc')->select();
            $r['like'] = $this->load->table('school/circle_comment')->where(array('cid'=>$r['id'],'type'=>1))->order('id asc')->select();
            $r['user'] = $this->load->table('member/member')->fetch_by_id($r['mid']);
            $circles[$r['id']] = $r;
        }
        return $circles;
    }
    
    public function _after_find($result, $options) {
        $r = $result;
        $r['comments'] = $this->load->table('school/circle_comment')->where(array('cid'=>$r['id'],'type'=>0))->order('id asc')->select();
        $r['like'] = $this->load->table('school/circle_comment')->where(array('cid'=>$r['id'],'type'=>1))->order('id asc')->select();
        $r['user'] = $this->load->table('member/member')->fetch_by_id($r['mid']);
        $this->data = $r;
        return $this->data;
    }
    public function output(){
        return $this->result['school'];
    }
}