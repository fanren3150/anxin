<?php
class initiate_table extends table {
	protected $result;
	protected $_validate = array(
        
    );
    protected $_auto = array(
		array('addtime', TIMESTAMP, 1, 'string'),
    );
	public function detail($id,$field){
		$this->result['initiate'] = $this->field($field)->find($id);
		return $this;
	}
	
	public function output(){
		return $this->result['initiate'];
	}
}