<?php
class class_table extends table {
	protected $result;
	protected $_validate = array(

    );
    protected $_auto = array(
    );
     public function detail($id,$field){
        $this->result['class'] = $this->field($field)->find($id);
        return $this;
    }
	
	public function _after_find($result, $options) {
        $r = $result;
        $r['school'] = $this->load->table('school/school')->where(array('id'=>$r['school_id']))->find();
        $r['teacher1'] = $this->load->table('member/member')->fetch_by_id($r['teacher1_id']);
        $r['teacher2'] = $this->load->table('member/member')->fetch_by_id($r['teacher2_id']);
        $this->data = $r;
        return $this->data;
    }
	
    public function output(){
        return $this->result['class'];
    }
}