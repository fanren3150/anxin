<?php
class school_table extends table {
	protected $result;
	protected $_validate = array(
        array('sort','number','{school/sort_require}',table::EXISTS_VALIDATE,'regex'),
    );
    protected $_auto = array(
    );
    public function fetch_by_id($id, $field = null) {
		if($id < 1) {
			$this->error = lang('_param_error_');
			return false;
		}
		$rs = $this->find($id);
		return (!is_null($field)) ? $rs[$field] : $rs;
	}
	public function detail($id,$field){
        $this->result['school'] = $this->field($field)->find($id);
        return $this;
    }
    public function output(){
        return $this->result['school'];
    }
}