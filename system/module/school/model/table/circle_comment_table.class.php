<?php
class circle_comment_table extends table {
	protected $result;
	protected $_validate = array(
		array('content', 'require', '请填写评论内容'),
    );
    protected $_auto = array(
		array('addtime', TIMESTAMP, 1, 'string'),
    );
    
    public function _after_select($result, $options) {
        $comments = array();
        foreach($result AS $r) {
            $r['truename'] = $this->load->table('member/member')->fetch_by_id($r['mid'], 'truename');
			if($r['truename']){
				$r['avatar'] = getavatar($r['mid']);
				$r['rtruename'] = $this->load->table('member/member')->fetch_by_id($r['rid'], 'truename');
				$r['ravatar'] = getavatar($r['rid']);
				$comments[$r['id']] = $r;
		   	}
        }
        return $comments;
    }
    
    public function _after_find($result, $options) {
        $result['user'] = $this->load->table('member/member')->fetch_by_id($result['mid']);
        $result['avatar'] = getavatar($result['mid']);
        $this->data = $result;
        return $this->data;
    }
    public function output(){
        return $this->result;
    }
}