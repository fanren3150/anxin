<?php
class initiate_service extends service {
	public function _initialize() {
		$this->db = $this->load->table('initiate/initiate');
	}
	
	public function get_lists($sqlmap,$page,$limit){
		$result = $this->db->where($sqlmap)->page($page)->limit($limit)->order('id asc')->select();
		$lists = array();
		foreach($result as $k => $v){
			$lists[] = array(
				'id'=>$v['id'],
				'initiate_name' => $this->load->table('member/member')->where(array('id'=>$v['mid']))->getField('username'),
				'school_name'  => $this->load->table('school/school')->where(array('id'=>$v['school_id']))->getField('name'),
				'number'  => $v['number'],
				'pay_number'  => $this->load->table('order/order')->where(array('iid'=>$v['id'],'status'=>1,'pay_status'=>1))->count(),
				'initiate_number'  => $this->load->table('member/member_deposit')->where(array('iid'=>$v['id']))->count(),
				'student_ids'  => $this->load->table('member/member_deposit')->where(array('iid'=>$v['id']))->getField('student_id',true),
				'area'  => $v['area'],
				'addtime'  => $v['addtime'],
				'isopen'  => $v['isopen'],
				'status'  => $v['status'],
				'_open'  => ($v['isopen'] == 1) ? '<font color="#FF0000">是</font>' : '否',
				'_status'  => ($v['status'] == 1) ? '有效' : '<font color="#FF0000">无效</font>',
			);
		}
		
		return $lists;
	}
	
	public function getlists($sqlmap){
		$result = $this->db->where($sqlmap)->order('id asc')->getField('id,mid,student_id,school_id,school_name,school_logo,school_price,area,number,isopen,addtime,status',TRUE);
		if(!$result){
			$this->error = $this->db->getError();
		}
		return $result;
	}
	
	public function add_initiate($params = array()){
		$result = $this->db->update($params);
    	if($result === FALSE){
    		$this->error = $this->db->getError();
    		return FALSE;
    	}
    	return $result;
	}
	
	public function edit_initiate($params){
		if((int)$params['id'] < 1){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		$result = $this->db->update($params);
    	if($result === FALSE){
    		$this->error = $this->db->getError();
    		return FALSE;
    	}
    	return $result;
	}
	
	public function delete_initiate($params){
		if(empty($params)){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$params);
		$infos = $this->db->where($data)->getField('logo',true);
		$result = $this->db->where($sqlmap)->delete();
		if(!$result){
    		$this->error = lang('_operation_fail_');
    		return FALSE;
    	}
    	return $result;
	}
	
	public function get_initiate_by_id($id){
		if((int)$id < 1){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		$result = $this->db->find($id);
		if(!$result){
			$this->error = lang('发起托班信息不存在');
		}
		return $result;
	}

	public function detail($id, $field=''){
    	return $this->db->detail($id, $field)->output();
    }
    /**
     * 条数
     * @param  [arra] sql条件
     * @return [type]
     */
    public function count($sqlmap = array()){
        $result = $this->db->where($sqlmap)->count();
        if($result === false){
            $this->error = $this->db->getError();
            return false;
        }
        return $result;
    }
	
	/**
	 * 生成查询条件
	 * @param  $options['type'] (1:未开班|2:已开班|3:无效|4:正常)
	 * @param  $options['keyword'] 	关键词(订单号|收货人姓名|收货人手机)
	 * @return [$sqlmap]
	 */
	public function build_sqlmap($options) {
		if(empty($options['type'])){
       		$options['type'] = $options['map']['type'];
     	}
		extract($options);
		$sqlmap = array();
		if (isset($type) && $type > 0) {
			switch ($type) {
				case 1:
					$sqlmap['status']     = 1;
					$sqlmap['isopen'] = 0;
					break;
				case 2:
					$sqlmap['status']     = 1;
					$sqlmap['isopen'] = 1;
					break;
				case 3:
					$sqlmap['status']     = 0;
					break;
				case 4:
					$sqlmap['status']     = 1;
					break;
				
				case 6:
					$sqlmap['status'] = 0;
					break;
			}
		}
		if($isopen > -1){
			$sqlmap['isopen'] = $isopen;
		}
		if (isset($keyword) && !empty($keyword)) {
			$buyer_ids = $this->load->table('member/member')->where(array('username' => array('LIKE','%'.$keyword.'%')))->getField('id',TRUE);
			if($buyer_ids){
				$sqlmap['mid'] = array('IN',$buyer_ids);
			}
			
			if (!$buyer_ids) {
				$sqlmap['school_name|area'] = array('LIKE','%'.$keyword.'%');
			}
		}
		return $sqlmap;
	}
}