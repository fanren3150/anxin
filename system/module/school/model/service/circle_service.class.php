<?php
class circle_service extends service {
    protected $result;

    public function _initialize() {
        $this->model = $this->load->table('school/circle');
	}

    public function fetch_by_id($id) {
        $r = $this->model->find($id);
        if(!$r) {
            $this->error = '_select_not_exist_';
            return FALSE;
        }
        return $r;
    }
    
    public function delete_by_id($id) {
        $ids = (array) $id;
        foreach($ids AS $id) {
            if($this->model->delete($id)) {
                $this->load->table('school/circle_comment')->where(array("cid" => $id))->delete();
            }
        }
        return TRUE;
    }
	
    public function add_circle($params){
        $data = $this->model->create($params);
        return $this->model->add($data);
    }

    public function get_lists($sqlmap,$page,$limit){
        $circles = $this->load->table('school/circle')->where($sqlmap)->page($page)->order('id DESC')->limit($limit)->select();		
        $lists = array();
        foreach ($circles AS $circle) {
            $lists[] = array(
                'id' => $circle['id'],
				'mid' => $circle['mid'],
                'title' => $circle['title'],
                'material' => unserialize($circle['material']),
                'comments' => $circle['comments'],
                'like' => $circle['like'],
                'user' => $circle['user'],
				'recommend' => $circle['recommend'],
				"time_tran" => time_tran($circle['addtime']),
                'addtime' => date('Y-m-d H:i:s', $circle['addtime'])
            );
        }
		
        return $lists;
    }
    /**
     * [update 更新数据]
     * @param  [type] $params [参数]
     * @return [type]         [description]
     */
    public function update($params ,$bool = true){
        if(empty($params)){
            $this->error = lang('_params_error_');
            return false;
        }
        $result = $this->model->update($params ,$bool);
        if($result === false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
   
    /**
     * 条数
     * @param  [arra]   sql条件
     * @return [type]
     */
    public function count($sqlmap = array()){
        $result = $this->model->where($sqlmap)->count();
        if($result === false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
    
    /**
     * @param  array    sql条件
     * @param  integer  读取的字段
     * @return [type]
     */
    public function find($sqlmap = array(), $field = "") {
        $result = $this->model->where($sqlmap)->field($field)->find();
        if($result===false){
            $this->error = $this->model->getError();
            return false;
        }
        $result['material'] = unserialize($result['material']);
        $result['addtime'] = date('Y-m-d H:i:s', $result['addtime']);
        return $result;
    }
	
	public function change_status($id) {
		$result = $this->model->where(array('id' => $id))->save(array('recommend' => array('exp', '1-recommend')));
		if (!$result) {
			$this->error = $this->model->getError();
			return FALSE;
		}
		return TRUE;
	}
}