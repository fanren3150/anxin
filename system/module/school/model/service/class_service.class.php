<?php
class class_service extends service {
	public function _initialize() {
		$this->db = $this->load->table('school/class');
	}
	
	public function get_lists($page,$limit,$sqlmap=''){
		$result = $this->db->where($sqlmap)->page($page)->limit($limit)->getField('id,name,school_id,teacher1_id,teacher2_id',TRUE);
		if(!$result){
			$this->error = $this->db->getError();
		}
		foreach($result as $k => $v){
                    $result[$k]['school_name'] = $this->load->table('school/school')->where(array('id'=>$v['school_id']))->getField('name');
                    $result[$k]['student_number'] = $this->load->table('member/member_student')->where(array('class_id'=>$v['id'],'end_time'=>array("gt",time())))->count();
                    $result[$k]['teacher1_name'] = $this->load->table('member/member')->where(array('id'=>$v['teacher1_id']))->getField('username');
                    $result[$k]['teacher2_name'] = $this->load->table('member/member')->where(array('id'=>$v['teacher2_id']))->getField('username');
		}
		return $result;
	}
	
	public function add_class($params = array()){
		$result = $this->db->update($params);
    	if($result === FALSE){
    		$this->error = $this->db->getError();
    		return FALSE;
    	}
    	return $result;
	}
	
	public function edit_class($params){
		if((int)$params['id'] < 1){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		$result = $this->db->update($params);
    	if($result === FALSE){
    		$this->error = $this->db->getError();
    		return FALSE;
    	}
    	return $result;
	}
	
	public function delete_class($params){
		if(empty($params)){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$params);
		$infos = $this->db->where($data)->getField('logo',true);
		$result = $this->db->where($sqlmap)->delete();
		if(!$result){
    		$this->error = lang('_operation_fail_');
    		return FALSE;
    	}
    	return $result;
	}
	
	public function change_name($params){
		if((int)$params['id'] < 1){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		$data = array();
		$data['name'] = $params['name'];
		$result = $this->db->where(array('id'=>$params['id']))->save($data);
		if(!$result){
    		$this->error = lang('_operation_fail_');
    		return FALSE;
    	}
    	return $result;
    }
	
	public function get_class_by_id($id){
		if((int)$id < 1){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		$result = $this->db->find($id);
		if(!$result){
			$this->error = lang('school/no_found_class');
		}
		return $result;
	}

	public function detail($id, $field=''){
    	return $this->db->detail($id, $field)->output();
    }
    /**
     * 条数
     * @param  [arra]   sql条件
     * @return [type]
     */
    public function count($sqlmap = array()){
        $result = $this->db->where($sqlmap)->count();
        if($result === false){
            $this->error = $this->db->getError();
            return false;
        }
        return $result;
    }


	
}