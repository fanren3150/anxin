<?php
class school_service extends service {
	public function _initialize() {
		$this->db = $this->load->table('school/school');
	}
	
	public function get_lists($page,$limit,$sqlmap=array()){
		$result = $this->db->page($page)->limit($limit)->where($sqlmap)->order('sort asc')->getField('id,sort,name,area,months_price,price,year_price,logo',TRUE);
		if(!$result){
			$this->error = $this->db->getError();
		}
		return $result;
	}
	
	public function getlists($sqlmap){
		$result = $this->db->where($sqlmap)->order('sort asc')->getField('id,name,area,months_price,price,year_price,logo,sort',TRUE);
		if(!$result){
			$this->error = $this->db->getError();
		}
		return $result;
	}
	
        public function getSchoolOrClass($sqlmap,$num=0,$class_id=0){
            $schoolInfo = $this->db->where($sqlmap)->find();
            $initiates = $this->load->table('school/initiate')->where(array('school_id' => $schoolInfo['id']))->find();
            $schoolInfo['iid'] = $initiates['id'];
            $schoolInfo['number'] = $initiates['number'];
            $schoolInfo['class_id'] = 0;
            if($initiates && $initiates['isopen'] == 1){                
                if($class_id > 0){
                    $class = $this->load->table('school/class')->where(array('id' => $class_id))->find();
                    $class = $this->db->query("SELECT *,(SELECT COUNT(id) FROM ax_member_student WHERE class_id=ax_class.id) AS studentN FROM ax_class where id=".$class_id." ORDER BY studentN ASC");
                    $class = $class[0];
                }else{
                    $setting = $this->load->service("admin/setting")->get();
                    $classNumber = $setting['classNumber'];
                    $info = $this->db->query("SELECT *,(SELECT COUNT(id) FROM ax_member_student WHERE class_id=ax_class.id) AS studentN FROM ax_class where school_id=".$schoolInfo['id']." ORDER BY studentN ASC");
                    if($info){
                        $newClass = $newClass1 = $newClass2 = array();
                        foreach($info as $k => $v){
                            if($v['studentN'] > 0){
                                if($v['studentN'] < $classNumber){
                                    $v['minc'] = abs(intval($v['name'])-$num);
                                    $newClass1[] =$v;
                                }

                                if($v['studentN'] > $classNumber){
                                    $v['minc'] = abs(intval($v['name'])-$num);
                                    $newClass2[] =$v;
                                }
                            }
							$newClass[] =$v;
                        }
						$newClass2 = array_filter($newClass2);
						$newClass1 = array_filter($newClass1);
                        if(count($newClass1) > 0){
                            $class = sortMultiArray($newClass1,'minc');
                            $class = $class[0];
                        }else{
							if(count($newClass2) > 0){
                            	$class = sortMultiArray($newClass2,'minc');
                            	$class = $class[0];
							}else{
								$class = sortMultiArray($newClass,'minc');
                            	$class = $class[0];	
							}
                        }
                    }    
                }
                $schoolInfo['name'] = $schoolInfo['name'].$class['name']."班";
                $schoolInfo['logo'] = $class['logo'];
                $schoolInfo['class_id'] = $class['id'];
                $schoolInfo['count'] = $this->load->service('member/member_student')->count(array('class_id'=>$class['id']));
                $schoolInfo['deps'] = $this->load->service('member/member_student')->get_lists(array('class_id'=>$class['id']),1,5);
            }else{
                $schoolInfo['count'] = $this->load->service('member/member_deposit')->count(array('iid'=>$schoolInfo['iid']));
				$schoolInfo['deps'] = $this->load->service('member/member_deposit')->fetch(array('iid'=>$schoolInfo['iid']),5);                
            }
            return $schoolInfo;
        }
        public function add_school($params = array()){
		$result = $this->db->update($params);
    	if($result === FALSE){
    		$this->error = $this->db->getError();
    		return FALSE;
    	}
    	return $result;
	}
	
	public function edit_school($params){
		if((int)$params['id'] < 1){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		$result = $this->db->update($params);
    	if($result === FALSE){
    		$this->error = $this->db->getError();
    		return FALSE;
    	}
    	return $result;
	}
	
	public function delete_school($params){
		if(empty($params)){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$params);
		$infos = $this->db->where($data)->getField('logo',true);
		$result = $this->db->where($sqlmap)->delete();
		if(!$result){
    		$this->error = lang('_operation_fail_');
    		return FALSE;
    	}
    	return $result;
	}
	
	public function change_name($params){
		if((int)$params['id'] < 1){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		$data = array();
		$data['name'] = $params['name'];
		$result = $this->db->where(array('id'=>$params['id']))->save($data);
		if(!$result){
    		$this->error = lang('_operation_fail_');
    		return FALSE;
    	}
    	return $result;
    }
	
	public function change_sort($params){
		if((int)$params['id'] < 1){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		$data = array();
		$data['sort'] = $params['sort'];
		$result = $this->db->where(array('id'=>array('eq',$params['id'])))->save($data);
		if(!$result){
    		$this->error = lang('_operation_fail_');
    		return FALSE;
    	}
    	return $result;
    }
	
	public function get_school_by_id($id){
		if((int)$id < 1){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		$result = $this->db->find($id);
		if(!$result){
			$this->error = lang('school/no_found_school');
		}
		return $result;
	}

	public function detail($id, $field=''){
    	return $this->db->detail($id, $field)->output();
    }
    /**
     * 条数
     * @param  [arra]   sql条件
     * @return [type]
     */
    public function count($sqlmap = array()){
        $result = $this->db->where($sqlmap)->count();
        if($result === false){
            $this->error = $this->db->getError();
            return false;
        }
        return $result;
    }

	public function ajax_school($keyword){
		$sqlmap = array();
		if($keyword){
			$sqlmap = array('name'=>array('LIKE','%'.$keyword.'%'));
		}
		$result = $this->db->where($sqlmap)->getField('id,name',TRUE);
		if(!$result){
			$this->error = lang('_operation_fail_');
    	}
		return $result;
	}
	
	public function get_area(){
		$result = $this->db->query("select distinct area from ax_school");
		return $result;
	}
}