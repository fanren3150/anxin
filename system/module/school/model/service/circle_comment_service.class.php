<?php
class circle_comment_service extends service {
    protected $result;

    public function _initialize() {
        $this->model = $this->load->table('school/circle_comment');
	}

    public function fetch_by_id($id) {
        $r = $this->model->find($id);
        if(!$r) {
            $this->error = '_select_not_exist_';
            return FALSE;
        }
        return $r;
    }
    
    public function delete_by_id($id) {
        $ids = (array) $id;
        foreach($ids AS $id) {
            if($this->model->delete($id)) {
                
            }
        }
        return TRUE;
    }

    public function add_comment($params){
        $data = $this->model->create($params);
        return $this->model->add($data);
    }

    /**
     * [update 更新数据]
     * @param  [type] $params [参数]
     * @return [type]         [description]
     */
    public function update($params ,$bool = true){
        if(empty($params)){
            $this->error = lang('_params_error_');
            return false;
        }
        $result = $this->model->update($params ,$bool);
        if($result === false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
   
     public function get_lists($sqlmap,$page,$limit){
        $comments = $this->load->table('school/circle_comment')->where($sqlmap)->page($page)->order('id DESC')->limit($limit)->select();
        $lists = array();
        foreach ($comments AS $k => $comment) {
            $comments[$k]['circle'] = $this->load->table('school/circle')->where(array('id'=>$comment['cid']))->find();
        }
        
        return $comments;
    }
    /**
     * 条数
     * @param  [arra]   sql条件
     * @return [type]
     */
    public function count($sqlmap = array()){
        $result = $this->model->where($sqlmap)->count();
        if($result === false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
    
    /**
     * @param  array    sql条件
     * @param  integer  读取的字段
     * @return [type]
     */
    public function find($sqlmap = array(), $field = "") {
        $result = $this->model->where($sqlmap)->field($field)->find();
        if($result===false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
}