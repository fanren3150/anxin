<?php
class index_control extends cp_control
{
	public function _initialize() {
		parent::_initialize();
		$this->schoolService = $this->load->service('school/school');
		$this->classTable = $this->load->table('school/class');
		$this->pointService = $this->load->service('member/point');
		$this->studentService = $this->load->service('member/member_student');
	}

	public function index(){
		
		$SEO = seo('会员');
		if($this->member['group_id'] == 2){
			$SEO = seo('点名打分');
			$class = $this->classTable->where(array('_string' => 'teacher1_id='.$this->member['id'].' or teacher2_id='.$this->member['id']))->getField('id',true);
			$students = $this->load->service('member/member_student')->get_lists(array('class_id'=>array("IN",$class),'end_time'=>array('gt',time())));
			$this->load->librarys('View')->assign('_config',$_config)->assign('students',$students)->assign('SEO',$SEO)->display('teacher_index');
		}else{                
			//获取已添加的孩子
			$students = $this->load->service('member/member_student')->get_lists(array('mid'=>$this->member['id']));
			if(!$students){
				redirect(url('member/index/validation'));
			}
			$ispay = 0;
			foreach($students as $key => $val){
				$orderInfo = $this->load->table('order/order')->where(array('student_id'=>$val['id'],'status'=>1,'pay_status'=>1))->order('system_time desc')->find();
				$students[$key]['orderInfo'] = $orderInfo;
				$students[$key]['isClass'] = 0;
				$schoolData = $this->load->service('school/school')->getSchoolOrClass(array('id'=>$val['school_id']),0);
				if($schoolData['class_id'] > 0){
					$students[$key]['isClass'] = 1;
					$did = model('member_deposit')->where(array("student_id"=>$val['id']))->getField('id');
					if(!$did){
						$ddata['iid'] = $schoolData['iid'];
						$ddata['mid'] = $val['mid'];
						$ddata['student_id'] = $val['id'];
						$ddata['school_id'] = $val['school_id'];
						$ddata['class_id'] = $schoolData['class_id'];
						$ddata['addtime'] = time();
						$this->load->service('member/member_deposit')->add_deposit($ddata);
					}
				}
				if($orderInfo && $ispay == 0){
					$ispay = 1;
				}
			}
			
			if($ispay == 0){
				redirect(url('member/index/validation'));
			}
			//配置文件
			$_config = model('admin/setting','service')->get();			
			$count = $this->load->service('school/initiate')->count(array('mid'=>$this->member['id'],'isopen'=>0,'status'=>1));
			helper('attachment');
			$attachment_init = attachment_init(array('module'=>'member', 'path' => 'member', 'mid' => $this->member['id'],'allow_exts'=>array('bmp','jpg','jpeg','gif','png')));
			$this->load->librarys('View')->assign('attachment_init',$attachment_init)->assign('students',$students)->assign('count',$count)->assign('SEO',$SEO)->display('index');
		}
	}
	
	public function ilist(){
		$SEO = seo('托班');
		$sid = intval($_GET['sid']);
		$school_id = intval($_GET['school_id']);
		if($school_id > 0){
			$initiate = $this->load->service('school/initiate')->getlists(array('school_id'=>$school_id,'isopen'=>0,'status'=>1));
		}else{
			$initiate = $this->load->service('school/initiate')->getlists(array('mid'=>$this->member['id'],'isopen'=>0,'status'=>1));
		}
		$this->load->librarys('View')->assign('initiate',$initiate)->assign('sid',$sid)->assign('SEO',$SEO)->display('ilist');
    }
	
	public function joins(){
		helper('attachment');
		if(IS_POST){
			$data = $_POST;
			$sqlmap = array();
			$sqlmap['mobile'] = $data['mobile'];
			$sqlmap['dateline'] = array('EGT',time()-1800);
			$vcode = model('vcode')->where($sqlmap)->order('dateline desc')->getField('vcode');
			if($vcode != $data['vcode']){
				showmessage("验证码错误！");
			}else{
				unset($data['vcode']);
				$data['group_id'] = 2;
				$data['status'] = 2;
				$data['mobilestatus'] = 1;
				$data['school'] = model('school')->where(array('id'=>$data['school_id']))->getField('name');
				$result = model("member")->where(array('id'=>$this->member['id']))->save($data);
				if(!$result){
					showmessage("提交失败");
				}else{
					showmessage("申请已经提交，请等待管理员审核！",url('joins'),1);
				}
			}
		}else{
			$SEO = seo('加入我们');
			$attachment_init = attachment_init(array('module'=>'member', 'path' => 'member', 'mid' => $this->member['id'],'allow_exts'=>array('bmp','jpg','jpeg','gif','png')));
			$schools = $this->schoolService->getlists();
            $areas = $this->schoolService->get_area();
			$this->load->librarys('View')->assign('attachment_init',$attachment_init)->assign('member',$this->member)->assign('areas',$areas)->assign('SEO',$SEO)->assign('schools',$schools)->assign('areas',$areas)->display('join');
		}
	}
	
	public function validation(){
		helper('attachment');
		if(IS_POST){
			$data = $_POST;
			$iid = intval($data['iid']);
			$sid = intval($data['sid']);
            $class_id = intval($data['class_id']);
			unset($data['iid']);unset($data['sid']);
			$mid = $this->member['id'];
			$school = $this->schoolService->get_school_by_id($data['school_id']);
			$iid = model('initiate')->where(array("school_id"=>$data['school_id']))->getField('id');
			if($sid > 0){
				$result = $sid;
				$data['id'] = $sid;
				$data['mid'] = $mid;
				$data['school_name'] = $school['name'];
				$this->studentService->update($data);
			}else{
				$data['mid'] = $this->member['id'];
				$data['school_name'] = $school['name'];
				$result = $this->studentService->add_student($data);
			}
			if(!$result){
				showmessage($this->studentService->error);
			}else{
				if($iid == 0){
					$setting = $this->load->service("admin/setting")->get();
					$idata['mid'] = $mid;
					$idata['student_id'] = $result;
					$idata['school_id'] = $school['id'];
					$idata['school_name'] = $school['name'];
					$idata['school_logo'] = $school['logo'];
					$idata['area'] = $school['area'];
					$idata['number'] = $setting['classNumber'];
					$idata['school_price'] = $school['months_price'];
					$idata['addtime'] = time();
					$iid = $this->load->service('school/initiate')->add_initiate($idata);
				}
				$ddata['iid'] = $iid;
				$ddata['mid'] = $mid;
				$ddata['student_id'] = $result;
				$ddata['school_id'] = $school['id'];
                $ddata['class_id'] = $class_id;
				$ddata['addtime'] = time();
				$this->load->service('member/member_deposit')->add_deposit($ddata);
				showmessage("提交成功",url('initiate',array('sid'=>$result)),1);
			}
		}else{
			
			$sid = intval($_GET['sid']);			
			/*如果未指定孩子，判断是否存在第一个孩子，如果存在那么已发起*/
			$students = $this->load->service('member/member_student')->find(array('mid'=>$this->member['id']));			
			if($students && $sid == 0){
				redirect(url('initiate',array('sid'=>$students['id'])));
			}
			/*指定孩子，判断是否已发起托班*/
			if($sid > 0 && $this->load->service('member/member_deposit')->count(array('student_id'=>$sid)) > 0){
				redirect(url('initiate',array('sid'=>$sid)));
			}else{
				if($sid > 0){
					$students = $this->load->service('member/member_student')->find(array('id'=>$sid));
					$schoolData = $this->schoolService->getSchoolOrClass(array('id'=>$students['school_id']),intval($students['class_name']));
				}else{
                    $schoolData = $this->schoolService->getSchoolOrClass(array('name'=>$this->member['school']),intval($students['class_name']));
				}
			}
			if($schoolData['class_id'] > 0){
				$SEO = seo('加入');
			}else{
				$SEO = seo('发起');
			}
			
			$areas = $this->schoolService->get_area();
			$schools = $this->schoolService->get_lists();
			$setting = $this->load->service("admin/setting")->get();
			$this->load->librarys('View')->assign('classNumber',$setting['classNumber'])->assign('sid',$sid)->assign('iid',$iid)->assign('attachment_init',$attachment_init)->assign('schools',$schools)->assign('schoolData',$schoolData)->assign('areas',$areas)->assign('students',$students)->assign('SEO',$SEO)->assign('deps',$deps)->assign('count',$count)->display('validation');
		}
	}
	
	public function initiate(){		
		$sid = intval($_GET['sid']);
		if (empty($sid)) showmessage(lang('_error_action_'));
		$deposit = $this->load->service('member/member_deposit')->find(array('student_id'=>$sid));
		if (!$deposit) {
			showmessage("该孩子还没有加入托班",url('validation',array('sid'=>$sid)),1);
		}
		
		$initiate = $this->load->service('school/initiate')->get_initiate_by_id($deposit['iid']);
		if($initiate['isopen'] == 1){
			redirect(url('order/order/index',array('id'=>$initiate['id'],'sid'=>$sid)));
		}
		
		$schoolData = $this->load->service('school/school')->getSchoolOrClass(array('id'=>$initiate['school_id']),0,$deposit['class_id']);
		
		$count = $schoolData['count'];//$this->load->service('member/member_deposit')->count(array('iid'=>$deposit['iid']));
		$deps = $schoolData['deps'];//$this->load->service('member/member_deposit')->fetch(array('iid'=>$deposit['iid']));
		$SEO = seo('待缴费');
		$this->load->librarys('View')->assign('order',$deposit)->assign('mid',$this->member['id'])->assign('signPackage',$signPackage)->assign('deps',$deps)->assign('initiate',$initiate)->assign('count',$count)->assign('SEO',$SEO)->display('initiate');
		
		/*$sid = intval($_GET['sid']);
		$iid = intval($_GET['iid']);
		$ist = intval($_GET['ist']);
		$sqlmap = array('mid'=>$this->member['id']);
		$student = $this->load->table('member/member_student')->where(array('id'=>$sid))->find();
		if($sid > 0 && $student['mid'] == $this->member['id']){$sqlmap['id'] = $sid;}
		$setting = $this->load->service("admin/setting")->get();
		$students = $this->load->service('member/member_student')->get_lists($sqlmap);
		$schools = $this->schoolService->get_lists();
		$areas = $this->schoolService->get_area();
		$initiate = $this->load->service('school/initiate')->get_initiate_by_id($iid);
		$this->load->librarys('View')->assign('classNumber',$setting['classNumber'])->assign('initiate',$initiate)->assign('areas',$areas)->assign('students',$students)->assign('schools',$schools)->assign('ist',$ist)->assign('SEO',$SEO)->display('initiate');*/
	}
	
    public function point(){
        $data = $_POST;        
        $point = $this->load->table('member/point')->where(array('student_id'=>$data['student_id'],'teacher_id'=>$this->member['id'],'type'=>0,'_string'=>'start_time < '.time().' and end_time >'.time()))->find();
        if(count($point) > 0){
            $data['id'] = $point['id'];
            if($point['status'] == 1){
                $data['status'] = 0;
            }else{
                $data['status'] = 1;
            }
            $result = $this->pointService->edit_point($data);
            if(!$result){
				showmessage("点到失败");
            }else{
                showmessage("点到成功",url('index'),1,$data['status']);
            }
        }else{
            $t = time();
            $start = mktime(0,0,0,date("m",$t),date("d",$t),date("Y",$t));
            $end = mktime(23,59,59,date("m",$t),date("d",$t),date("Y",$t));
            $data['start_time'] = $start;
            $data['end_time'] = $end;
            $data['teacher_id'] = $this->member['id'];
            $data['type'] = 0;
            $result = $this->pointService->add_point($data);
            if(!$result){
				showmessage("点到失败");
            }else{
                showmessage("点到成功",url('index'),1,1);
            }
        }
    }
    
    public function scoring(){
        $data = $_POST;        
        $point = $this->load->table('member/scoring')->where(array('student_id'=>$data['student_id'],'teacher_id'=>$this->member['id'],'type'=>0,'_string'=>'start_time < '.time().' and end_time >'.time()))->find();
        if(count($point) > 0){
            $data['id'] = $point['id'];
            $result = $this->load->service('member/scoring')->edit_scoring($data);
            if(!$result){
                    showmessage($this->load->service('member/scoring')->error);
            }else{
                    showmessage("打分成功",url('index'),1);
            }
        }else{
            $t = time();
            $start = mktime(0,0,0,date("m",$t),date("d",$t),date("Y",$t));
            $end = mktime(23,59,59,date("m",$t),date("d",$t),date("Y",$t));

            $data['start_time'] = $start;
            $data['end_time'] = $end;
            $data['teacher_id'] = $this->member['id'];
            $data['type'] = 0;
            $result = $this->load->service('member/scoring')->add_scoring($data);
            if(!$result){
                    showmessage("打分失败");
            }else{
                    showmessage("打分成功",url('index'),1);
            }
        }
    }
	
	public function post(){		
		$student_id = intval($_POST['student_id']);
		$school_id = intval($_POST['school_id']);
		$iid = intval($_POST['iid']);
		$ist = intval($_POST['ist']);
		if($student_id == 0 || $school_id==0){
			showmessage("",url('initiate'),1);
		}else{
			if($iid > 0){
				showmessage("提交成功",url('/order/order/index',array('id'=>$iid,'sid'=>$student_id)),1);
			}else{
				$count = $this->load->service('school/initiate')->count(array('school_id'=>$school_id,'isopen'=>0,'status'=>1));
				if($count > 0 && $ist == 0){
					showmessage("提交成功",url('/member/index/ilist',array('school_id'=>$school_id,'sid'=>$student_id)),1);
				}else{
					$setting = $this->load->service("admin/setting")->get();
					$mid = $this->member['id'];
					$school = $this->schoolService->get_school_by_id($school_id);
					$data['mid'] = $mid;
					$data['student_id'] = $student_id;
					$data['school_id'] = $school_id;
					$data['school_name'] = $school['name'];
					$data['school_logo'] = $school['logo'];
					$data['area'] = $school['area'];
					$data['number'] = $setting['classNumber'];
					$data['school_price'] = $school['months_price'];
					$data['addtime'] = time();
					$result = $this->load->service('school/initiate')->add_initiate($data);
					if(!$result){
						showmessage($this->load->service('school/initiate')->error);
					}else{
						showmessage("提交成功",url('/order/order/index',array('id'=>$result)),1);
					}
				}
			}
		}
	}
}