<?php
class point_control extends cp_control
{
	public function _initialize() {
		parent::_initialize();
		if($this->member['id'] < 1) {
			redirect(url('cp/index'));
		}
		$this->service = $this->load->service('member/point');
		$this->scoringService = $this->load->service('member/scoring');
	}

	public function index() {
		$SEO = seo('评分');
		$sid = intval($_GET['sid']);
		$cid = $this->load->table('member/member_student')->where(array('id'=>$sid))->getField('class_id');
		$tid = $this->load->table('school/class')->where(array('id'=>$cid))->find();
		$teacher1 = $this->load->table('member/member')->fetch_by_id($tid['teacher1_id']);
		$teacher1['mealNum'] = 3;
		if($teacher1){
			$point = $this->load->table('member/scoring')->where(array('teacher_id'=>$tid['teacher1_id'],'student_id'=>$sid,'type'=>1,'_string'=>'start_time < '.time().' and end_time >'.time()))->find();
			if($point){
				$teacher1['mealNum'] = $point['mealNum'];
			}
		}
		if($tid['teacher2_id'] == $tid['teacher1_id']){
			$teacher2 = $this->load->table('member/member')->fetch_by_id($tid['teacher2_id']);
		}else{
			$teacher2 = '';
		}
		$t = mktime(0,0,0,date('m'),date('d')-date('w'),date('Y'));
		$start = mktime(0,0,0,date("m",$t),date("d",$t),date("Y",$t));
		$t = mktime(0,0,0,date('m'),date('d')+(6-date('w')),date('Y'));
		$end = mktime(23,59,59,date("m",$t),date("d",$t),date("Y",$t));    
        $point = $this->load->table('member/scoring')->where(array('teacher_id'=>$tid['teacher1_id'],'student_id'=>$sid,'type'=>1,'_string'=>'start_time = '.$start.' and end_time ='.$end))->count();
		$this->load->librarys('View')->assign('SEO',$SEO)->assign('point',$point)->assign('teacher2',$teacher2)->assign('teacher1',$teacher1)->assign('student_id',$sid)->display('point_index');
	}
	
	public function leave(){
		$SEO = seo('请假');
		$sid = intval($_GET['sid']);
		$week = array();
		$month = array('1'=>"一",'2'=>"二",'3'=>"三",'4'=>"四",'5'=>"五",'6'=>"六",'7'=>"七",'8'=>"八",'9'=>"九",'10'=>"十",'11'=>"十一",'12'=>"十二");
		$student = $this->load->table('member/member_student')->where(array('id'=>$sid))->find();
		$school = model('school')->where(array('id'=>$student['school_id']))->find();
		$bl = sprintf('%.2f',($school['price']/$school['months_price'])*100);
		for($i=0;$i<5;$i++){
			$t = strtotime(" +".$i." week");
			$starts = mktime(0,0,0,date('m',$t),date('d',$t)-date('w',$t),date('Y',$t));
			for($j=0;$j<7;$j++){
				$date = strtotime('+'.$j.' day',$starts);
				$week[$i][$j]['date'] = date('Y-m-d',$date);
				$week[$i][$j]['y'] = date('Y',$date);
				$week[$i][$j]['m'] = date('m',$date);
				$week[$i][$j]['mt'] = $month[intval(date('m',$date))];
				$week[$i][$j]['d'] = date('d',$date);
				$week[$i][$j]['leave'] = 0;
				$start = mktime(0,0,0,date("m",$date),date("d",$date),date("Y",$date));
				$end = mktime(23,59,59,date("m",$date),date("d",$date),date("Y",$date));
				$info = model('holidays')->where(array('year'=>$week[$i][$j]['y']))->find();
				$alldays = unserialize($info['alldays']);
				$days = explode(',',$alldays[intval($week[$i][$j]['m'])]);
				foreach($days as $v){
					if(intval($v)==intval($week[$i][$j]['d'])){
						$week[$i][$j]['leave'] = 1;
					}
				}
				$week[$i][$j]['point'] = $this->load->table('member/point')->where(array('student_id'=>$sid,'type'=>1,'_string'=>'start_time='.$start.' and end_time = '.$end))->count();					
			}		
		}
		
		$this->load->librarys('View')->assign('SEO',$SEO)->assign('bl',$bl)->assign('student_id',$sid)->assign('week',$week)->display('leave');
	}
	
	public function ajaxleave(){
		$data = $_POST;
		if(intval($data['student_id']) > 0){
			$dates = array_filter(explode(",",$data['dates']));
			$ldates = array_filter(explode(",",$data['ldates']));
			unset($data['dates']);unset($data['ldates']);
			$mid = $this->member['id'];
			$student = $this->load->table('member/member_student')->where(array('id'=>$data['student_id']))->find();
			if($student && $student['school_id'] > 0 && $student['class_id'] > 0){
				$money = '+'.$student['school']['price'];
				if(count($dates)>0){		
					foreach($dates as $k => $v){
						$t = strtotime($v);
						$start = mktime(0,0,0,date("m",$t),date("d",$t),date("Y",$t));
						$end = mktime(23,59,59,date("m",$t),date("d",$t),date("Y",$t));
						$point = $this->load->table('member/point')->where(array('student_id'=>$data['student_id'],'type'=>1,'_string'=>'start_time='.$start.' and end_time = '.$end))->find();			
						if(count($point) > 0){
									
						}else{
							$data['mid'] = $mid;
							$data['start_time'] = $start;
							$data['end_time'] = $end;
							$data['type'] = 1;
							$result = $this->service->add_point($data);
							/*if($result){
								$field = "money";
								$msg = $v.$student['truename']."请假退款";
								$this->load->table('member')->where(array('id' => $mid))->setField($field, array("exp", $field.$money));
								$_member = $this->load->table('member')->setid($mid)->output();
								$log_info = array(
									'mid'      => $mid,
									'value'    => $money,
									'ltype'     => 1,
									'type'     => $field,
									'msg'      => $msg,
									'dateline' => TIMESTAMP,
									'admin_id' => (defined('IN_ADMIN')) ? ADMIN_ID : 0,
									'money_detail' => json_encode(array($field => sprintf('%.2f' ,$_member[$field])))
								);
								$this->load->table('member_log')->update($log_info);
							}*/
						}
					}
				}
				if(count($ldates)>0){
					foreach($ldates as $k => $v){
						$t = strtotime($v);
						$start = mktime(0,0,0,date("m",$t),date("d",$t),date("Y",$t));
						$end = mktime(23,59,59,date("m",$t),date("d",$t),date("Y",$t));
						$point = $this->load->table('member/point')->where(array('student_id'=>$data['student_id'],'type'=>1,'_string'=>'start_time='.$start.' and end_time = '.$end))->find();			
						if(count($point) > 0){
							$result = $this->service->delete_point($point['id']);
						}
					}
				}
				showmessage("请假已更新",url('/member/index/index'),1);
			}else{
				showmessage("请假孩子不存在或还没加入托班");
			}
		}else{
			showmessage("参数错误");
		}
	}
	
	public function ajaxscoring(){
        $data = $_POST;    
		$t = mktime(0,0,0,date('m'),date('d')-date('w'),date('Y'));
		$start = mktime(0,0,0,date("m",$t),date("d",$t),date("Y",$t));
		$t = mktime(0,0,0,date('m'),date('d')+(6-date('w')),date('Y'));
		$end = mktime(23,59,59,date("m",$t),date("d",$t),date("Y",$t));    
        $point = $this->load->table('member/scoring')->where(array('teacher_id'=>$data['teacher_id'],'student_id'=>$data['student_id'],'type'=>1,'_string'=>'start_time = '.$start.' and end_time ='.$end))->count();
        if($point > 0){
            showmessage("打分成功",url('index'),1);
        }else{
            $data['start_time'] = $start;
            $data['end_time'] = $end;
            $data['type'] = 1;
            $result = $this->load->service('member/scoring')->add_scoring($data);
            if(!$result){
                showmessage("打分失败");
            }else{
                showmessage("打分成功",url('index'),1);
            }
        }
    }
}