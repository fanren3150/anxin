<?php
class student_control extends cp_control
{
    public function _initialize() {
        parent::_initialize();
        if($this->member['id'] < 1) {
                redirect(url('cp/index'));
        }
        $this->service = $this->load->service('member/member_student');
    }

    public function index() {				
        $sqlmap = array();
        $sqlmap['mid'] = $this->member['id'];
        $students = $this->service->get_lists($sqlmap);
        $SEO = seo('校内紧急联络人');
        $this->load->librarys('View')->assign('students',$students)->assign('SEO',$SEO)->display('member_student');
    }

    public function add() {
        helper('attachment');
        if(checksubmit('dosubmit')) {
            $_POST['mid'] = $this->member['id'];
            $result = $this->service->update($_POST);
            if($result === FALSE) {
                showmessage($this->service->error);
            }
            showmessage(lang('_operation_success_'), url('/member/index/index'), 1);
        } else {
            $SEO = seo('添加孩子');
			$areas = $this->load->service('school/school')->get_area();
            $attachment_init = attachment_init(array('module'=>'member', 'path' => 'member', 'mid' => $this->member['id'],'allow_exts'=>array('bmp','jpg','jpeg','gif','png')));
            $this->load->librarys('View')->assign('attachment_init',$attachment_init)->assign('SEO',$SEO)->assign('areas',$areas)->assign('type',1)->display('student_edit');
        }
    }

    public function edit() {
        helper('attachment');
        if(checksubmit('dosubmit')) {			
			$ids = $_POST['id'];
			$truename = $_POST['truename'];
			$teacher_name = $_POST['teacher_name'];
			$teacher_mobile = $_POST['teacher_mobile'];
			echo "<pre>";
			foreach($ids as $k => $v){
				$data = array();
				$data['id'] = $v;
				$data['truename'] = $truename[$k];
				$data['teacher_name'] = $teacher_name[$k];
				$data['teacher_mobile'] = $teacher_mobile[$k];	
				$this->service->update($data);
			}
            showmessage(lang('_operation_success_'), url('/member/index/index'), 1);
        }
    }
	
	public function head(){
		$student_id = $_POST['student_id'];	
		$head = $_POST['head'];
		$x = (int) $_GET['x'];
		$y = (int) $_GET['y'];
		$w = (int) $_GET['w'];
		$h = (int) $_GET['h'];
		if(is_file($head) && file_exists($head)) {
			$ext = strtolower(pathinfo($head, PATHINFO_EXTENSION));
			$name = basename($head, '.'.$ext);
			$dir = dirname($head);
			if(in_array($ext, array('gif','jpg','jpeg','bmp','png'))) {
				$name = $name.'_crop_200_200.'.$ext;
				$file = $dir.'/'.$name;
				$this->image_center_crop($head,200,200,$file);
				if($student_id > 0){
				$data = array();
				$data['id'] = $student_id;
				$data['head'] = $file;
				$result = $this->service->update($data);
				}
				showmessage(lang('change_head_portrait_success','member/language'),'',1,$file);
			} else {
				showmessage(lang('illegal_image','member/language'),'',0);
			}
		}
		
		showmessage(lang('_operation_success_'), url('/member/index/index'), 1);
	}
	
	function image_center_crop($source, $width, $height, $target)
	{
		if (!file_exists($source)) return false;
		/* 根据类型载入图像 */
		switch (exif_imagetype($source)) {
			case IMAGETYPE_JPEG:
				$image = imagecreatefromjpeg($source);
				break;
			case IMAGETYPE_PNG:
				$image = imagecreatefrompng($source);
				break;
			case IMAGETYPE_GIF:
				$image = imagecreatefromgif($source);
				break;
		}
		if (!isset($image)) return false;
		/* 获取图像尺寸信息 */
		$target_w = $width;
		$target_h = $height;
		$source_w = imagesx($image);
		$source_h = imagesy($image);
		/* 计算裁剪宽度和高度 */
		$judge = (($source_w / $source_h) > ($target_w / $target_h));
		$resize_w = $judge ? ($source_w * $target_h) / $source_h : $target_w;
		$resize_h = !$judge ? ($source_h * $target_w) / $source_w : $target_h;
		$start_x = $judge ? ($resize_w - $target_w) / 2 : 0;
		$start_y = !$judge ? ($resize_h - $target_h) / 2 : 0;
		/* 绘制居中缩放图像 */
		$resize_img = imagecreatetruecolor($resize_w, $resize_h);
		imagecopyresampled($resize_img, $image, 0, 0, 0, 0, $resize_w, $resize_h, $source_w, $source_h);
		$target_img = imagecreatetruecolor($target_w, $target_h);
		imagecopy($target_img, $resize_img, 0, 0, $start_x, $start_y, $resize_w, $resize_h);
		/* 将图片保存至文件 */
		if (!file_exists(dirname($target))) mkdir(dirname($target), 0777, true);
		switch (exif_imagetype($source)) {
			case IMAGETYPE_JPEG:
				imagejpeg($target_img, $target);
				break;
			case IMAGETYPE_PNG:
				imagepng($target_img, $target);
				break;
			case IMAGETYPE_GIF:
				imagegif($target_img, $target);
				break;
		}
		return boolval(file_exists($target));
	}
	
    public function delete() {
        $id = (int) $_GET['id'];
        if($id < 1) showmessage(lang('_param_error_'));
        $result = $this->service->delete_by_id($id);
        if($result === FALSE) {
                showmessage($this->service->error);
        }
        showmessage(lang('_operation_success_'), url('index'), 1);
    }

    /* 设置默认 */
    public function set_default() {
        $id = (int) $_GET['id'];
        if($id < 1) {
                showmessage(lang('_param_error_'));
        }
        $r = $this->service->mid($this->member['id'])->fetch_by_id($id);
        if(!$r) showmessage(lang('_valid_access_'));
        $result = $this->service->set_default($id, $this->member['id']);
        if($result === FALSE) {
                showmessage($this->service->error);
        }
        showmessage(lang('_operation_success_'), url('index'), 1);
    }

    public function ajax_district(){
        $id = (int) $_GET['id'];
        $result = $this->load->service('admin/district')->get_children($id);
        $this->load->librarys('View')->assign('result',$result);
        $result = $this->load->librarys('View')->get('result');
        echo json_encode($result);
    }
}