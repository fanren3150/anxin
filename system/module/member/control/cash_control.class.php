<?php
hd_core::load_class('init', 'admin');
class cash_control extends  init_control
{
	public function _initialize() {
		parent::_initialize();
		helper('attachment');
	}
	
    public function index() {
		$sqlmap = array();
		//条件查询用户
		$sqlmap['ltype']=2;
		if(isset($_GET['keyword']) && !empty($_GET['keyword'])) {
            $sql_map['store_name|true_name|phone'] = array("LIKE", '%'.$_GET['keyword'].'%');
			$_map1 = model('store')->where($sql_map)->getField('mid',true);
			$_map1 = implode(',',$_map1);
			$sqlmap['mid'] = array("IN", $_map1);
		}
		
        if(isset($_GET['isagent']) && $_GET['isagent'] != -1) {
            $sqlmap['status'] = array("LIKE", '%'.$_GET['isagent'].'%');
        }
		//查询提现记录
		$limit = (isset($_GET['limit']) && is_numeric($_GET['limit'])) ? $_GET['limit'] : 20;
		
		$count = model('member_log')->where($sqlmap)->count();
		$member_log = model('member_log')->where($sqlmap)->page($_GET['page'])->limit($limit)->order('id desc')->select();
		
		//用户名称
		foreach($member_log as $key => $val){
			$member_log[$key]['name'] = model('member')->where(array('mid'=>$val['mid']))->getField('username');
		}
		//统计
        $ydk = model('member_log')->where(array('ltype'=>2,'status'=>'1'))->sum('value');
        $ddk = model('member_log')->where(array('ltype'=>2,'status'=>'0'))->sum('value');
        $dksb = model('member_log')->where(array('ltype'=>2,'status'=>'2'))->sum('value');
        if (!$ydk){
            $ydk = '0.00';
        }
        if (!$ddk){
            $ddk = '0.00';
        }
        if (!$dksb){
            $dksb = '0.00';
        }
		$pages = $this->admin_pages($count, $limit);
		include $this->admin_tpl('cash_info');
	}
    
	//删除
	public function delete(){
		if(empty($_GET['formhash']) || $_GET['formhash'] != FORMHASH) {
            showmessage('_TOKEN_ERROR_',url('index'),0);
        }
		$ids = (array) $_GET['id'];
		$ids= implode(',',$ids);
		$result=model('member_log')->where(array('id'=>array('IN',$ids)))->delete();
		if(!$result){
			showmessage(model('store')->error);
		}
		showmessage('操作成功',url('index'),1);
    }
	
	//状态
   	public function togglestate() {
	   if(empty($_GET['formhash']) || $_GET['formhash'] != FORMHASH) {
            showmessage('_TOKEN_ERROR_',url('index'),0);
        }
        $log_ids = $ids = (array) $_GET['id'];
		$ids= implode(',',$ids);
		$status=intval($_GET["status"]);
		$data['status'] = $status;
		$result = model('member_log')->where(array('id'=>array('in',$ids)))->save($data);
		if(!$result){
			showmessage('操作失败',url('index'),1);
		}else{
			if($status != 1){
				foreach($log_ids as $k => $v){
					$log = model('member_log')->where(array('ltype'=>2,'id'=>$v))->find();
					if($log){
						$this->load->service('member/member')->change_account($log['mid'],'money',$log['value'],'提现打款失败,退回提现金额');			
					}
				}
				showmessage('操作成功2',url('index'),1);
			}else{
				showmessage('操作成功',url('index'),1);
			}
		}
    }
}