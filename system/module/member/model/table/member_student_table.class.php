<?php
class member_student_table extends table
{
	protected $_validate = array(
		//array('username', 'require', '用户名不能为空'),
		//array('password', 'require', '登陆密码不能为空'),
		//array('pwdconfirm', 'require', '确认密码不能为空'),
		//array('mobile', 'mobile', '手机号格式错误'),
		//array('email', 'email', '电子邮箱格式错误'),
	);

	protected $_auto = array(
		//array(填充字段,填充内容,[填充条件,附加规则])
		array('register_time', TIMESTAMP, 1, 'string'),
		array('register_ip', 'get_client_ip', 1, 'function'),
	);

	protected $_where = array();
	protected $_result = array();

	public function fetch_by_id($id, $field = null) {
		if($id < 1) {
			$this->error = lang('_param_error_');
			return false;
		}
		$rs = $this->find($id);
		return (!is_null($field)) ? $rs[$field] : $rs;
	}

    public function _after_select($result, $options) {
        $members = array();
        foreach($result AS $r) {
            $r['parents'] = $this->load->table('member/member')->fetch_by_id($r['mid']);
            $r['school'] = $this->load->table('school/school')->where(array('id'=>$r['school_id']))->find();
            $scoring = $this->load->table('member/scoring')->where(array('student_id'=>$r['id'],'type'=>0,'_string'=>'start_time < '.time().' and end_time >'.time()))->order('addtime desc')->find();
            $r['iscoring'] = 0;
            if($scoring['start_time'] < time() && $scoring['end_time'] > time()){
                $r['iscoring'] = 1;
            }
            if(!$scoring){
                $scoring['mealNum'] = 3;
                $scoring['bedNum'] = 3;
                $scoring['disciplineNum'] = "棒棒哒~";
                $scoring['addtime'] = time();
            }
            $scoring['addtime'] = date("Y-m-d",$scoring['addtime']);
            $point = $this->load->table('member/point')->where(array('student_id'=>$r['id'],'status'=>1,'type'=>0,'_string'=>'start_time < '.time().' and end_time >'.time()))->count();
            $leave = $this->load->table('member/point')->where(array('student_id'=>$r['id'],'status'=>1,'type'=>1,'_string'=>'start_time < '.time().' and end_time >'.time()))->count();
            $cleave = $this->load->table('member/point')->where(array('student_id'=>$r['id'],'status'=>1,'type'=>1,'_string'=>'start_time < '.(time()+86400).' and end_time >'.(time()+86400)))->count();
            $r['scoring'] = $scoring;
            $r['point'] = $point;
            $r['leave'] = $leave;
			$r['cleave'] = $cleave;
            $members[$r['id']] = $r;
        }
        return $members;
    }
	
	public function _after_find($result, $options) {
        $r = $result;
        $r['school'] = $this->load->table('school/school')->where(array('id'=>$r['school_id']))->find();
        $r['class'] = $this->load->table('school/class')->where(array('id'=>$r['class_id']))->find();
        $r['user'] = $this->load->table('member/member')->fetch_by_id($r['mid']);
        $this->data = $r;
        return $this->data;
    }
	
    public function setid($id) {
            $this->_where['mid'] = $id;
            $this->_result = $this->fetch_by_id($id);
            return $this;
    }

    public function output() {
            return $this->_result;
    }
}