<?php
class member_deposit_table extends table {

    protected $where = array();
	protected $_validate = array(
       
    );

    //自动完成
    protected $_auto = array(
    	
    );

    public function _initialize() {
    }
	
	public function _after_find($result, $options) {
        $r = $result;	
		//$r['_user'] = $this->load->table('member/member')->fetch_by_id($r['mid']);
		$r['_student'] = $this->load->table('member/member_student')->fetch_by_id($r['student_id']);
		$r['_school'] = $this->load->table('school/school')->fetch_by_id($r['school_id']);
		$r['avatar'] = $r['_student']['user']['avatar'];
        $this->data = $r;
        return $this->data;
    }
	
    public function _after_select($deps, $options) {
		$members = array();
        foreach ($deps as $k => $r) {
            //$r['_user'] = $this->load->table('member/member')->fetch_by_id($r['mid']);
			$r['_student'] = $this->load->table('member/member_student')->fetch_by_id($r['student_id']);
			$r['_school'] = $this->load->table('school/school')->fetch_by_id($r['school_id']);
			$r['avatar'] = $r['_student']['user']['avatar'];
			$members[$r['id']] = $r;
        }
        return $members;
    }
}