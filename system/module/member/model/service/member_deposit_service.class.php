<?php
class member_deposit_service extends service
{
	protected $sqlmap = array();

	public function _initialize(){
		$this->table = $this->load->table('member_deposit');
	}

	/**
	 * @param  array 	sql条件
	 * @param  integer 	条数
	 * @param  integer 	页数
	 * @param  string 	排序
	 * @return [type]
	 */
	public function fetch($sqlmap = array(), $limit, $page, $order = "id desc") {
		$lists = $this->table->where($sqlmap)->limit($limit)->page($page)->order($order)->select();
		if($lists===false){
			$this->error = lang('_param_error_');
			return false;
		}
		return $lists;
	}
	/**
	 * @param  array 	sql条件
	 * @param  integer 	读取的字段
	 * @return [type]
	 */
	public function find($sqlmap = array(), $field = "") {
		$result = $this->table->where($sqlmap)->field($field)->find();
		if($result===false){
			$this->error = $this->table->getError();
			return false;
		}
		return $result;
	}
	
	public function count($sqlmap = array()){
        $result = $this->table->where($sqlmap)->count();
        if($result === false){
            $this->error = $this->table->getError();
            return false;
        }
        return $result;
    }
	
	public function add_deposit($params){
        $data = $this->table->create($params);
        $result = $this->table->add($data);
		if($result === false){
            $this->error = $this->table->getError();
            return false;
        }
        return $result;
    }
}