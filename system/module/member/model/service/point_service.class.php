<?php
class point_service extends service {
    protected $result;

    public function _initialize() {
        $this->model = $this->load->table('member/point');
	}

    public function add_point($params){
        $data = $this->model->create($params);
        return $this->model->add($data);
    }
    
    public function edit_point($params){
        if((int)$params['id'] < 1){
            $this->error = lang('_param_error_');
            return FALSE;
        }
        $result = $this->model->update($params);
    	if($result === FALSE){
            $this->error = $this->model->getError();
            return FALSE;
    	}
    	return $result;
    }
	
	public function delete_point($params){
		if(empty($params)){
			$this->error = lang('_param_error_');
			return FALSE;
		}
		
		$sqlmap = array();
		$sqlmap['id'] = array('IN',$params);
		$result = $this->model->where($sqlmap)->delete();
		if(!$result){
    		$this->error = lang('_operation_fail_');
    		return FALSE;
    	}
    	return $result;
	}
	
	public function get_lists($sqlmap,$page,$limit){
        $scorings = $this->load->table('member/point')->where($sqlmap)->page($page)->order('id DESC')->limit($limit)->select();
        $lists = array();
        foreach ($scorings AS $scoring) {
			$student = $this->load->table('member/member_student')->where(array('id'=>$scoring['student_id']))->find();
            $lists[] = array(
                'id' => $scoring['id'],
                'type' => $scoring['type'],
                'student_id' => $scoring['student_id'],
				'student_name' => $student['truename'],
				'parents_name' => $student['user']['username'],
				'school_name' => $this->load->table('school/school')->where(array('id'=>$student['school_id']))->getField('name'),
				'class_name' => $this->load->table('school/class')->where(array('id'=>$student['class_id']))->getField('name'),
                'start_time' => $scoring['start_time'],
                'end_time' => $scoring['end_time'],
				'pointTime' => date('Y-m-d', $scoring['start_time']),
                'addtime' => date('Y-m-d H:i:s', $scoring['addtime'])
            );
        }
        return $lists;
    }
    /**
     * @param  array    sql条件
     * @param  integer  读取的字段
     * @return [type]
     */
    public function find($sqlmap = array(), $field = "", $order = "addtime desc") {
        $result = $this->model->where($sqlmap)->field($field)->order($order)->find();
        if($result===false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
	
	/**
     * 条数
     * @param  [arra] sql条件
     * @return [type]
     */
    public function count($sqlmap = array()){
        $result = $this->model->where($sqlmap)->count();
        if($result === false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
}