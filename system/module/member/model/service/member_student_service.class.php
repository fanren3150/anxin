<?php
class member_student_service extends service {
    protected $result;

    public function _initialize() {
        $this->model = $this->load->table('member/member_student');
	}

    
    /**
     * 查询单个会员信息
     * @param int $id
     * @return mixed
     */
    public function fetch_by_id($id) {
        $r = $this->model->find($id);
        if(!$r) {
            $this->error = '_select_not_exist_';
            return FALSE;
        }
        return $r;
    }
    /**
     * 删除指定会员
     * @param type $id
     */
    public function delete_by_id($id) {
        $ids = (array) $id;
        foreach($ids AS $id) {
            if($this->model->delete($id)) {
                
            }
        }
        return TRUE;
    }

    /**
     * [add_student 增加会员]
     * @param [type] $params [description]
     */
    public function add_student($params){
        $data = $this->model->create($params);
        return $this->model->add($data);
    }

    public function get_lists($sqlmap,$page,$limit){
        $students = $this->load->table('member/member_student')->where($sqlmap)->page($page)->order('id DESC')->limit($limit)->select();
        $lists = array();
        foreach ($students AS $student) {
            $head = $student['head']?$student['head']:"./template/default/statics/images/member/default_head.png";
            $lists[$student['id']] = array(
                'id' => $student['id'],
                'mid' => $student['mid'],
                'school_id' => $student['school_id'],
                'school_name' => $this->load->table('school/school')->where(array('id' => $student['school_id']))->getField('name'),
                'class_id' => $student['class_id'],
                'class_name' => $this->load->table('class/class')->where(array('id' => $student['class_id']))->getField('name'),
				'classname' => $student['class_name'],
				'area' => $student['area'],
                'truename' => $student['truename'],
                'head' => $head,
                'teacher_name' => $student['teacher_name'],
                'teacher_mobile' => $student['teacher_mobile'],
                'parents' => $student['parents'],
                'scoring' => $student['scoring'],
                'point' => $student['point'],
				'roll' => $student['point']==1?"已到":"未到",
                'leave' => $student['leave'],
				'leaves' => $student['cleave']==1?"请假":"未请",
                'iscoring' => $student['iscoring'],
				'avatar' => $student['parents']['avatar'],
                'register_time' => date('Y-m-d H:i:s', $student['register_time'])
            );
        }
        return $lists;
    }
    /**
     * [update 更新数据]
     * @param  [type] $params [参数]
     * @return [type]         [description]
     */
    public function update($params ,$bool = true){
        if(empty($params)){
            $this->error = lang('_params_error_');
            return false;
        }
        $result = $this->model->update($params ,$bool);
        if($result === false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
   
    /**
     * 条数
     * @param  [arra]   sql条件
     * @return [type]
     */
    public function count($sqlmap = array()){
        $result = $this->model->where($sqlmap)->count();
        if($result === false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
    
    /**
     * @param  array    sql条件
     * @param  integer  读取的字段
     * @return [type]
     */
    public function find($sqlmap = array(), $field = "") {
        $result = $this->model->where($sqlmap)->field($field)->find();
        if($result===false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
}