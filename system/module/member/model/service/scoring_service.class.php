<?php
class scoring_service extends service {
    protected $result;

    public function _initialize() {
        $this->model = $this->load->table('member/scoring');
	}
	
	public function get_lists($sqlmap,$page,$limit){
        $scorings = $this->load->table('member/scoring')->where($sqlmap)->page($page)->order('id DESC')->limit($limit)->select();
        $lists = array();
        foreach ($scorings AS $scoring) {
			$student = $this->load->table('member/member_student')->where(array('id'=>$scoring['student_id']))->find();
			$discipline = array('喧哗',"追赶","打架","脏话","未准时到班","拿动他人物品","乱扔垃圾","恶作剧");
			$disciplineNum = explode(',',$scoring['disciplineNum']);
			$other ='';
			foreach($disciplineNum as $v){
				if(!in_array($v,$discipline)){
					$other .= $v;	
				}
			}
            $lists[] = array(
                'id' => $scoring['id'],
                'type' => $scoring['type'],
                'teacher_id' => $scoring['teacher_id'],
				'teacher_name' => $this->load->table('member/member')->where(array('id'=>$scoring['teacher_id']))->getField('truename'),
                'student_id' => $scoring['student_id'],
				'student_name' => $student['truename'],
				'parents_name' => $this->load->table('member/member')->where(array('id'=>$student['mid']))->getField('truename'),
				'school_name' => $this->load->table('school/school')->where(array('id'=>$student['school_id']))->getField('name'),
				'class_name' => $this->load->table('school/class')->where(array('id'=>$student['class_id']))->getField('name'),
                'mealNum' => $scoring['mealNum'],
                'bedNum' => $scoring['bedNum'],
				'disciplineNum' => $scoring['disciplineNum'],
                'start_time' => $scoring['start_time'],
                'end_time' => $scoring['end_time'],
				'noise' => in_array("喧哗",$disciplineNum)?"F":"",
				'chase' => in_array("追赶",$disciplineNum)?"F":"",
				'fight' => in_array("打架",$disciplineNum)?"F":"",
				'dirty' => in_array("脏话",$disciplineNum)?"F":"",
				'nottime' => in_array("未准时到班",$disciplineNum)?"F":"",
				'takeGoods' => in_array("拿动他人物品",$disciplineNum)?"F":"",
				'litter' => in_array("乱扔垃圾",$disciplineNum)?"F":"",
				'prank' => in_array("恶作剧",$disciplineNum)?"F":"",
				'other' => $other,
                'addtime' => date('Y-m-d H:i', $scoring['addtime'])
            );
        }
        return $lists;
    }
	
    public function add_scoring($params){
        $data = $this->model->create($params);
        return $this->model->add($data);
    }
    
    public function edit_scoring($params){
        if((int)$params['id'] < 1){
            $this->error = lang('_param_error_');
            return FALSE;
        }
        $result = $this->model->update($params);
    	if($result === FALSE){
            $this->error = $this->model->getError();
            return FALSE;
    	}
    	return $result;
    }
    /**
     * @param  array    sql条件
     * @param  integer  读取的字段
     * @return [type]
     */
    public function find($sqlmap = array(), $field = "", $order = "addtime desc") {
        $result = $this->model->where($sqlmap)->field($field)->order($order)->find();
        if($result===false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
	
	/**
     * 条数
     * @param  [arra] sql条件
     * @return [type]
     */
    public function count($sqlmap = array()){
        $result = $this->model->where($sqlmap)->count();
        if($result === false){
            $this->error = $this->model->getError();
            return false;
        }
        return $result;
    }
}