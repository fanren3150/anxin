<?php include template('header','admin');?>
<script type="text/javascript" src="<?php echo __ROOT__ ?>statics/js/admin/class_action.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		class_action.init();
	});
</script>
<div class="fixed-nav layout">
	<ul>
		<li class="first">孩子管理<a id="addHome" title="添加到首页快捷菜单">[+]</a></li>
		<li class="spacer-gray"></li>
	</ul>
	<div class="hr-gray"></div>
</div>

<div class="content padding-big have-fixed-nav">
	<div class="tips margin-tb">
		<div class="tips-info border">
			<h6>温馨提示</h6>
			<a id="show-tip" data-open="true" href="javascript:;">关闭操作提示</a>
		</div>
		<div class="tips-txt padding-small-top layout">
			<p>- 通过孩子管理，你可以进行查看、编辑孩子资料以及删除孩子等操作</p>
			<p>- 你可以根据条件搜索孩子，然后选择相应的操作</p>
		</div>
	</div>

	<div class="member-list-search clearfix">
	<form action="" method="get">
    	<div class="form-box form-layout-rank clearfix border-bottom-none" style="width:865px">
		<?php echo form::input('text', 'keyword', $_GET['keyword'], '搜索', '', array('placeholder' => '输入孩子名/所在区域均可搜索'));?>
        <?php echo form::input('text', 'jkeyword', $_GET['jkeyword'], '', '', array('placeholder' => '输入家长名/家长电话均可搜索'));?>
        <?php  $school_keys = array_keys($slists);$first_key = current($school_keys);?>
		<?php echo form::input('select','school_id',$first_key,'学校：','',array('items' => $slists)); ?>        
        <?php  $school_keys = array_keys($clists);$first_key = current($school_keys);?>
		<?php echo form::input('select','class_id',$first_key,'班级：','',array('items' => $clists)); ?>
		</div>
		<input type="hidden" name="m" value="member">
		<input type="hidden" name="c" value="member">
		<input type="hidden" name="a" value="student">
		<input class="button bg-sub fl" type="submit" value="查询">
	</form>
	</div>

	<div class="table-work border margin-tb">
		<div class="border border-white tw-wrap">
            <a href="javascript:;" onClick="class_action.classes('<?php echo url("member/member/classes"); ?>');"><i class="ico_add"></i>分班</a>
            <div class="spacer-gray"></div>
			<a data-message="是否确定删除所选？" href="<?php echo url('delete')?>" data-ajax='ids'><i class="ico_delete"></i>删除</a>
			<div class="spacer-gray"></div>
            <a data-confirm="是否确定要执行此操作？本次操作不可恢复，请慎重。" href="<?php echo url('tk')?>"><i class="ico_add"></i>一键退款</a>
			<div class="spacer-gray"></div>
		</div>
	</div>
	<?php echo runhook('admin_member_lists_extra')?>
	<div class="table-wrap member-info-table">
		<div class="table resize-table paging-table check-table border clearfix">
			<div class="tr">
				<span class="th check-option" data-resize="false"><span><input id="check-all" type="checkbox" /></span></span>
				<?php foreach ($lists['th'] AS $th) {?>
				<span class="th" data-width="<?php echo $th['length']?>">
					<span class="td-con"><?php echo $th['title']?></span>
				</span>
				<?php }?>
				<span class="th" data-width="20" style="width: 183px;"><span class="td-con">操作</span></span>
			</div>
			<?php foreach ($lists['lists'] AS $list) {?>
				<div class="tr">
					<span class="td check-option"><input type="checkbox" name="id" value="<?php echo $list['id']?>" /></span>					
					<span class="td">
						<span class="td-con"><?php echo $list['truename'];?></span>
					</span>					
					<span class="td">
						<span class="td-con" style="line-height:20px;"><?php echo $list['parents']['username'];?><br><?php echo $list['parents']['mobile'];?></span>
					</span>					
					<span class="td">
						<span class="td-con"><?php echo $list['school_name'];?></span>
					</span>					
					<span class="td">
						<span class="td-con"><?php echo $list['class_name'];?></span>
					</span>					
					<span class="td">
						<span class="td-con"><?php echo $list['roll'];?></span>
					</span>
					<span class="td">
						<span class="td-con" style="line-height:20px;">
						<?php if($list['iscoring'] > 0){?>
						<?php echo $list['scoring']['mealNum'];?><br><?php echo $list['scoring']['bedNum'];?><br><?php echo $list['scoring']['disciplineNum'];?>
						<?php }else{?>
						未点评
						<?php }?>
						</span>						
					</span>
					<span class="td">
						<span class="td-con"><?php echo $list['leaves'];?></span>
					</span>
					<span class="td">
						<span class="td-con"><?php echo $list['classname'];?></span>
					</span>
					<span class="td">
						<span class="td-con"><?php echo $list['teacher_name'];?></span>
					</span>
					<span class="td">
						<span class="td-con"><?php echo $list['teacher_mobile'];?></span>
					</span>
					<span class="td">
					<span class="td-con">
					<a href="<?php echo url('/member/member/leave',array('id'=>$list['id']))?>">请假</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo url('/member/member/score',array('id'=>$list['id']))?>">打分</a>&nbsp;&nbsp;&nbsp;<?php echo runhook('member_info_edit',$list['id']);?><a href="<?php echo url('student_edit', array('id' => $list['id'],'type'=>1)) ?>" class="member_update">编辑</a>&nbsp;&nbsp;&nbsp;<a href="<?php echo url('student_delete', array('ids[]' => $list['id'])); ?>" data-confirm="您确认要删除？">删除</a></span>
				</span>
				</div>
				<?php }?>
			
			<div class="paging padding-tb body-bg clearfix">
				<?php echo $pages;?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(window).load(function(){
		$(".table").resizableColumns();
		$(".paging-table").fixedPaging();
		$(".member-info-tip").hover(function(){
			$(this).children("span").show();
		},function(){
			$(this).children("span").hide();
		});
		$(".member-list-search .form-group").each(function(i){
			$(this).addClass("form-group-id"+(i+3));
		});
	})
	$(function(){
		var $val=$("input[type=text]").eq(1).val();
		$("input[type=text]").eq(1).focus().val($val);
	})
</script>
<?php include template('footer','admin');?>
