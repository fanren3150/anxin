<?php include template('header','admin');?>
<div class="fixed-nav layout">
	<ul>
		<li class="first">请假管理</li>
		<li class="spacer-gray"></li>
	</ul>
	<div class="hr-gray"></div>
</div>

<div class="content padding-big have-fixed-nav">
	<?php echo runhook('admin_member_lists_extra')?>
	<div class="table-wrap member-info-table">
		<div class="table resize-table paging-table check-table border clearfix">
			<div class="tr">
				<span class="th check-option" data-resize="false"><span><input id="check-all" type="checkbox" /></span></span>
				<?php foreach ($lists['th'] AS $th) {?>
				<span class="th" data-width="<?php echo $th['length']?>">
					<span class="td-con"><?php echo $th['title']?></span>
				</span>
				<?php }?>
			</div>
			<?php foreach ($lists['lists'] AS $list) {?>
				<div class="tr">
					<span class="td check-option"><input type="checkbox" name="id" value="<?php echo $list['id']?>" /></span>								
					<span class="td">
						<span class="td-con"><?php echo $list['student_name']."(".$list['parents_name'].")";?></span>
					</span>					
					<span class="td">
						<span class="td-con"><?php echo $list['school_name'];?></span>
					</span>					
					<span class="td">
						<span class="td-con"><?php echo $list['class_name'];?></span>
					</span>	
					<span class="td">
						<span class="td-con"><?php echo $list['pointTime'];?></span>
					</span>				
                    <span class="td">
						<span class="td-con"><?php echo $list['addtime'];?></span>
					</span>
				</div>
				<?php }?>
			
			<div class="paging padding-tb body-bg clearfix">
				<?php echo $pages;?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(window).load(function(){
		$(".table").resizableColumns();
		$(".paging-table").fixedPaging();
		$(".member-info-tip").hover(function(){
			$(this).children("span").show();
		},function(){
			$(this).children("span").hide();
		});
		$(".member-list-search .form-group").each(function(i){
			$(this).addClass("form-group-id"+(i+3));
		});
	})
	$(function(){
		var $val=$("input[type=text]").eq(1).val();
		$("input[type=text]").eq(1).focus().val($val);
	})
</script>
<?php include template('footer','admin');?>
