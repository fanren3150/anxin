<?php include $this->admin_tpl('header', 'admin'); ?>
<div class="fixed-nav layout">
	<ul>
		<li class="first">编辑资料</li>
		<li class="spacer-gray"></li>
	</ul>
	<div class="hr-gray"></div>
</div>
<div class="content padding-big have-fixed-nav">
<form action="<?php echo url('teacher_edit')?>" method="POST"  name="form-validate" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
<input type="hidden" name="type" value="<?php echo $_GET['type']; ?>">
<div class="edit-user-info">
	<div class="form-box clearfix">
		<?php echo form::input('text', 'truename', $member['truename'], '老师姓名：',''); ?>
		<?php echo form::input('text', 'username', $member['username'], '用户名：', '请输入用户名'); ?>
		<?php echo form::input('password', 'password','', '登录密码', '请输入登录密码'); ?>
		<?php echo form::input('text', 'mobile', $member['mobile'], '手机号码','请输入手机号码'); ?>
		<?php echo form::input('text', 'email', $member['email'], '电子邮箱', '请输入电子邮箱'); ?>
        <?php echo form::input('file','head', $member['head'], '头像：','请上传老师头像，用于前台展示。',array('preview'=>$member['head'])); ?>  
		<div class="form-group">
			<span class="label">角色切换:</span>
			<span class="desc">请选择角色</span>
			<div class="box">
				<select  class="input hd-input" name="group_id">
				<?php foreach ($group as $gr): ?>
				<?php if( $gr['id']==$member['group_id']){?>
				<option value="<?php echo $gr['id'] ?>" selected="selected"><?php echo $gr['name'] ?></option>
				<?php }else {
				?>
				<option value="<?php echo $gr['id'] ?>"><?php echo $gr['name'] ?></option>
				<?php
				}
				?>
				<?php endforeach ?>
				</select>
			</div>
		</div>
		<?php echo form::input('text', 'ident', $member['ident'], '身份证号码：',''); ?>        
		<?php echo form::input('text', 'educational', $member['educational'], '学历：',''); ?>
		<?php echo form::input('text', 'isEngage', $member['isEngage'], '儿童教育行业从业时长（年）：',''); ?>
		<?php echo form::input('text', 'city', $member['city'], '所在区：',''); ?>
		<?php echo form::input('text', 'school', $member['school'], '意向学校：',''); ?>    
		<?php echo form::input('radio', 'status', $member['status'], '老师状态:','',array('items' => array('1'=> '通过待岗','2'=>'申请中','3'=>'申请未通过'),'colspan' => 3)); ?>
    </div>
</div>

<div class="padding text-left ui-dialog-footer">
	<input type="submit" name="dosubmit" class="button bg-main" value="确定" />
	<input type="button" name="button" value="取消" class="button margin-left bg-gray" data-back="false"/>
</div>
</form>
</div>
<script type="text/javascript">
	$(function(){
		try {
			var dialog = top.dialog.get(window);
		} catch (e) {
			alert('请勿非法访问');
			return;
		}

		var form_member_update = $("form[name='member_update']").Validform({
			ajaxPost:true,
			dragonfly:true,
			callback:function(ret) {
				dialog.title(ret.message);
				if(ret.status == 1) {
					setTimeout(function(){
						dialog.close(ret);
						dialog.remove();
					}, 1000);
				}
				return false;
			}
		})

		$('input[type=button]').live('click', function () {
			dialog.close();
			dialog.remove();
			return false;
		})
	})
</script>
</body>
</html>