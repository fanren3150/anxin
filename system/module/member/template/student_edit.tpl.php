<?php include $this->admin_tpl('header', 'admin'); ?>
<div class="fixed-nav layout">
	<ul>
		<li class="first">编辑资料</li>
		<li class="spacer-gray"></li>
	</ul>
	<div class="hr-gray"></div>
</div>
<div class="content padding-big have-fixed-nav">
<form action="<?php echo url('student_edit')?>" method="POST"  name="form-validate" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
<div class="edit-user-info">
	<div class="form-box clearfix">    
    <?php echo form::input('text', 'truename', $member['truename'], '孩子姓名：', '请输入孩子姓名'); ?>
	<?php  $school_keys = array_keys($clists);$first_key = $member['class_id'];?>
	<?php echo form::input('select','class_id',$first_key,'午托班：','',array('items' => $clists)); ?>
	<?php echo form::input('radio', 'sex', $member['sex'], '性别:','',array('items' => array('1'=> '男','2'=>'女'),'colspan' => 2)); ?>
	<?php echo form::input('text', 'area', $member['area'], '区', '请输入所在区域'); ?>
	<?php  $school_keys = array_keys($slists);$first_key = $member['school_id'];?>
	<?php echo form::input('select','school_id',$first_key,'学校：','',array('items' => $slists)); ?>  
	<?php echo form::input('text', 'class_name', $member['class_name'], '班级：', '请输入班级'); ?>    
    <?php echo form::input('text', 'teacher_name', $member['teacher_name'], '班主任姓名', '请输入班主任姓名'); ?>
    <?php echo form::input('text', 'teacher_mobile', $member['teacher_mobile'], '班主任手机号码','请输入班主任手机号码'); ?>        
    <?php echo form::input('calendar','start_time',date('Y-m-d H:i:s',$member['start_time']),'开始时间：','托班有效的开始时间',array('format' => 'YYYY-MM-DD hh:mm:ss','datatype' => '*')); ?>
    <?php echo form::input('calendar','end_time',date('Y-m-d H:i:s',$member['end_time']),'结束时间：','托班有效的结束时间',array('format' => 'YYYY-MM-DD hh:mm:ss','datatype' => '*')); ?>
	<?php echo form::input('file','head', $member['head'], '头像：','请上传孩子，用于前台展示。',array('preview'=>$member['head'])); ?>
    </div>
</div>

<div class="padding text-left ui-dialog-footer">
	<input type="submit" name="dosubmit" class="button bg-main" value="确定" />
	<input type="button" name="button" value="取消" class="button margin-left bg-gray" data-back="false"/>
</div>
</form>
</div>
<script type="text/javascript">
	$(function(){
		try {
			var dialog = top.dialog.get(window);
		} catch (e) {
			alert('请勿非法访问');
			return;
		}

		var form_member_update = $("form[name='member_update']").Validform({
			ajaxPost:true,
			dragonfly:true,
			callback:function(ret) {
				dialog.title(ret.message);
				if(ret.status == 1) {
					setTimeout(function(){
						dialog.close(ret);
						dialog.remove();
					}, 1000);
				}
				return false;
			}
		})

		$('input[type=button]').live('click', function () {
			dialog.close();
			dialog.remove();
			return false;
		})
	})
</script>
</body>
</html>