<?php include template('header','admin');?>
<div class="fixed-nav layout">
	<ul>
		<li class="first">老师给孩子评分管理<a id="addHome" title="添加到首页快捷菜单">[+]</a></li>
		<li class="spacer-gray"></li>
	</ul>
	<div class="hr-gray"></div>
</div>

<div class="content padding-big have-fixed-nav">
	<div class="tips margin-tb">
		<div class="tips-info border">
			<h6>温馨提示</h6>
			<a id="show-tip" data-open="true" href="javascript:;">关闭操作提示</a>
		</div>
		<div class="tips-txt padding-small-top layout">
			<p>- 通过老师给孩子评分管理，你可以进行查看老师给孩子每天的评分</p>
		</div>
	</div>

	<form method="GET" action="">
			<input type="hidden" value="member" name="m" />
			<input type="hidden" value="member" name="c" />
			<input type="hidden" value="score" name="a" />
            <input type="hidden" value="<?php echo $_GET['id'];?>" name="id" />
            <input type="hidden" value="<?php echo $_GET['cid'];?>" name="cid" />
			<div class="member-comment-search clearfix">
				<div class="form-box clearfix border-bottom-none" style="width: 390px;">
					<div style="z-index: 4;" id="form-group-id1" class="form-group form-layout-rank group1">
						<span class="label">操作时间</span>
						<div class="box margin-none">
							<?php echo form::calendar('start',!empty($_GET['start']) ? $_GET['start']:'',array('format' => 'YYYY-MM-DD'))?>
						</div>
					</div>
					<div style="z-index: 3;" id="form-group-id2" class="form-group form-layout-rank group2">
						<span class="label">~</span>
						<div class="box margin-none">
							<?php echo form::calendar('end',!empty($_GET['end'])? $_GET['end']:'',array('format' => 'YYYY-MM-DD'))?>
						</div>
					</div>					
				</div>
				<input class="button bg-sub fl" value="查询" type="submit">
			</div>
			</form>
	<div class="table-wrap member-info-table">
		<div class="table resize-table paging-table check-table border clearfix">
			<div class="tr">
				<span class="th check-option" data-resize="false"><span><input id="check-all" type="checkbox" /></span></span>
				<?php foreach ($lists['th'] AS $th) {?>
				<span class="th" data-width="<?php echo $th['length']?>">
					<span class="td-con"><?php echo $th['title']?></span>
				</span>
				<?php }?>
			</div>
			<?php foreach ($lists['lists'] AS $list) {?>
				<div class="tr">
					<span class="td check-option"><input type="checkbox" name="id" value="<?php echo $list['id']?>" /></span>					
					<span class="td">
						<span class="td-con"><?php echo $list['class_name'];?></span>
					</span>					
					<span class="td">
						<span class="td-con"><?php echo $list['student_name'];?></span>
					</span>					
					<span class="td">
						<span class="td-con"><?php echo $list['mealNum'];?></span>
					</span>					
					<span class="td">
						<span class="td-con"><?php echo $list['bedNum'];?></span>
					</span>					
					<span class="td">
						<span class="td-con"><?php echo $list['noise'];?></span>
					</span>
					<span class="td">
						<span class="td-con"><?php echo $list['chase'];?></span>
					</span>
					<span class="td">
						<span class="td-con"><?php echo $list['fight'];?></span>
					</span>
                    <span class="td">
						<span class="td-con"><?php echo $list['dirty'];?></span>
					</span>
                    
                    <span class="td">
						<span class="td-con"><?php echo $list['nottime'];?></span>
					</span>
                    
                    <span class="td">
						<span class="td-con"><?php echo $list['takeGoods'];?></span>
					</span>
                    
                    <span class="td">
						<span class="td-con"><?php echo $list['litter'];?></span>
					</span>
                    
                    <span class="td">
						<span class="td-con"><?php echo $list['prank'];?></span>
					</span>
                    
                    <span class="td">
						<span class="td-con"><?php echo $list['other'];?></span>
					</span>
                    <span class="td">
						<span class="td-con"><?php echo $list['addtime'];?></span>
					</span>
				</div>
				<?php }?>
			
			<div class="paging padding-tb body-bg clearfix">
				<?php echo $pages;?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(window).load(function(){
		$(".table").resizableColumns();
		$(".paging-table").fixedPaging();
		$(".member-info-tip").hover(function(){
			$(this).children("span").show();
		},function(){
			$(this).children("span").hide();
		});
		$(".member-list-search .form-group").each(function(i){
			$(this).addClass("form-group-id"+(i+3));
		});
	})
	$(function(){
		var $val=$("input[type=text]").eq(1).val();
		$("input[type=text]").eq(1).focus().val($val);
	})
</script>
<?php include template('footer','admin');?>
