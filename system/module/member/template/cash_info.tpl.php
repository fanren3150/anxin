<?php include $this->admin_tpl('header', 'admin');?>
<style type="text/css">
.member-list-search .form-group{
	width:280px;
	}
.form-group .box .hd-input{ 
    width:138px;
}
</style>
		<div class="fixed-nav layout">
			<ul>
				<li class="first">提现记录<a id="addHome" title="添加到首页快捷菜单">[+]</a></li>
				<li class="spacer-gray"></li>
			</ul>
			<div class="hr-gray"></div>
		</div>
        <div class="content padding-big have-fixed-nav">
        <div class="member-list-search clearfix" >
                <form action="" method="get">
                    <div class="form-box form-layout-rank clearfix border-bottom-none" style="width:640px;">
                          <?php echo form::input('text', 'keyword', $_GET['keyword'], '搜索', '', array('placeholder' => '输入用户名称/真实姓名'));?>
						  <?php echo form::input('select','isagent',$_GET['isagent'] ? $_GET['isagent'] : -1,'状态','',array('items' => array('-1'=>'请选择', '0'=>'待打款','1'=>'已打款','2'=>'打款失败')))?>
					</div>
                    <input type="hidden" name="m" value="member">
                    <input type="hidden" name="c" value="cash">
                    <input type="hidden" name="a" value="index">
                    <input class="button bg-sub fl" type="submit" value="查询">
                </form>
	        </div>
            <div class="table-work border margin-tb">
				<div class="border border-white tw-wrap">
                    <div class="spacer-gray"></div>
                    <a data-message="是否确定删除所选？" href="<?php echo url('delete')?>"  data-ajax='id'><i class="ico_delete"></i>删除</a>
					<div class="spacer-gray"></div>
                    <a data-message="是否批量修改待打款" href="<?php echo url('togglestate',array('status'=>0))?>" data-ajax='id'><i class="ico_lock"></i>待打款</a>
                    <div class="spacer-gray"></div>
                    <a data-message="是否批量修改已打款" href="<?php echo url('togglestate',array('status'=>1))?>" data-ajax='id'><i class="ico_lock"></i>已打款</a>
                    <div class="spacer-gray"></div>
                    <a data-message="是否批量修改打款失败" href="<?php echo url('togglestate',array('status'=>2))?>" data-ajax='id'><i class="ico_lock"></i>打款失败</a>
                    <div class="spacer-gray"></div>
					<a ><i class="ico_lock"></i>已打款总额: <?php echo $ydk ?> </a>
                    <div class="spacer-gray"></div>
					<a ><i class="ico_lock"></i>待打款总额: <?php echo $ddk ?> </a>
                    <div class="spacer-gray"></div>
					<a ><i class="ico_lock"></i>失败总额: <?php echo $dksb ?> </a>
                    <div class="spacer-gray"></div>
                </div>
			</div>
			<div class="table-wrap member-info-table">
				<div class="table resize-table check-table paging-table border clearfix">
					<div class="tr">
						<span class="th check-option" data-resize="false" >
							<span><input id="check-all" type="checkbox"  /></span>
						</span>
					
                        <span class="th" data-width="10">
							<span class="td-con">用户名称</span>
						</span>
						<span class="th" data-width="10">
							<span class="td-con">变动金额</span>
						</span>
						 <span class="th" data-width="20">
							<span class="td-con">记录描述内容</span>
						 </span>
                        <span class="th" data-width="10">
							<span class="td-con">记录生成时间</span>
						</span>
                        <span class="th" data-width="5">
							<span class="td-con">类型</span>
						</span>
                        <span class="th" data-width="20">
							<span class="td-con">卡号（银行名称）</span>
						</span>
                        <span class="th" data-width="10">
							<span class="td-con">余额明细</span>
						</span>
                        <span class="th" data-width="5">
							<span class="td-con">状态</span>
						</span>

						<span class="th" data-width="10" >
							<span class="td-con">操作</span>
						</span>
					</div>
					<?php foreach ($member_log AS $log): ?>
					<div class="tr">
						<p class="td check-option"><input type="checkbox"  name="id" value="<?php echo $log['id']; ?>" /></span>
						<div class="td">
							<span class="td-con double-row" style="line-height:30px"><?php echo $log['name']; ?><br><?php echo $log['phone']; ?></span>
						</div>
						<div class="td"><?php echo $log['value']; ?></div>
                        <div class="td"><?php echo $log['msg']; ?></div>
                        <div class="td"><?php echo date('Y-m-d H:i:s', $log['dateline']);?></div>
                        <?php if($log['ltype']==2){?>
                         <div class="td">提现</div>
                        <?php }else{
						 ?>
                          <div class="td">////</div>
                         <?php
						   }
						 ?>
                        
                        <div class="td" style="line-height:30px"><?php echo $log['bankCode']; ?> | <?php echo $log['truename']; ?><br>（<?php echo $log['bank']; ?>）</div>
                        <div class="td"><?php echo $log['money_detail']['money']; ?></div>
                        <?php if($log['status']==0){?>
                        <div class="td">待打款</div>
                        <?php
						}else if($log['status']==1){
						?>
                         <div class="td">已打款</div>
                        <?php
						}else {
						?>
                         <div class="td">打款失败</div>
                        <?php 
						 }
						?>
                        <div class="td" style="width:136px">
                        <a href="<?php echo url('delete', array("id" => $log['id'])) ?>" data-confirm="是否确定删除？">删除</a>
						</div>
					</div>
					<?php endforeach ?>
					<div class="paging padding-tb body-bg clearfix">
						<?php echo $pages; ?>
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
		<script>
			$(".table").resizableColumns();
			$(".paging-table").fixedPaging();
		</script>
    </body>
</html>
