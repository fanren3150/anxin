<?php include template('header','admin');?>
<form name="pay_form" method="post">
<div class="form-box border-bottom-none order-eidt-popup clearfix">
	<?php echo form::input('hidden','ids',$ids,'孩子号：','',array('datatype' => '*')); ?>
	<?php echo form::input('hidden','iid',$iid,'id：','',array('datatype' => '*')); ?>
	<?php echo form::input('hidden','school_id',$school_id,'school_id：','',array('datatype' => '*')); ?>    
	<?php  $school_keys = array_keys($clists);$first_key = current($school_keys);?>
	<?php echo form::input('select','class_id',$first_key,'班级：','',array('items' => $clists)); ?>
</div>
<div class="padding text-right ui-dialog-footer">
	<input type="submit" class="button bg-main" id="okbtn" value="确定" name="dosubmit" data-reset="false"/>
	<input type="button" class="button margin-left bg-gray" id="closebtn" value="取消"  data-reset="false"/>
</div>
</form>
<?php include template('footer','admin');?>

<script>
	$(function(){
		try {
			var dialog = top.dialog.get(window);
		} catch (e) {
			return;
		}
		var $val=$("input[type=text]").first().val();
		$("input[type=text]").first().focus().val($val);
		dialog.title('分配班级');
		var obj_validform = $("form[name='pay_form']").Validform({
			ajaxPost:true,
			dragonfly:true,
			callback:function(ret) {
				message(ret.message);
				if(ret.status == 1) {
					setTimeout(function(){
						dialog.close();
						window.top.main_frame.location.reload();
					}, 1000);
				}
				return false;
			}
		});
		
		$('#closebtn').on('click', function () {
			dialog.remove();
			return false;
		});
	})
</script>