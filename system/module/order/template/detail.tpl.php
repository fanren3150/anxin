<?php include template('header','admin');?>

<script type="text/javascript" src="<?php echo __ROOT__ ?>statics/js/admin/order_action.js"></script>
<script type="text/javascript">
	var order = <?php echo json_encode($order); ?>;
	$(document).ready(function(){
		order_action.init();
	});
</script>

<body>
	<div class="fixed-nav layout">
		<ul>
			<li class="first">订单详情</li>
			<li class="spacer-gray"></li>
		</ul>
		<div class="hr-gray"></div>
	</div>
	<div class="content padding-big have-fixed-nav">
		<!--订单概况-->
		<table cellpadding="0" cellspacing="0" class="border bg-white layout margin-top">
			<tbody>
				<tr class="bg-gray-white line-height-40 border-bottom">
					<th class="text-left padding-big-left">
						订单概况
						<div class="order-edit-btn fr">
							<!-- 确认付款(仅在线支付) -->
			                <?php if ($order['pay_type'] == 1): ?>
			                	<button <?php if ($order['_status']['now'] == 'load_pay'): ?> onClick="order_action.pay('<?php echo url("order/admin_order/pay",array("order_sn"=>$order['sn'])); ?>');" class="bg-main"<?php else: ?>class="bg-gray"<?php endif; ?>>确认付款</button>
			                <?php endif; ?>

							<!-- 取消订单 -->
							<button <?php if($order['delivery_status'] == 0 && $order['status'] == 1): ?>class="bg-main" onClick="order_action.order(2,'<?php echo url("order/admin_order/cancel",array("order_sn" => $order['sn'])); ?>');"<?php else: ?>class="bg-gray"<?php endif; ?>>取消订单</button>

							
						</div>
					</th>
				</tr>
				<tr class="border">
					<td class="padding-big-left padding-big-right">
						<table cellpadding="0" cellspacing="0" class="layout">
							<tbody>
								<tr class="line-height-40">
									<th class="text-left">
										订单号：<?php echo $order['sn'];?>&emsp;
										<?php if($order['source']==1) { ?>
											<i class="ico_order_mobile"></i>
										<?php }elseif($order['source']==2) { ?>
											<i class="ico_order_wechat"></i>
										<?php }else { ?>
											<i class="ico_order"></i>
										<?php } ?>
									</th>
									<th class="text-left">支付方式：<?php echo ($order['pay_type']==2) ? '货到付款' : '在线支付'?></th>
									<th class="text-left">订单状态：<?php echo ch_status($order['_status']['now']);?></th>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<!--订单详情-->
		<table cellpadding="0" cellspacing="0" class="border bg-white layout margin-top">
			<tbody>
				<tr class="bg-gray-white line-height-40 border-bottom">
					<th class="text-left padding-big-left">订单详情</th>
				</tr>
				<tr class="border">
					<td class="padding-big-left padding-big-right">
						<table cellpadding="0" cellspacing="0" class="layout">
							<tbody>
								<tr class="line-height-40">
									<td class="text-left">家长：<?php echo $order['_buyer']['username']; ?></td>
									<td class="text-left">孩子姓名：<?php echo ($order['_student']['truename']) ? $order['_student']['truename'] : '--';?></td>
									<td class="text-left">学校名称：<?php echo ($order['_school']['name']) ? $order['_school']['name'] : '--';?></td>
								</tr>
								<tr class="line-height-40">
									<td class="text-left">缴费类型：<?php echo ch_payType($order['payType']);?></td>
									<td class="text-left">支付时间：<?php echo ($order['pay_time']) ? date('Y-m-d H:i:s',$order['pay_time']) : '--' ?></td>
									<td class="text-left">下单时间：<?php echo date('Y-m-d H:i:s',$order['system_time']); ?></td>
								</tr>
								<?php runhook('admin_order_send_time',$order['send_time']);?>
							</tbody>
						</table>
					</td>
				</tr>
				<tr class="border">
					<td class="padding-big-left padding-big-right">
						<table cellpadding="0" cellspacing="0" class="layout">
							<tbody>
								<tr class="line-height-40">
									<th class="text-left" colspan="3">
										应付订单总额：￥<?php echo ($order['real_amount']);?>
										<?php if($order['status'] == 1 && $order['pay_status'] == 0 && $order['pay_type'] != 2): ?>
											<a class="text-main"  onclick="order_action.update_real_price('<?php echo url("order/admin_order/update_real_price",array("sn"=>$order['sn'])); ?>');" href="javascript:;">修改订单应付总额</a>
										<?php endif; ?>
									</th>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		
		<!-- 订单日志 -->
		<table cellpadding="0" cellspacing="0" class="border bg-white layout margin-top">
			<tbody>	
				<tr class="bg-gray-white line-height-40 border-bottom">
					<th class="text-left padding-big-left">订单日志</th>
				</tr>
				<tr class="border">
					<td class="padding-big-left padding-big-right">
						<table cellpadding="0" cellspacing="0" class="layout">
							<tbody>
								<?php foreach ($order_logs as $k => $log) : ?>
									<tr class="line-height-40">
										<td class="text-left">
											<?php if($log['operator_type']==1){echo '系统';} elseif($log['operator_type']==2){echo '买家';} ?>&emsp;
											<?php echo $log['operator_name'] ?>&emsp;于&emsp;
											<?php echo date('Y-m-d H:i:s' ,$log['system_time']); ?>&emsp;
											「<?php echo $log['action']; ?>」&emsp;
											<?php if ($log['msg']) : ?>操作备注：<?php echo $log['msg']; endif;?>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="padding-tb">
			<input class="button margin-left bg-gray border-none" type="button" value="返回" />
		</div>
	</div>
<?php include template('footer','admin');?>
<script>
$('.table').resizableColumns();

$(".look-log").live('click',function(){
	if($(this).hasClass('bg-gray')) return false;
	$(this).removeClass('bg-sub').addClass('bg-gray').html("加载中...");
	var $this = $(this);
	var txt = '';
	$.getJSON('<?php echo url("order/cart/get_delivery_log") ?>', {o_d_id: $(this).attr('data-did')}, function(ret) {
		if (ret.status == 0) {
			alert(ret.message);
			return false;
		}
		if (ret.result.logs.length > 0) {
			$.each(ret.result.logs,function(k, v) {
				txt += '<p>'+ v.add_time +'&nbsp;&nbsp;&nbsp;&nbsp;'+ v.msg +'</p>';
			});
			top.dialog({
				content: '<div class="logistics-info padding-big bg-white text-small"><p class="border-bottom border-dotted padding-small-bottom margin-small-bottom"><span class="margin-big-right">物流公司：'+ret.result.delivery_name+'</span>&nbsp;&nbsp;物流单号：'+ret.result.delivery_sn+'</p>'+ txt +'</div>',
				title: '查看物流信息',
				width: 680,
				okValue: '确定',
				ok: function(){
					$this.removeClass('bg-gray').addClass('bg-sub').html("查看物流");
				},
				onclose: function(){
					$this.removeClass('bg-gray').addClass('bg-sub').html("查看物流");
				}
			})
			.showModal();
		}
	});
})
</script>
