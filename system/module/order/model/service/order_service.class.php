<?php
class order_service extends service {

	protected $where = array();

	public function _initialize() {
		/* 实例化数据层 */
		$this->table      = $this->load->table('order/order');
		$this->table_member = $this->load->table('member/member');
		$this->table_trade = $this->load->table('order/order_trade');
		/* 实例化服务层 */
		$this->service_order_log = $this->load->service('order/order_log');
		$this->service_payment = $this->load->service('pay/payment');
		$this->service_member = $this->load->service('member/member');
        $this->service_order_trade = $this->load->service('order/order_trade');
	}

	/**
	 * 创建订单
	 * @param  integer $buyer_id    会员id
	 * @param  integer $start_time  缴费开始时间
	 * @param  integer $end_time 	缴费结束时间
	 * @param  integer $iid    		发起托班id
	 * @param  array   $payType     缴费类型 0 本月缴费 1 下月缴费 2 下学期缴费
 	 * @param  boolean $submit      是否创建 (为false时 获取订单结算信息，为true时 创建订单)
	 * @return mixed
	 */
	public function create($buyer_id = 0, $start_time, $end_time, $iid = 0, $payType = 1, $student_id=0, $submit = false) {
		/* 定义默认值 */
		$order_sn = $this->_build_order_sn();
		$setting = $this->load->service("admin/setting")->get();
		$numberclass = $setting['classNumber'];		
		$initiate = $this->load->service('school/initiate')->get_initiate_by_id($iid);		
		
		if($student_id == 0) $student_id = $initiate['student_id'];
		
		$deposit = $this->load->service('member/member_deposit')->find(array('iid'=>$iid,'student_id'=>$student_id));
		$schoolData = $this->load->service('school/school')->getSchoolOrClass(array('id'=>$initiate['school_id']),0,$deposit['class_id']);
        if($schoolData['studentN'] < $numberclass){
			$day = 3;
		}else{
			 $day = 7;
		}
		$inTime = getNextMonthDays($schoolData['months_price'],$day);
		if($payType == 0) $price = $inTime[2][3];
		if($payType == 1) $price = $inTime[1][3];
		if($payType == 2) $price = $inTime[0][3];
		if($payType == 3) $price = $inTime[3][3];
		$school_id = $initiate['school_id'];
		$order_data = array(
			'iid'             => $iid,
			'sn'              => $order_sn,
			'buyer_id'        => $buyer_id,
			'source'          => 2,
			'pay_type'        => 1, 	 // 支付类型(1:在线支付 , 2:货到付款)
			'amount'      	  => $price, // 商品总额
			'real_amount'     => $price, // 应付总额
			'payType'         => $payType,
			'start_time'  	  => $start_time,
			'end_time'        => $end_time,
			'school_id'       => $school_id,
			'student_id'      => $student_id,
			'system_time'     => time(),
		);
		$this->table->where(array('iid' => $iid,'buyer_id'=>$buyer_id,'pay_status'=>0,'status'=>1))->delete();
		$oid = $this->table->update($order_data);
		if (!$oid) {
			$this->error = $this->table->getError();
			return FALSE;
		}
		
		// 订单日志
		$data = array();
		$data['order_sn']      = $order_sn;
		$data['sub_sn']        = $order_sn;
		$data['action']        = '创建订单';
		$data['operator_id']   = $operator['id'];
		$data['operator_name'] = $operator['username'];
		$data['operator_type'] = $operator['operator_type'];
		$data['msg']           = '发起托班并生成订单';
		$this->service_order_log->add($data);
		
		// 钩子：下单成功
		$member = array();
		$member['member'] = $this->load->table('member/member')->where(array('id' => $buyer_id))->find();
		$member['order_sn'] = $order_sn;
		runhook('create_order',$member);
		return $order_sn;		
	}

	/**
	 * 根据日期生成唯一订单号
	 * @param boolean $refresh 	是否刷新再生成
	 * @return string
	 */
	private function _build_order_sn($refresh = FALSE) {
		if ($refresh == TRUE) {
			return date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 12);
		}
		return date('YmdHis').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 6);
	}

	/**
	 * 获取订单列表
	 * @param  array  $sqlmap 查询条件，具体参数见 build_sqlmap()方法
	 * @return [result]
	 */
	public function get_lists($sqlmap = array()) {
		$sqlmap = $this->build_sqlmap($sqlmap);
		$result = $this->table->where($sqlmap)->order('id DESC')->select();
		return $result;
	}

	/**
	 * 生成查询条件
	 * @param  $options['type'] (1:待付款|2:待确认|3:待发货|4:待收货|5:已完成|6:已取消|7:已回收|8:已删除)
	 * @param  $options['keyword'] 	关键词(订单号|收货人姓名|收货人手机)
	 * @return [$sqlmap]
	 */
	public function build_sqlmap($options) {
		if(empty($options['type'])){
       		$options['type'] = $options['map']['type'];
     	}
		extract($options);
		$sqlmap = array();
		if (isset($type) && $type > 0) {
			switch ($type) {
				// 待付款
				case 1:
					$sqlmap['pay_type']   = 1;
					$sqlmap['status']     = 1;
					$sqlmap['pay_status'] = 0;
					break;
				// 已付款
				case 2:
					$sqlmap['pay_type']   = 1;
					$sqlmap['status']     = 1;
					$sqlmap['pay_status'] = 1;
					$sqlmap['finish_status'] = 0;
					break;
				// 已完成
				case 5:
					$sqlmap['status'] = 1;
					$sqlmap['finish_status'] = 1;
					break;
				// 已取消
				case 6:
					$sqlmap['status'] = 2;
					break;
				// 已作废
				case 7:
					$sqlmap['status'] = (defined('IN_ADMIN')) ? array('GT', 2) : 3;
					break;
				// 前台已删除
				case 8:
					$sqlmap['status'] = 4;
			}
		}
		
		if (isset($iid) && !empty($iid)) {
			$sqlmap['iid'] = $iid;
		}
		if (isset($keyword) && !empty($keyword)) {
			$buyer_ids = $this->load->table('member/member')->where(array('username' => array('LIKE','%'.$keyword.'%')))->getField('id',TRUE);
			$sn = $this->load->table('order/order_trade')->where(array('trade_no' => array('LIKE','%'.$keyword.'%')))->getField('order_sn',TRUE);
			$student_ids = $this->load->table('member/member_student')->where(array('truename' => array('LIKE','%'.$keyword.'%')))->getField('id',TRUE);
			$school_ids = $this->load->table('school/school')->where(array('name' => array('LIKE','%'.$keyword.'%')))->getField('id',TRUE);
			if($buyer_ids){
				$sqlmap['buyer_id'] = array('IN',$buyer_ids);
			}
			if($sn){
				$sqlmap['sn'] = array('IN',$sn);
			}
			
			if($student_ids){
				$sqlmap['student_id'] = array('IN',$student_ids);
			}
			
			if($school_ids){
				$sqlmap['school_id'] = array('IN',$school_ids);
			}
			
			if (!$buyer_ids && !$sn && !$school_ids && !$student_ids) {
				$sqlmap['sn|pay_sn'] = array('LIKE','%'.$keyword.'%');
			}
		}
		return $sqlmap;
	}

	/**
	 * 提交订单支付
	 * @param  string  $sn        主订单号
	 * @param  integer $isbalance 是否余额支付
	 * @param  string  $pay_code  支付方式code
	 * @param  string  $pay_bank  网银支付bank($pay_code = 'bank'时必填)
	 * @param  string  $mid  用户id
	 * @return [result]
	 */
	public function detail_payment($sn = '' ,$isbalance = 0,$pay_code = '' ,$pay_bank = '' ,$mid = 0) {
		$sn = (string) $sn;
		$isbalance = (int) $isbalance;
		/* 读取后台配置 (是否减库存) */
		$stock_change = $this->load->service('admin/setting')->get('stock_change');
		$order = $this->table->detail($sn)->output();
		if ($order['buyer_id'] != $mid) {
			$this->error = lang('_valid_access_');
			return FALSE;
		}
		if ($order['pay_status'] == 1) {
			$this->error = lang('order_paid','pay/language');
			return FALSE;
		}
		if ($pay_code == 'bank' && empty($pay_bank)) {
			$this->error = lang('pay_ebanks_error','order/language');
			return FALSE;
		}
		$money = $this->load->table('member/member')->where(array('id' => $mid))->getfield('money');
		// 后台余额支付开关
		$balance_pay = $this->load->service('admin/setting')->get('balance_pay');
		// 还需支付总额
		$total_fee = round($order['real_amount'] - $order['balance_amount'], 2);
		/* 含有余额支付的 */
		if ($balance_pay == 1 && $isbalance == 1 && $money > 0) {
			$balance_amount = $total_fee;	// 本次余额支付的金额
			if ($money < $total_fee) {
				$balance_amount = $money;
				$total_fee = abs(round($total_fee - $money,2));
			} else {
				$total_fee = 0;
			}
			// 扣除会员余额($balance_amount),并写入 冻结资金
			$result = $this->service_member->action_frozen($mid ,$balance_amount , true ,'余额支付订单,主订单号:'.$sn);
			if (!$result) {
				$this->error = $this->service_member->error;
				return FALSE;
			}
			// 把余额支付金额写入订单余额支付总额里
			$_balance_amount = $order['balance_amount'] + $balance_amount;
			$result = $this->table->where(array('sn' => $sn))->setField('balance_amount' ,$_balance_amount);
			if (!$result) {
				$this->error = $this->table->getError();
				return FALSE;
			}
		}
		/* 需要再用网银支付的 */
		$result = array();
		if ($total_fee > 0) {
            $trade_no = $this->service_order_trade->get_trade_no($sn,$total_fee,$pay_code);

            $pay_info = array();
            $pay_info['trade_sn']  = $trade_no;
            $pay_info['total_fee'] = $total_fee;
            $pay_info['subject']   = '订单支付号：'.$trade_no;
            $pay_info['pay_bank']  = $pay_bank;
			/* 请求支付 */
			$gateway = $this->service_payment->gateway($pay_code,$pay_info);
			if($gateway === false) showmessage(lang('pay_set_error','pay/language'));
			$gateway['order_sn'] = $sn;
            $gateway['trade_no'] = $trade_no;
			$gateway['total_fee'] = $total_fee;
		} else {
			// 设置订单为支付状态
			$data = array();
			$order = $this->table->detail($sn)->output();
			if($order['balance_amount'] == $order['real_amount']  && $balance_pay==1){
				$data['pay_method'] = '余额支付';
			}elseif($order['balance_amount'] == 0){
				$data['pay_method'] = $pay_code;
			}else{
				$data['pay_method'] ='余额支付'. '+' . $pay_code;
			}
			$data['paid_amount'] = $order['real_amount'];
			$data['msg'] = '您的订单已支付成功';
			$this->load->service('order/order_sub')->set_order($sn ,'pay','',$data);
			$result['pay_success'] = 1;
		}
		
		/* 支付后的跳转地址 */
		$gateway['url_forward'] = url('order/order/pay_success',array('order_sn' => $sn));
		$result['gateway'] = $gateway;
		return $result;
	}
	
	public function get_order_lists($sqlmap,$page,$limit){
		$orders = $this->table->page($page)->where($sqlmap)->limit($limit)->order('id DESC')->select();
		
		$lists = array();
		foreach($orders as $k => $order){
			$lists[] = array(
				'id'=>$order['id'],
				'sn'=>$order['sn'],
				'username'=>$order['_buyer']['username'],
				'avatar'=>$order['_buyer']['avatar'],
				'student_id'=>$order['_student']['id'],
				'student_name'=>$order['_student']['truename'],
				'school_id'=>$order['_school']['id'],
				'school_name'=>$order['_school']['name'],
				'system_time'=>$order['system_time'],
				'real_amount'=>$order['real_amount'],
				'_pay_type'=>$order['_pay_type'],
				'payType'=>$order['payType'],
				'_status'=>$order['_status']['now'],
			);
		}

		return $lists;
	}

	/**
     * 条数
     * @param  [arra]   sql条件
     * @return [type]
     */
    public function count($sqlmap = array()){
        $result = $this->table->where($sqlmap)->count();
        if($result === false){
            $this->error = $this->table->getError();
            return false;
        }
        return $result;
    }
    public function member_table_detail($order_sn){
		return $this->table->detail($order_sn)->subs(FALSE ,FALSE, FALSE)->output();
	}
	public function order_table_detail($order_sn){
		return $this->table->detail($order_sn)->output();
	}
	/**
	 * @param  array 	sql条件
	 * @param  integer 	条数
	 * @param  integer 	页数
	 * @param  string 	排序
	 * @return [type]
	 */
	public function fetch($sqlmap = array(), $limit = 20, $page = 1, $order = "", $field = "") {
		$result = $this->table->where($sqlmap)->limit($limit)->page($page)->order($order)->field($field)->select();
		if($result===false){
			$this->error = $this->table->getError();
			return false;
		}
		return $result;
	}
	/**
	 * @param  array 	sql条件
	 * @param  integer 	读取的字段
	 * @return [type]
	 */
	public function find($sqlmap = array(), $field = "") {
		$result = $this->table->where($sqlmap)->field($field)->find();
		if($result===false){
			$this->error = $this->table->getError();
			return false;
		}
		return $result;
	}
	
	//获取用户资料
	public function member_data($sqlmap){
		$result = $this->load->table('member/member')->find($sqlmap);
		return $result;
	}
	
	/**
	 * 修改订单应付总额
	 * @param  string  $sn 		订单号
	 * @param  float   $real_price  修改后的价格
	 * @return [boolean]
	 */
	public function update_real_price($sn = '',$real_amount = 0) {
		$real_price = sprintf('%.2f' , max(0,(float) $real_amount));
		$order = $this->table->where(array('sn' => $sn))->find();
		if ($order['pay_status'] ==1) {
			$this->error = lang('dont_edit_order_amount','order/language');
			return FALSE;
		}
		$result = $this->table->where(array('sn' => $sn))->setField('real_amount' ,$real_amount);
		if (!$result) {
			$this->error = $this->table->getError();
			return FALSE;
		}
		
		// 订单操作日志
		$operator = get_operator();	// 获取操作者信息
		$data = array();
		$data['order_sn']      = $order['sn'];
		$data['sub_sn']        = $order['sn'];
		$data['action']        = '修改订单应付总额';
		$data['operator_id']   = $operator['id'];
		$data['operator_name'] = $operator['username'];
		$data['operator_type'] = $operator['operator_type'];
		$data['msg']           = '原应付总额「'.$order['real_amount'].'」修改为「'.$real_amount.'」';
		$this->service_order_log->add($data);
		return $result;
	}
	
	/**
	 * 设置订单
	 * @param  string 	$sn  		订单号(确认支付时传主订单号，其它传子订单号)
	 * @param  string 	$action 	操作类型
	 *         (order:订单 || pay:支付 || confirm:确认 || delivery:发货 || finish:完成)
	 * @param  int 		$status 	状态(只有$action = 'order'时必填)
	 * @param  array 	$options 	附加参数
	 * @return [boolean]
	 */
	public function set_order($sn = '',$action = '',$status = 0 ,$options = array()) {
		$sn     = (string) trim($sn);
		$action = (string) trim($action);
		$status = (int) $status;
		$msg    = (string) trim($options['msg']);
		unset($options['msg']);
		if (empty($sn)) {
			$this->error = lang('order_sn_not_null','order/language');
			return FALSE;
		}
		if (empty($action)) {
			$this->error = lang('operate_type_empty','order/language');
			return FALSE;
		}
		if (!in_array($action, array('order','pay','confirm','delivery','finish'))) {
			$this->error = lang('operate_type_error','order/language');
			return FALSE;
		}
		// 检测订单是否存在
		$this->order = $this->table->where(array('sn' => $sn))->find();
		if (!$this->order) {
			$this->error = lang('order_not_exist','order/language');
			return FALSE;
		}
		// 获取订单状态
		$this->order['_status'] = $this->table->get_status($this->order);
		switch ($action) {
			case 'order':	// (2：已取消，3：已回收，4：已删除)
					$result = $this->_order($status ,$options);
					// 后台删除订单直接返回
					if (IN_ADMIN && $status == 4 && $result !== FALSE) {
						return TRUE;
					}
				break;
			case 'pay':	// 针对所有子订单操作
				$result = $this->_pay($options);
				break;
			case 'finish':
				$result = $this->_finish($options);
				break;
		}
		if ($result === FALSE) return FALSE;
		// 订单日志
		$operator = get_operator();	// 获取操作者信息
		$data = array();
		if ($action == 'pay') {
			$data['order_sn'] = $sn;
			$data['sub_sn']   = $sn;
			$data['action']        = $result['action'];
			$data['operator_id']   = $operator['id'];
			$data['operator_name'] = $operator['username'];
			$data['operator_type'] = $operator['operator_type'];
			$data['msg']           = $msg;
			$this->service_order_log->add($data);
 		} else {
 			$data['order_sn'] = $this->order['sn'];
			$data['sub_sn']   = $this->order['sn'];
			$data['action']        = $result;
			$data['operator_id']   = $operator['id'];
			$data['operator_name'] = $operator['username'];
			$data['operator_type'] = $operator['operator_type'];
			$data['msg']           = $msg;
			$this->service_order_log->add($data);
 		}
		return TRUE;
	}
	
	/* 订单操作 */
	private function _order($status ,$options) {
		$order = $this->order;
		$data = $sqlmap = array();
		switch ($status) {
			case 2:	// 取消订单
				$string = '您的订单已取消';
				if ($order['status'] != 1) {
					$this->error = lang('order_dont_operate','order/language');
					return FALSE;
				}
				/* 在线支付：取消整个订单，货到付款：取消当前子订单 */
				$data['status'] = 2;
				$data['system_time'] = time();
				if ($order['pay_type'] == 1) {
					
					// 主订单信息
					$order_main = $this->table->where(array('sn' =>$order['sn']))->find();
					/* 已付款的&是否退款到账户余额 ==> 退款到账户余额 */
					if ($order['pay_status'] == 1 && $options['isrefund'] == 1) {
						$this->load->service('member/member')->change_account($order_main['buyer_id'],'money',$order_main['paid_amount'],'取消订单退款,订单号:'.$order['order_sn']);
						// 解冻余额支付的金额
						if ($order_main['balance_amount'] > 0) {
							$this->load->service('member/member')->action_frozen($order_main['buyer_id'],$order_main['balance_amount'],false);
						}
						$string = '您的订单已取消，已退款到您的账户余额，请查收';
					}
					/* 未付款的&是否退款到账户余额 ==> 退款到账户余额 */
					if ($order['pay_status'] == 0 && $options['isrefund'] == 1) {

						$this->load->service('member/member')->change_account($order_main['buyer_id'],'money',$order_main['balance_amount'],'取消订单退款,订单号:'.$order['sn']);
						// 解冻余额支付的金额
						if ($order_main['balance_amount'] > 0) {
							$this->load->service('member/member')->action_frozen($order_main['buyer_id'],$order_main['balance_amount'],false);
						}
						$string = '您的订单已取消，已退款到您的账户余额，请查收';
					}
				} 
				
				$result = $this->table->where(array('sn' =>$order['sn']))->save($data);
				if (!$result) {
					$this->error = $this->table->getError();
					return FALSE;
				}
				
				return $string;
				break;

			case 3:	// 订单回收站
				if ($order['status'] != 2) {
					$this->error = lang('_valid_access_');
					return FALSE;
				}
				// 标记当前子订单为已回收
				$data['status'] = 3;
				$data['system_time'] = time();
				
				// 统计子订单已取消的总数
				$sqlmap['status'] = 3;
				$this->table->where(array('sn' => $order['sn']))->save($data);
				return '您的订单已放入回收站';
				break;

			/* 订单删除 */
			case 4:
				if ($order['status'] != 3) {
					$this->error = lang('_valid_access_');
					return FALSE;
				}
				// 前台用户删除的只更改状态，管理员删除的需删除所有订单相关的信息
				if (defined('IN_ADMIN')) {
					// 删除子订单
					$sqlmap['sn'] = $order['sn'];
					$sqlmap['status'] = 3;
					$result = $this->table->where($sqlmap)->delete();
					if (!$result) {
						$this->error = lang('delete_order_error','order/language');
						return FALSE;
					}
					
					// 删除订单日志
					$this->load->table('order/order_log')->where(array('sn' => $order['sn']))->delete();
					return '订单删除成功';
				} else {
					// 标记当前子订单为删除
					$data['status'] = 4;
					$data['system_time'] = time();
					$result = $this->table->where(array('sn' => $order['sn']))->save($data);
					if (!$result) {
						$this->error = $this->table->getError();
						return FALSE;
					}
					return '您的订单已从回收站删除';
				}
				break;
		}
	}
	
	/**
	 * 支付操作 (针对所有子订单操作)
	 * @param  array $options
	 *         			paid_amount ：实付金额
	 *         			pay_method  ：支付方式
	 *         			pay_sn 		：支付流水号
	 * @return [string]
	 */
	private function _pay($options) {
		$order = $this->order;
		if ($order['pay_type'] != 1 || $order['pay_status'] != 0) {
			$this->error = lang('_valid_access_');
			return FALSE;
		}
		$data = array();
		$data['pay_status'] = 1;
		$data['pay_time']   = time();
		
		// 设置主订单表信息
		if (isset($options['paid_amount'])) {	// 实付金额
			$data['paid_amount'] = sprintf("%.2f", (float) $options['paid_amount']);
		} else {
			$data['paid_amount'] = $order['real_amount'];
		}
		if (isset($options['pay_method'])) {	// 支付方式
			$data['pay_method']  = (string) $options['pay_method'];
		}
		if (isset($options['pay_sn'])) {		// 支付流水号
			$data['pay_sn'] = (string) $options['pay_sn'];
		}
		$result = $this->table->where(array('sn' => $order['sn']))->save($data);
		if (!$result) return FALSE;
		
		if (isset($options['paid_amount']) && isset($options['pay_method']) && isset($options['pay_sn'])) {
			$_map = array();
			$_map['order_sn'] = $order['sn'];
			$_map['trade_no'] = date('Ymd') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 12);
			$_map['total_fee'] = $options['paid_amount'];
			$_map['status'] = 1;
			$_map['time'] = time();
			$_map['method'] = $options['pay_method'];
			$_map['pay_sn'] = $options['pay_sn'];
			$set_pay_sn = $this->table_trade->add($_map);			
		}else{
			$set_pay_sn = $this->table_trade->where(array('order_sn' => $order['sn']))->setField('pay_sn',$data['pay_sn']);
		}
		if ($set_pay_sn === FALSE){
			$this->error = $this->table_trade->getError();
			return FALSE;
		}
		
		$sinfo = $this->load->table('member/member_student')->where(array('id'=>$order['student_id']))->find();
		if($sinfo['start_time'] == 0){
			$this->load->table('member/member_student')->where(array('id'=>$order['student_id']))->save(array('start_time'=>$order['start_time']));
		}
		
		if($sinfo['end_time'] < $order['end_time']){
			$this->load->table('member/member_student')->where(array('id'=>$order['student_id']))->save(array('end_time'=>$order['end_time']));
		}
		
		$field = "money";
		$msg = "订单号:".$order['sn'];
		$_member = $this->load->table('member')->setid($order['buyer_id'])->output();
		$log_info = array(
			'mid'      => $order['buyer_id'],
			'value'    => '-'.$data['paid_amount'],
			'ltype'     => 0,
			'type'     => $field,
			'msg'      => $msg,
			'dateline' => TIMESTAMP,
			'admin_id' => (defined('IN_ADMIN')) ? ADMIN_ID : 0,
			'money_detail' => json_encode(array($field => sprintf('%.2f' ,$_member[$field])))
		);
		$this->load->table('member_log')->update($log_info);
							
		// 钩子：支付成功
		runhook('order_pay_success');
		return array('action' => '支付订单','order_sn' => $order['sn']);
	}
	
	private function _finish($options = array()) {
		$site_name = $this->load->service('admin/setting')->get('site_name');
		$order = $this->order;
		if ($order['finish_status'] == 1) {
			$this->error = lang('_valid_access_');
			return FALSE;
		}		
		
		$data['finish_status'] = 1;
		$result = $this->table->where(array('sn' => $order['sn']))->save($data);
		if (!$result) {
			$this->error = $this->table->getError();
			return FALSE;
		}
		/*$sinfo = $this->load->table('member/member_student')->where(array('id'=>$order['student_id']))->find();
		if($sinfo['start_time'] == 0){
			$this->load->table('member/member_student')->where(array('id'=>$order['student_id']))->save(array('start_time'=>$order['start_time']));
		}
		
		if($sinfo['end_time'] < $order['end_time']){
			$this->load->table('member/member_student')->where(array('id'=>$order['student_id']))->save(array('end_time'=>$order['end_time']));
		}*/
		// 钩子：(完成)
		//runhook('delivery_finish');
		$string = '感谢您来到'.$site_name;
		return $string;
	}
}