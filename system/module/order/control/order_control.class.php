<?php
hd_core::load_class('init','school');
class order_control extends init_control {
	public function _initialize() {
		parent::_initialize();
		runhook('no_login');
		if ($this->member['id'] == 0 && !defined('NO_LOGIN')) {
			$url_forward = $_GET['url_forward'] ? $_GET['url_forward'] : urlencode($_SERVER['REQUEST_URI']);
			showmessage(lang('_not_login_'), url('member/public/login',array('url_forward'=>$url_forward)),0);
		}
		$this->service = $this->load->service('order/order');
	}
	
	public function index(){		
		$SEO = seo('缴费');
		$id = intval($_GET['id']);
		$sid = intval($_GET['sid']);
		$initiate = $this->load->service('school/initiate')->get_initiate_by_id($id);
		if($initiate['isopen'] == 0){
			redirect(url('member/index/initiate',array('sid'=>$sid)));
		}
		$deposit = $this->load->service('member/member_deposit')->find(array('iid'=>$id,'student_id'=>$sid));
		$schoolData = $this->load->service('school/school')->getSchoolOrClass(array('id'=>$initiate['school_id']),0,$deposit['class_id']);		
		$setting = $this->load->service("admin/setting")->get();
		$classNumber = $setting['classNumber'];
		if($schoolData['studentN'] < $classNumber){
			$day = 3;
		}else{
			 $day = 7;
		}
		$inTime = getNextMonthDays($schoolData['months_price'],$day);
		$this->load->librarys('View')->assign('initiate',$initiate)->assign('inTime',$inTime)->assign('sid',$sid)->assign('day',$day)->assign('schoolData',$schoolData)->assign('SEO',$SEO)->display('index');
	}
    
	public function jf(){		
		$SEO = seo('缴费');
		$sid = intval($_GET['sid']);		
		$student = $this->load->service('member/member_student')->fetch_by_id($sid);
		$schoolData = $this->load->service('school/school')->getSchoolOrClass(array('id'=>$student['school_id']),0,$student['class_id']);
		
		$setting = $this->load->service("admin/setting")->get();
		$classNumber = $setting['classNumber'];
		if($schoolData['studentN'] < $classNumber){
			$day = 3;
		}else{
			 $day = 7;
		}
		$inTime = getNextMonthDays($schoolData['months_price'],$day,intval($student['end_time']));	
		$this->load->librarys('View')->assign('initiate',$initiate)->assign('inTime',$inTime)->assign('sid',$sid)->assign('day',$day)->assign('schoolData',$schoolData)->assign('SEO',$SEO)->display('index');
	}  
     /**
	 * 创建订单
	 * @param 	array
	 * @return  [boolean]
	 */
	public function create() {		
		$start_time = strtotime($_POST['start_time']." 00:00:00");
		$end_time = strtotime($_POST['end_time']." 23:59:59");
		$iid = (int) $_GET['iid'];
		$sid = (int) $_GET['sid'];
		$payType = (int) $_GET['payType'];		
		$result =  $this->service->create($this->member['id'], $start_time , $end_time, $iid, $payType, $sid, true);
		
		if (!$result) {
			showmessage($this->service->error);
		}
		runhook('after_create_order',$result);
		showmessage(lang('order_create_success','order/language'),url('order/order/detail',array('order_sn'=>$result)),1,'json');
	}
	
	public function show() {
		$order_sn = trim($_GET['order_sn']);
		if (empty($order_sn)) showmessage(lang('_error_action_'));
		$order = $this->service->order_table_detail($order_sn);
		if ($order['pay_type'] == 1 && $order['pay_status'] != 1) {
			showmessage(lang('order_not_pay_status','order/language'));
		}
		
		$initiate = $this->load->service('school/initiate')->get_initiate_by_id($order['iid']);
		$count = $this->service->count(array('iid'=>$order['iid'],'pay_status'=>1));
		$orders = $this->service->get_order_lists(array('iid'=>$order['iid'],'pay_status'=>1,'status'=>1));
		$SEO = seo('托班进度');
		
		require_once APP_PATH."library/sdk/jssdk.php";
		$jssdk = new JSSDK(AppId, AppSecret);
		$signPackage = $jssdk->GetSignPackage();
		
		$this->load->librarys('View')->assign('order',$order)->assign('mid',$this->member['id'])->assign('signPackage',$signPackage)->assign('orders',$orders)->assign('initiate',$initiate)->assign('count',$count)->assign('SEO',$SEO)->display('detail_show');
	}
	
	public function detail() {
		$order_sn = trim($_GET['order_sn']);
		if (empty($order_sn)) showmessage(lang('_error_action_'));
		$order = $this->service->order_table_detail($order_sn);
		if ($this->member['id'] != $order['buyer_id']) {
			showmessage(lang('no_promission_view','pay/language'));
		}
		if ($order['pay_type'] == 1 && $order['pay_status'] != 0) {
			showmessage(lang('order_not_pay_status','order/language'),url("member/index/index"));
		}
		if($order['real_amount'] == 0) {
			redirect(url('order/order/pay_success',array('sn'=>$order_sn)));
		}
		if (checksubmit('submit')) {
			$result = $this->service->detail_payment($_GET['order_sn'],$_GET['balance_checked'],$_GET['pay_code'],$_GET['pay_bank'],$this->member['id']);
			if ($result == FALSE) showmessage($this->service->error);
			$gateway = $result['gateway'];
			// 已支付成功的订单跳转到成功页面
			if ($result['pay_success'] == 1) {
				redirect($gateway['url_forward']);
			}
			$SEO = seo('收银台 - 会员中心');
			if (defined('MOBILE') && $gateway['gateway_type'] == 'redirect') {
				redirect($gateway['gateway_url']);
			}
			include template('cashier', 'pay');
		} else {
			if ($order['pay_type'] == 2) {	// 货到付款
				include template('order_success');
				return FALSE;
			}
			// 后台设置-余额支付 1:开启，0：关闭
			$setting = $this->load->service('admin/setting')->get();
			$balance_pay = $setting['balance_pay'];
			$member_info = $this->member;
			$pays = $setting['pays'];
			$payments = $this->load->service('pay/payment')->getpayments('wap', $pays);
			$SEO = seo('订单支付');
			$this->load->librarys('View')->assign('order',$order)->assign('order_sn',$order_sn)->assign('setting',$setting)->assign('balance_pay',$balance_pay)->assign('member_info',$member_info)->assign('pays',$pays)->assign('payments',$payments)->assign('SEO',$SEO)->display('detail_payment');
		}
	}

	/* 获取支付状态 */
	public function get_pay_status() {
		$order_sn = $_GET['order_sn'];
		$order = $this->service->order_table_detail($order_sn);
		if (!$order || $order['buyer_id'] != $this->member['id']) {
			showmessage(lang('no_promission_view','pay/language'));
		}
		if ($order['_status']['now'] == 'pay') {
			showmessage(lang('order_paid','pay/language'),url('order/order/pay_success',array('order_sn'=>$order_sn)),1,'json');
		} else {
			showmessage(lang('order_no_pay','order/language'));
		}
	}

	/* 支付成功 */
	public function pay_success() {
		$order_sn = $_GET['order_sn'] ? $_GET['order_sn'] : $_GET['sn'];
		$order = $this->service->order_table_detail($order_sn);
		if (!$order) showmessage(lang('order_not_exist','order/language'));
		if ($order['buyer_id'] != $this->member['id']) showmessage(lang('no_promission_view','order/language'));
		$SEO = seo('支付成功');
		runhook('after_pay_success',$order_sn);
		$this->load->librarys('View')->assign('order',$order)->assign('order_sn',$order_sn)->assign('SEO',$SEO)->display('order_success');
	}
	
	public function protocol(){
		$SEO = seo('安昕午托须知');
		$school = model('school')->where(array('id'=>$_GET['school_id']))->find();
		$bl = sprintf('%.2f',($school['price']/$school['months_price'])*100);
		$this->load->librarys('View')->assign('bl',$bl)->assign('SEO',$SEO)->display('protocol');
	}
}