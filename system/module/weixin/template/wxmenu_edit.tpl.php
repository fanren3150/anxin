<?php include $this->admin_tpl('header','admin');?>
	<body>
		<div class="fixed-nav layout">
			<ul>
				<li class="first">添加菜单</li>
				<li class="spacer-gray"></li>
			</ul>
			<div class="hr-gray"></div>
		</div>
		<form action="" method="POST">
		<div class="content padding-big have-fixed-nav">
			<div class="hidden">
				<input type="hidden" name="parent_id" value="<?php echo isset($info['pid']) ? $info['pid'] : $_GET['id']?>">
			</div>
			<div class="form-box clearfix">
				<?php echo form::input('text', 'name', $info['name'] ? $info['name'] : '', '菜单名称：', '请填写文字菜单的名称'); ?>
			</div>
			<div class="form-box clearfix">
				<?php echo form::input('radio', 'type', isset($info['type']) ? $info['type']: 0, '菜单动作类型：', '', array('items' => array('0'=> '关键字回复菜单','1'=>'url链接菜单'), 'colspan' => 3,)); ?>
			</div>
			<div class="form-box clearfix">
				<?php echo form::input('text', 'keyword', $info['keyword'] ? $info['keyword'] :'' , '关键词：', '请输入关键词，如果是关键字回复菜单这项必填'); ?>
			</div>
			<div class="form-box clearfix">
				<?php echo form::input('text', 'likes', $info['likes'] ? $info['likes'] :'http://' , '菜单链接：', '用字母和数字组成，如果是url链接菜单这项必填'); ?>
			</div>
            
			<div class="form-box clearfix">
				<?php echo form::input('radio', 'status', isset($info['status']) ? $info['status']: 1, '状态：', '', array('items' => array('0'=> '关闭','1'=>'开启'), 'colspan' => 2,)); ?>
			</div>
			<div class="form-box clearfix">
				<?php echo form::input('text', 'sort', $info['sort'] ? $info['sort'] :100 , '排序：', '请填写自然数，文字分类列表将会根据排序进行由小到大排列显示'); ?>
			</div>
			<div class="padding">
				<input type="submit" class="button bg-main" value="保存" name="dosubmit" />
				<input type="button" class="button margin-left bg-gray" value="返回" />
			</div>
		</div>
		</form>
		<script>
			$("input[name=parent]").attr("readonly","readonly");
			$("input[name=parent]").live('click',function(){
				var data = $(this).attr('data-ids');
				top.dialog({
					url: '<?php echo url('weixin/admin/menu_choose',array('type'=>'category'))?>',
					title: '加载中...',
					width: 930,
					data:data,
					onclose: function () {
						if(this.returnValue){
							$("input[name=parent]").attr('data-ids',this.returnValue.split("category_ids=")[1].split(',').reverse());
							$("#choosecat").val(html_encode(this.returnValue,0));
							$("input[name=parent_id]").val(html_encode(this.returnValue,1));
						}
					}
				})
				.showModal();
			})
			//选择分类操作
			function html_encode(str,i){ 
			    str = str.split("category_ids=")[i];
				if(i == 1){
					var id = "<?php echo $_GET['id']?>";
					var arr_str = str.split(",");
					if(arr_str[arr_str.length-1] == id){
						return;
					}
					return arr_str[arr_str.length-1];
				}
				str = str.replace(/&gt;/g, ">"); 
				str = str.replace(/&lt;/g, "<");   
				str = str.replace(/&gt;/g, ">");   
				str = str.replace(/&nbsp;/g, " ");   
				str = str.replace(/&#39;/g, "\'");   
				str = str.replace(/&quot;/g, "\""); 
				return str;
			}
		</script>
	</body>
</html>
