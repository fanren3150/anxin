<?php include $this->admin_tpl('header','admin');?>
<body>
	<div class="fixed-nav layout">
		<ul>
			<li class="first">关注自动回复</li>
			<li class="spacer-gray"></li>
		</ul>
		<div class="hr-gray"></div>
	</div>
	<form action="" method="POST">
	<div class="content padding-big have-fixed-nav">
		<div class="hidden">
			<input type="hidden" name="id" value="<?php echo isset($info['id']) ? $info['id'] : $_GET['id']?>">
		</div>
		<div class="form-box clearfix">
			<?php echo form::input('textarea', 'content', $info['content'] ? $info['content'] : '', '菜单名称：', '请填写文字菜单的名称'); ?>
		</div>
		<div class="padding">
			<input type="submit" class="button bg-main" value="保存" name="dosubmit" />
			<input type="button" class="button margin-left bg-gray" value="返回" />
		</div>
	</div>
	</form>
</body>
</html>
