<?php include $this->admin_tpl('header','admin');?>
	<body>
		<div class="fixed-nav layout">
			<ul>
				<li class="first">消息发送</li>
				<li class="spacer-gray"></li>
			</ul>
			<div class="hr-gray"></div>
		</div>
		<form action="" method="POST" name="form-validate">
		<div class="content padding-big have-fixed-nav">
			<div class="form-box clearfix">
				<?php echo form::input('radio', 'type', isset($info['type']) ? $info['type']: 2, '发送范围：', '', array('items' => array('2'=>'小B端','1'=>'普通用户'), 'colspan' => 4,)); ?>
			</div>
			<div class="form-box clearfix">
				<?php echo form::input('textarea', 'content', $info['content'] ? $info['content'] :'' , '发送内容：', '请输发送内容', array('datatype' => '*','nullmsg' => '请输发送内容',)); ?>
			</div>
			<div class="padding">
				<input type="submit" class="button bg-main" value="发送" name="dosubmit" />
				<input type="button" class="button margin-left bg-gray" value="返回" />
			</div>
		</div>
		</form>
		<script>
			$(document).ready(function(){
				var promotion_order_add = $("form[name=form-validate]").Validform({
					ajaxPost:false
				})
			});
			$("input[name=parent]").attr("readonly","readonly");
			$("input[name=types]").live('click',function(){
				var data = $(this).attr('data-ids');
				top.dialog({
					url: '<?php echo url('weixin/admin/menu_choose',array('type'=>'category'))?>',
					title: '加载中...',
					width: 930,
					data:data,
					onclose: function () {
						if(this.returnValue){
							$("input[name=parent]").attr('data-ids',this.returnValue.split("category_ids=")[1].split(',').reverse());
							$("#choosecat").val(html_encode(this.returnValue,0));
							$("input[name=parent_id]").val(html_encode(this.returnValue,1));
						}
					}
				})
				.showModal();
			})
			//选择分类操作
			function html_encode(str,i){ 
			    str = str.split("category_ids=")[i];
				if(i == 1){
					var id = "<?php echo $_GET['id']?>";
					var arr_str = str.split(",");
					if(arr_str[arr_str.length-1] == id){
						return;
					}
					return arr_str[arr_str.length-1];
				}
				str = str.replace(/&gt;/g, ">"); 
				str = str.replace(/&lt;/g, "<");   
				str = str.replace(/&gt;/g, ">");   
				str = str.replace(/&nbsp;/g, " ");   
				str = str.replace(/&#39;/g, "\'");   
				str = str.replace(/&quot;/g, "\""); 
				return str;
			}
		</script>
	</body>
</html>
