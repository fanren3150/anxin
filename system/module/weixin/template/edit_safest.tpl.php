<?php include $this->admin_tpl('header','admin');?>
		<div class="fixed-nav layout">
			<ul>
				<li class="first">保险编辑</li>
				<li class="spacer-gray"></li>
			</ul>
			<div class="hr-gray"></div>
		</div>
		<form action="" method="POST" enctype="multipart/form-data">
		<div class="content padding-big have-fixed-nav">
			<div class="hidden">
				<input type="hidden" name="id" value="<?php echo $info['id']?>" />
				<input type="hidden" name="buyer_id" value="<?php echo $info['buyer_id']?>" />
				<input type="hidden" name="seller_id" value="<?php echo $info['seller_id']?>" />
			</div>
			<div class="form-box clearfix">
				<div class="form-group">
					<span class="label">开始时间：</span>
						<div class="box">
							<?php echo $info['stime'];?>
						</div><p class="desc">保险开始时间</p>
				</div>
				<div class="form-group">
					<span class="label">结束时间：</span>
					<div class="box">
						<?php echo $info['etime'];?>
					</div><p class="desc">保险结束时间</p>
				</div>
				<div class="form-group">
					<span class="label">保险卡区域代码：</span>
						<div class="box">
							<input class="input hd-input" type="text" name="safest_code" value="<?php echo $info['safest_code'];?>" tabindex="0">
						</div>
				</div>
				<div class="form-group">
					<span class="label">保险卡号：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="safest_id" value="<?php echo $info['safest_id'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">保险名称：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="safest_name" value="<?php echo $info['safest_name'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">锁芯型号：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="type" value="<?php echo $info['type'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">用户姓名：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="name" value="<?php echo $info['name'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">用户身份证：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="idcart" value="<?php echo $info['idcart'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">用户手机号：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="phone" value="<?php echo $info['phone'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">用户座机号：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="tel" value="<?php echo $info['tel'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">用户座机号：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="idcart" value="<?php echo $info['idcart'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">原防盗门品牌：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="oldsdtype" value="<?php echo $info['oldsdtype'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">原锁芯型号：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="oldtype" value="<?php echo $info['oldtype'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">所在省：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="provinces" value="<?php echo $info['provinces'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">所在市：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="city" value="<?php echo $info['city'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">所在区县：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="area" value="<?php echo $info['area'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">详细地址：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="address" value="<?php echo $info['address'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">安装公司名称：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="com_name" value="<?php echo $info['com_name'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">固定电话：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="com_tel" value="<?php echo $info['com_tel'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">安装师傅：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="m_name" value="<?php echo $info['m_name'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">安装师傅手机号码：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="m_phone" value="<?php echo $info['m_phone'];?>" tabindex="0">
					</div>
				</div>
				<div class="form-group">
					<span class="label">安装师傅身份证号码：</span>
					<div class="box">
						<input class="input hd-input" type="text" name="m_idcart" value="<?php echo $info['m_idcart'];?>" tabindex="0">
					</div>
				</div>

			</div>


			<div class="padding">
				<input type="submit" class="button bg-main" value="保存" name="dosubmit"/>
				<input type="button" class="button margin-left bg-gray" value="返回" />
			</div>
		</div>
		</form>
	</body>
</html>