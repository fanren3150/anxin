<?php

class wxkeyword_service extends service {
	public function __construct() {
		$this->db = model('misc/article');
        $this->category_db = model('misc/article_category');
	}
	/**
	 * [get_article_by_id 根据id获取图文信息]
	 * @param  [type] $id [查询单条图文id]
	 * @return [type]     [description]
	 */
	public function get_article_by_id($id){
		if((int)$id < 1){
			$this->error = '图文不存在';
			return FALSE;
		}
		$result = $this->db->find($id);
		$result['category_ids'] = $this->get_parent_id($result['category_id']);
		$result['category'] = $this->create_cat_format($result['category_id']);
		if(!$result){
			$this->error = $this->db->getError();
		}
		return $result;
	}
	/**
	 * [get_parent_id 根据分类id获取图文分类信息]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function get_parent_id($id){
    	if($id < 0){
    		$this->error = '图文分类不存在';
    		return FALSE;
    	}
		static $flist = array();
		$row = $this->category_db->where(array('id'=>$id))->find();
		if((int) $row['parent_id'] != 0){
			$classFID  = $row['parent_id'];
			$flist[] = $row['id'];
			$this->get_parent_id($classFID);
		} elseif($row['parent_id'] == 0){
			$flist[] = $row['id'];
		}
		array_push($flist,0);
		$ids = implode(",",$flist);
		return $ids;
	}
	/**
	 * [get_parent 根据分类id获取图文分类信息]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function get_parent($id){
    	if($id < 0){
    		$this->error = '分类不存在';
    		return FALSE;
    	}
		static $flist=array();
		$row = $this->category_db->where(array('id'=>$id))->find();
		if(!$row){
			$this->error = '分类不存在';
			return FALSE;
		}
		if ((int)$row['parent_id'] != 0){
			$classFID  = $row['parent_id'];
			$flist[] = $classFID;
			$this->get_parent($classFID);
		}else{
			$classFID  = $row['parent_id'];
			$flist[] = $classFID;
		}
		if(!empty($flist)){
			return $flist;
		}else{
			$this->error = '该分类没有父级分类';
			return FALSE;
		}
	}
	public function create_cat_format($id){
		$cat_str = '';
		$info = $this->get_parent($id);
		array_push($info,$id);
		$cat_arr = $this->category_db->where(array('id'=>array('IN',$info)))->getField('name',TRUE);
		foreach ($cat_arr as $key => $value) {
			if($key == count($cat_arr)-1){
				$cat_str .= $value;
			}else{
				$cat_str .= $value.' > ';
			}
		}
		return $cat_str;
	}
	/**
	 * [edit 编辑图文]
	 * @param [array] $params [规格信息]
	 * @return [boolean]         [返回ture or false]
	 */
	public function edit($params){
		if((int)$params['id'] < 1){
			$this->error = '图文不存在';
			return FALSE;
		}
		$data = array();
		$data = $params;
		if($params['thumb']){
			$data['thumb'] =  $params['thumb'];
		}
		$result = $this->db->update($data);
    	if($result === false){
			$this->error = $this->db->getError();
    		return FALSE;
    	}else{
    		return TRUE;
    	}
	}
	/**
	 * [delete 删除图文]
	 * @param [array] $params [规格信息]
	 * @return [boolean]         [返回ture or false]
	 */
	public function delete($params){
		if(!$this->is_array_null($params)){
			$this->error = '图文不存在';
			return FALSE;
		}
		$data = array();
		$data['id'] = array('IN', $params['id']);
		if(!$this->delete_img(explode(',',$params['id'][0]))){
			$this->error = '图片删除失败，请手动删除';
			return FALSE;
		}
		$infos = $this->db->where($data)->getField('id,thumb,content',true);
		foreach ($infos AS $info) {
			model('attachment/attachment', 'service')->attachment('', $info['thumb'],false);
			model('attachment/attachment', 'service')->attachment('', $info['content']);
		}
		$result = $this->db->where($data)->delete();
    	if(!$result){
			$this->error = $this->db->getError();
    		return FALSE;
    	}
    	return TRUE;
	}
	/**
	 * [is_array_null 是否传值]
	 * @param [array] $params [传递的数组]
	 * @return [boolean]         [返回ture or false]
	 */
	public function is_array_null($params){
		if($params['id']['0'] == null){
			return FALSE;
		}
		return TRUE;
	}
	/**
	 * [delete_img 删除图文插件下的图片]
	 * @param [array] $params [图文id]
	 * @return [boolean]         [返回ture or false]
	 */
	public function delete_img($params){
		foreach($params as $key => $value){
			$content = $this->db->where(array('id'=>array('eq',$value)))->getField('content');
			//获取图片全路径
			$path = substr($_SERVER[DOCUMENT_ROOT],0,strlen($_SERVER[DOCUMENT_ROOT])-1);
			preg_match_all("/src=('|\")([^'\"]+)('|\")/", $content,$match);
			//组装路径
			$img_path = array_unique($match);
			$img_path = str_replace('src=','',$img_path[0]);
			for($i=0; $i<count($img_path);$i++){
				$new_path = substr($img_path[$i],1);
				$final_path = substr($new_path,0,-1);
				if(!unlink($path.$final_path)){
					return TRUE;
				}
			}
		}
		return TRUE;
	}
	/**
	 * [add 添加图文]
	 * @param [array] $params [图文信息]
	 * @return [type] [description]
	 */
	public function add($params){
		$data = array();
		$data = $params;
		$result = $this->db->update($data);
    	if(!$result){
    		$this->error = $this->db->getError();
    		return FALSE;
    	}else{
    		return TRUE;
    	}
	}
	/**
	 * [ajax_edit 修改图文]
	 * @param  [array] $params [修改的数据]
	 * @return [boolean]     [返回更改结果]
	 */
	public function ajax_edit($params){
		$result = $this->db->update($this->assembl_array($params));
		if(!$result){
    		$this->error = $this->db->getError();
    		return FALSE;
    	}else{
    		return TRUE;
    	}
	}
	/**
	 * [assembl_array 组装数组]
	 * @param  [array] $params [修改的数据]
	 * @return [type] $data     [返回更改结果]
	 */
	public function assembl_array($params){
		if((int)$params['id'] < 1){
			$this->error = '参数错误';
			return FALSE;
		}
		$data = array();
		$data_key = array_keys($params);
		$data_value = array_values($params);
		foreach($data_key as $key => $value){
			$data[$value] = $data_value[$key];
		}
		return $data;
	}
	/**
	 * [get_parent_category 获取多条分类]
	 * @param  [array] $params [图文id和title]
	 * @return [boolean]     [返回更改结果]
	 */
	public function get_parent_category($params){
		$result = $this->category_db->where(array('id'=>array('IN',$params)))->getField('name',TRUE);
		if(!$result){
			$this->error = $this->logic->error;
			return FALSE;
		}
		return $result;
	}
	/**
	* [fieldinc 增加字段值]
	* @param integer $id    id
	* @return [boolean]     [返回更改结果]
	*/
	public function hits($id){
		return $this->db->where(array('id' => $id))->setInc('hits');
	}
}