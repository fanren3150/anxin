<?php

class weixin_service extends service {
	public function __construct() {        
		$this->setting = model('admin/setting','service')->get();
		$this->token = Token;
		$this->AppId = AppId;
		$this->AppSecret = AppSecret;
	}
	
	public function content($poststr) {
		$xmlObj 			= simplexml_load_string($poststr, 'SimpleXMLElement', LIBXML_NOCDATA);
		$this->ToUserName 	= $xmlObj->ToUserName;//接收用户
		$this->FromUserName = $xmlObj->FromUserName;//来源用户
		$CreateTime 		= $xmlObj->CreateTime;
		$MsgType 			= $xmlObj->MsgType;//消息内容 text event location
		$MsgEvent 			= $xmlObj->Event;//按钮类型
		$EventKey 			= $xmlObj->EventKey;//按钮触发关键词
		$Content 			= $xmlObj->Content;//用户发送的关键词
		$log_data['source_wechat'] = $this->mid;		
		$log_data['openid'] = (array)$this->FromUserName;
		$log_data['openid'] = $log_data['openid'][0];		
		$log_data['action'] = (array)$MsgEvent;
		$log_data['action'] = $log_data['action'][0];		
		$log_data['actionType'] = (array)$MsgType;
		$log_data['actionType'] = $log_data['actionType'][0];
		if ($MsgType == 'text') {
			$log_data['content'] = (array)$Content;
			$log_data['content'] = $log_data['content'][0];		
			$log_data['addtime'] = time();
			$this->sendPassiveResponseMessage($Content);
		} elseif ($MsgType == 'event') {
			if ($MsgEvent == 'CLICK') {
				$Content = $xmlObj->EventKey;
				$log_data['content'] = (array)$Content;
				$log_data['content'] = $log_data['content'][0];		
				$log_data['addtime'] = time();			
				$this->sendPassiveResponseMessage($Content);
			}elseif($MsgEvent == 'subscribe'){
				$log_data['content'] = '关注';
				$log_data['addtime'] = time();
				model('wxuser')->where(array("openid"=>$log_data['openid']))->save(array('subscribe'=>1));
				$this->sendPassiveResponseMessage('关注');
			}elseif($MsgEvent == 'unsubscribe'){
				$log_data['content'] = '取消关注';
				$log_data['addtime'] = time();
				model('wxuser')->where(array("openid"=>$log_data['openid']))->save(array('subscribe'=>0));
				$this->sendPassiveResponseMessage('取消关注');
					
			}
		} elseif ($MsgType == 'location') {
			$log_data['content'] = '获取地理位置';
			$log_data['addtime'] = time();
			/*$x = $xmlObj->Location_X;
			$y = $xmlObj->Location_Y;
			$ocityArr=json_decode(file_get_contents('http://apis.map.qq.com/ws/geocoder/v1/?location='.$x.','.$y.'&coord_type=1&get_poi=0&key=RWHBZ-TWF3X-6CO4Y-7OCHR-QXSPK-HWFRO'),1);
			//$ocityArr=json_decode(file_get_contents('http://api.map.baidu.com/geocoder/v2/?ak=C93b5178d7a8ebdb830b9b557abce78b&location='.$x.','.$y.'&output=json&pois=0'),1);
			echo $this->send($this->ToUserName, $this->FromUserName, var_export($ocityArr,true));exit();*/
		}
    }
	
	private function sendPassiveResponseMessage($Content, $data=array()){
		if($Content == 1){
			$openid = (array)$this->FromUserName;
			$openid = $openid[0];		
			$info = $this->get_info1($openid);
			echo $this->send($this->ToUserName, $this->FromUserName, '亲'.$info['nickname'].",".$this->setting['attention_reply']);exit();	
		}elseif($Content == 'kf' || $Content == '客服'){
			echo $this->send2($this->ToUserName, $this->FromUserName,$this->ToUserName);exit();
		}else{
			$artinfo = model('article')->where(array('keywords'=>array('LIKE','%'.$Content.'%')))->order('sort desc')->limit(6)->select();
			if($artinfo){
				$i=0;
				foreach($artinfo as $val){
					$news[$i]['title'] = $val['title'];
					$news[$i]['picurl']	= str_replace("/./","/","http://".$_SERVER['HTTP_HOST'].'/'.$val['thumb']);
					if($val['url']){
							$news[$i]['url'] = $val['url'];
					}else{
							$news[$i]['url'] = "http://".$_SERVER['HTTP_HOST'].url('misc/index/article_detail',array('id'=>$val['id']));
					}
					$i++;
				}
				echo $this->send_pic($this->ToUserName, $this->FromUserName, $news);exit();
			}else{
				if($Content == '关注'){
					$openid = (array)$this->FromUserName;
					$openid = $openid[0];		
					$info = $this->get_info1($openid);
					echo $this->send($this->ToUserName, $this->FromUserName, '亲'.$info['nickname'].",".$this->setting['attention_reply']);exit();	
				}else{
					echo $this->send2($this->ToUserName, $this->FromUserName,$this->ToUserName);exit();
				}
			}
		}
	}
	
	private function send($ToUserName, $FromUserName, $content) {
        $content = $this->matchContent($content);
        $str = "<xml>
				 <ToUserName><![CDATA[%s]]></ToUserName>
				 <FromUserName><![CDATA[%s]]></FromUserName>
				 <CreateTime>%s</CreateTime>
				 <MsgType><![CDATA[text]]></MsgType>
				 <Content><![CDATA[%s]]></Content>
				</xml>";
        return $resultstr = sprintf($str, $FromUserName, $ToUserName, time(), $content);
    }
	
	private function send_pic($ToUserName, $FromUserName, $arr) {
        $str = "<xml>
				 <ToUserName><![CDATA[" . $FromUserName . "]]></ToUserName>
				 <FromUserName><![CDATA[" . $ToUserName . "]]></FromUserName>
				 <CreateTime>" . time() . "</CreateTime>
				 <MsgType><![CDATA[news]]></MsgType>
				 <ArticleCount>" . count($arr) . "</ArticleCount>
				 <Articles>";
		
        foreach ($arr as $k => $v) {
            if ($k == 0) {
                $picurl = $v['picurl'];
            } else {
                $picurl = $v['picurl'];
            }
            $v['url'] = $this->matchLinks($v['url']);
            $str .="
			 <item>
			 <Title><![CDATA[" . $v['title'] . "]]></Title> 
			 <Description><![CDATA[" . $v['description'] . "]]></Description>
			 <PicUrl><![CDATA[" . $picurl . "]]></PicUrl>
			 <Url><![CDATA[" . $v['url'] . "]]></Url>
			 </item>";
        }

        $str .= "</Articles></xml>";

        return $str;
    }
	
	private function matchLinks($linkinfo) {//链接处理
        if (stristr($linkinfo, $_SERVER['SERVER_NAME'])) {
            if (stristr($linkinfo, "?")) {
                $links = $linkinfo . "&openid=" . $this->FromUserName . "&token=" . $this->token;
            } else {
                $links = $linkinfo . "?openid=" . $this->FromUserName . "&token=" . $this->token;
            }
        } else {
            $links = $linkinfo;
        }
        return $links;
    }
	
	private function matchContent($contentStr) {
        $str = $contentStr;
        $reg = '/\shref=[\'\"]([^\'"]*)[\'"]/i';
        preg_match_all($reg, $str, $out_ary); //正则：得到href的地址
        $src_ary = $out_ary[1];
        if (!empty($src_ary)) {//存在
            $comment = $src_ary[0];
            if (stristr($comment, $_SERVER['SERVER_NAME'])) {
                if (stristr($comment, "?")) {
                    $links = $comment . "&openid=" . $this->FromUserName . "&token=" . $this->token;
                    $contentStr = str_replace($comment, $links, $str);
                } else {
                    $links = $comment . "?openid=" . $this->FromUserName . "&token=" . $this->token;
                    $contentStr = str_replace($comment, $links, $str);
                }
            }
        }
        return $contentStr;
    }
	
    public function get_token($code)
    {
    	$ch = curl_init();
    	curl_setopt($ch,CURLOPT_URL,"https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$this->AppId."&secret=".$this->AppSecret."&code=".$code."&grant_type=authorization_code");
        //echo "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$this->AppId."&secret=".$this->AppSecret."&code=".$code."&grant_type=authorization_code";
    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    	curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	$tmpInfo = curl_exec($ch);
    	if (curl_errno($ch)) {
    		echo 'Errno'.curl_error($ch);
    	}
    	curl_close($ch);
    	$arr= json_decode($tmpInfo,true);
        
    	return $arr;
    }
	
	public function get_access_token()
    {
    	$ch = curl_init();
    	curl_setopt($ch,CURLOPT_URL,"https://api.weixin.qq.com/cgi-bin/token?appid=".$this->AppId."&secret=".$this->AppSecret."&grant_type=client_credential");		
    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    	curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	$tmpInfo = curl_exec($ch);
    	if (curl_errno($ch)) {
    		echo 'Errno'.curl_error($ch);
    	}
    	curl_close($ch);
    	$arr= json_decode($tmpInfo,true);
    	return $arr;
    }

 	public function get_info($access_token,$openid){
    	$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,"https://api.weixin.qq.com/sns/userinfo?access_token=".$access_token."&openid=".$openid."&lang=zh_CN");	
    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    	curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	$tmpInfo = curl_exec($ch);
    	if (curl_errno($ch)) {
    		echo 'Errno'.curl_error($ch);
    	}
    	curl_close($ch);
    	$arr= json_decode($tmpInfo,true);
    	return $arr;
    }
	
	public function get_info1($openid){
    	$ch = curl_init();		
		$access_token = $this->get_access_token();
		$access_token = $access_token['access_token'];
		curl_setopt($ch,CURLOPT_URL,"https://api.weixin.qq.com/cgi-bin/user/info?access_token=".$access_token."&openid=".$openid);
    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    	curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	$tmpInfo = curl_exec($ch);
    	if (curl_errno($ch)) {
    		echo 'Errno'.curl_error($ch);
    	}
    	curl_close($ch);
    	$arr= json_decode($tmpInfo,true);
    	return $arr;
    }
	
	public function errInfo($code){	
		
		$err = array(
			-1=>'系统繁忙，此时请开发者稍候再试',
			0=>'请求成功',
			40001=>'获取access_token时AppSecret错误，或者access_token无效。请开发者认真比对AppSecret的正确性，或查看是否正在为恰当的公众号调用接口',
			40002=>'不合法的凭证类型',
			40003=>'不合法的OpenID，请开发者确认OpenID（该用户）是否已关注公众号，或是否是其他公众号的OpenID',
			40004=>'不合法的媒体文件类型',
			40005=>'不合法的文件类型',
			40006=>'不合法的文件大小',
			40007=>'不合法的媒体文件id',
			40008=>'不合法的消息类型',
			40009=>'不合法的图片文件大小',
			40010=>'不合法的语音文件大小',
			40011=>'不合法的视频文件大小',
			40012=>'不合法的缩略图文件大小',
			40013=>'不合法的AppID，请开发者检查AppID的正确性，避免异常字符，注意大小写',
			40014=>'不合法的access_token，请开发者认真比对access_token的有效性（如是否过期），或查看是否正在为恰当的公众号调用接口',
			40015=>'不合法的菜单类型',
			40016=>'主菜单最多只能三个按钮',
			40017=>'子菜单最多只能五个按钮',
			40018=>'不合法的按钮名字长度',
			40019=>'不合法的按钮KEY长度',
			40020=>'不合法的按钮URL长度',
			40021=>'不合法的菜单版本号',
			40022=>'不合法的子菜单级数',
			40023=>'不合法的子菜单按钮个数',
			40024=>'不合法的子菜单按钮类型',
			40025=>'不合法的子菜单按钮名字长度',
			40026=>'不合法的子菜单按钮KEY长度',
			40027=>'不合法的子菜单按钮URL长度',
			40028=>'不合法的自定义菜单使用用户',
			40029=>'不合法的oauth_code',
			40030=>'不合法的refresh_token',
			40031=>'不合法的openid列表',
			40032=>'不合法的openid列表长度',
			40033=>'不合法的请求字符，不能包含\uxxxx格式的字符',
			40035=>'不合法的参数',
			40038=>'不合法的请求格式',
			40039=>'不合法的URL长度',
			40050=>'不合法的分组id',
			40051=>'分组名字不合法',
			40055=>'不合法的URL',
			40117=>'分组名字不合法',
			40118=>'media_id大小不合法',
			40119=>'button类型错误',
			40120=>'button类型错误',
			40121=>'不合法的media_id类型',
			40132=>'微信号不合法',
			40137=>'不支持的图片格式',
			41001=>'缺少access_token参数',
			41002=>'缺少appid参数',
			41003=>'缺少refresh_token参数',
			41004=>'缺少secret参数',
			41005=>'缺少多媒体文件数据',
			41006=>'缺少media_id参数',
			41007=>'缺少子菜单数据',
			41008=>'缺少oauth code',
			41009=>'缺少openid',
			42001=>'access_token超时，请检查access_token的有效期，请参考基础支持-获取access_token中，对access_token的详细机制说明',
			42002=>'refresh_token超时',
			42003=>'oauth_code超时',
			42007=>'用户修改微信密码，accesstoken和refreshtoken失效，需要重新授权',
			43001=>'需要GET请求',
			43002=>'需要POST请求',
			43003=>'需要HTTPS请求',
			43004=>'需要接收者关注',
			43005=>'需要好友关系',
			44001=>'多媒体文件为空',
			44002=>'POST的数据包为空',
			44003=>'图文消息内容为空',
			44004=>'文本消息内容为空',
			45001=>'多媒体文件大小超过限制',
			45002=>'消息内容超过限制',
			45003=>'标题字段超过限制',
			45004=>'描述字段超过限制',
			45005=>'链接字段超过限制',
			45006=>'图片链接字段超过限制',
			45007=>'语音播放时间超过限制',
			45008=>'图文消息超过限制',
			45009=>'接口调用超过限制',
			45010=>'创建菜单个数超过限制',
			45015=>'回复时间超过限制',
			45016=>'系统分组，不允许修改',
			45017=>'分组名字过长',
			45018=>'分组数量超过上限',
			45047=>'客服接口下行条数超过上限',
			46001=>'不存在媒体数据',
			46002=>'不存在的菜单版本',
			46003=>'不存在的菜单数据',
			46004=>'不存在的用户',
			47001=>'解析JSON/XML内容错误',
			48001=>'api功能未授权，请确认公众号已获得该接口，可以在公众平台官网-开发者中心页中查看接口权限',
			48004=>'api接口被封禁，请登录mp.weixin.qq.com查看详情',
			50001=>'用户未授权该api',
			50002=>'用户受限，可能是违规后接口被封禁',
			61451=>'参数错误(invalid parameter)',
			61452=>'无效客服账号(invalid kf_account)',
			61453=>'客服帐号已存在(kf_account exsited)',
			61454=>'客服帐号名长度超过限制(仅允许10个英文字符，不包括@及@后的公众号的微信号)(invalid kf_acount length)',
			61455=>'客服帐号名包含非法字符(仅允许英文+数字)(illegal character in kf_account)',
			61456=>'客服帐号个数超过限制(10个客服账号)(kf_account count exceeded)',
			61457=>'无效头像文件类型(invalid file type)',
			61450=>'系统错误(system error)',
			61500=>'日期格式错误',
			65301=>'不存在此menuid对应的个性化菜单',
			65302=>'没有相应的用户',
			65303=>'没有默认菜单，不能创建个性化菜单',
			65304=>'MatchRule信息为空',
			65305=>'个性化菜单数量受限',
			65306=>'不支持个性化菜单的帐号',
			65307=>'个性化菜单信息为空',
			65308=>'包含没有响应类型的button',
			65309=>'个性化菜单开关处于关闭状态',
			65310=>'填写了省份或城市信息，国家信息不能为空',
			65311=>'填写了城市信息，省份信息不能为空',
			65312=>'不合法的国家信息',
			65313=>'不合法的省份信息',
			65314=>'不合法的城市信息',
			65316=>'该公众号的菜单设置了过多的域名外跳（最多跳转到3个域名的链接）',
			65317=>'不合法的URL',
			9001001=>'POST数据参数不合法',
			9001002=>'远端服务不可用',
			9001003=>'Ticket不合法',
			9001004=>'获取摇周边用户信息失败',
			9001005=>'获取商户信息失败',
			9001006=>'获取OpenID失败',
			9001007=>'上传文件缺失',
			9001008=>'上传素材的文件类型不合法',
			9001009=>'上传素材的文件尺寸不合法',
			9001010=>'上传失败',
			9001020=>'帐号不合法',
			9001021=>'已有设备激活率低于50%，不能新增设备',
			9001022=>'设备申请数不合法，必须为大于0的数字',
			9001023=>'已存在审核中的设备ID申请',
			9001024=>'一次查询设备ID数量不能超过50',
			9001025=>'设备ID不合法',
			9001026=>'页面ID不合法',
			9001027=>'页面参数不合法',
			9001028=>'一次删除页面ID数量不能超过10',
			9001029=>'页面已应用在设备中，请先解除应用关系再删除',
			9001030=>'一次查询页面ID数量不能超过50',
			9001031=>'时间区间不合法',
			9001032=>'保存设备与页面的绑定关系参数错误',
			9001033=>'门店ID不合法',
			9001034=>'设备备注信息过长',
			9001035=>'设备申请参数不合法',
			9001036=>'查询起始值begin不合法'
		);		
		return $err[$code];
    }
	
    private function send2($ToUserName, $FromUserName, $content) {
        //$content = $this->matchContent($content);
        $str = "<xml>
				 <ToUserName><![CDATA[%s]]></ToUserName>
				 <FromUserName><![CDATA[%s]]></FromUserName>
				 <CreateTime>%s</CreateTime>
				 <MsgType><![CDATA[transfer_customer_service]]></MsgType>
				 <Content><![CDATA[%s]]></Content>
				</xml>";
        return $resultstr = sprintf($str, $FromUserName, $ToUserName, time(), $content);
    }
}
	