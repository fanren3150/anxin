<?php
/**
 * 		图文公共函数
 */
	/**
	 * [crumbs 图文模块下的面包屑导航]
	 * @param  [type] $id  [description]
	 * @param  [type] $type  [图文列表]
	 * @return [type]         [description]
	 */
	function crumbs($id,$type) {
		$symbol = " > ";
		if($type){
			$category = model('misc/article_category','service')->get_category_by_id($id);
			$category_id = array_reverse(array_unique($category['category_id']));
			$category['category'] = explode(">",$category['parent']);
		}else{
			$category = model('misc/article','service')->get_article_by_id($id);
			$category_id = array_reverse(array_unique($category['category_ids']));
			$category['category'] = explode(">",$category['category']);
		}
		foreach($category['category'] as $k => $v){
			$url = url('misc/index/article_lists', array('category_id' =>$category_id[$k]));
			$pos .= "<a href=".$url.">".$v."</a><em>".$symbol."</em>";
		}
		$pos = substr($pos,0,strlen($pos)-8);
		return $pos;
	}
	
	function ch_status($ident) {
	$arr = array(
			'cancel'        => '已取消',
			'recycle'       => '已回收',
			'delete'        => '已删除',
			'create'        => '创建订单',
			'load_pay'      => '待付款',
			'pay'           => '已付款',
			'load_confirm'  => '待接单',
			'load_delivery' => '待到达',
			'load_server'   => '服务中',
			'load_finish'   => '待完成',
			'all_finish'    => '已完成',
			'receive'       => '已收货',
			'nocancel'      => '无商户匹配，已取消',
			// 前台时间轴
			'time_cancel'   => '取消订单',
			'time_recycle'  => '回收订单',
			'time_create'   => '提交订单',
			'time_pay'      => '确认付款',
			'time_confirm'  => '确认订单',
			'time_delivery' => '商品发货',
			'time_finish'   => '确认收货',
		);
	return $arr[$ident];
}