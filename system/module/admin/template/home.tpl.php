<?php include template('header','admin');?>
	
		<div class="fixed-nav layout">
			<ul>
				<li class="first">后台首页</li>
				<li class="spacer-gray"></li>
			</ul>
			<div class="hr-gray"></div>
		</div>
		<div class="content padding-big have-fixed-nav">
			<?php if(!empty($product_notify)):?>
			<div class="warn-info border bg-white margin-top padding-lr">
				<i class="warn-info-ico ico_warn margin-right"></i>
				<div id="FontScroll" style="height: 40px;overflow: hidden;">
					<ul>
						<?php foreach($product_notify as $k =>$v):?>
							<li><?php echo $v?></li>
						<?php endforeach;?>
					</ul>
				</div>
				<a href="javascript:;" class="close text-large fr" style="margin-top:-40px ;">×</a>
			</div>
			<?php endif;?>
			<div class="margin-top">
				<div class="padding-small-left">
					
					<table cellpadding="0" cellspacing="0" class="margin-top border bg-white layout">
						<tbody>
							<tr class="bg-gray-white line-height-40 border-bottom">
								<th class="text-left padding-big-left">系统信息</th>
							</tr>
							<tr class="border-bottom">
								<td class="text-left today-sales padding-big padding-small-top padding-small-bottom line-height-40">
									<span class="fl">系统版本</span>
									<span class="fr"><?php if(HD_BRANCH == 'stable'){?>稳定版&nbsp;<?php }else{?>开发版&nbsp;<?php }?>v<?php echo HD_VERSION ?></span>
								</td>
							</tr>
							<tr class="border-bottom">
								<td class="text-left today-sales padding-big padding-small-top padding-small-bottom line-height-40">
									<span class="fl">服务器系统及PHP</span>
									<span class="fr"><?php echo php_uname('s');?>/<?php echo  PHP_VERSION;?></span>
								</td>
							</tr>
							<tr class="border-bottom">
								<td class="text-left today-sales padding-big padding-small-top padding-small-bottom line-height-40">
									<span class="fl">服务器软件</span>
									<span class="fr"><?php echo php_uname('s');?></span>
								</td>
							</tr>
							<tr class="border-bottom">
								<td class="text-left today-sales padding-big padding-small-top padding-small-bottom line-height-40">
									<span class="fl">数据库信息</span>
									<span class="fr">MySQL&nbsp;<?php echo version_compare(phpversion(), '7.0.0') > -1 ? mysqli_get_server_info() : mysql_get_server_info();?>/数据库大小&nbsp;<em data-id="dbsize">0</em>M</span>
								</td>
							</tr>
						</tbody>
					</table>
                    
				</div>
			</div>
		</div>
<?php include template('footer','admin');?>
<script>
	$('#FontScroll').FontScroll({time: 3000,num: 1});
</script>
