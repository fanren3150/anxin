<?php include template('header','admin');?>
<body>
	<div class="fixed-nav layout">
		<ul>
			<li class="first">托班圈详情</li>
			<li class="spacer-gray"></li>
		</ul>
		<div class="hr-gray"></div>
	</div>
	<div class="content padding-big have-fixed-nav">
		
		<!--订单详情-->
		<table cellpadding="0" cellspacing="0" class="border bg-white layout margin-top">
			<tbody>
				<tr class="bg-gray-white line-height-40 border-bottom">
					<th class="text-left padding-big-left">文字内容</th>
				</tr>
				<tr class="border">
					<td class="padding-big-left padding-big-right">
						<table cellpadding="0" cellspacing="0" class="layout">
							<tbody>
								<tr class="line-height-40">
									<td class="text-left"><?php echo $info['title']; ?></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
                <tr class="bg-gray-white line-height-40 border-bottom">
					<th class="text-left padding-big-left">图片内容</th>
				</tr>
				<tr class="border">
					<td class="padding-big-left padding-big-right">
						<table cellpadding="0" cellspacing="0" class="layout">
							<tbody>
								<tr class="line-height-40">
									<th class="text-left" colspan="3"><br>
										<?php foreach ($info['material'] as $k => $v) : ?>
                                        <a href="<?php echo $v;?>" target="_blank"><img src="<?php echo $v;?>" width="80" height="80"></a>&nbsp;
										<?php endforeach; ?>
									</th>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		
		<table cellpadding="0" cellspacing="0" class="border bg-white layout margin-top">
			<tbody>
				<tr class="bg-gray-white line-height-40 border-bottom">
					<th class="text-left padding-big-left">点赞</th>
					<th class="text-right padding-big-right"></th>
				</tr>
				<tr class="border">
					<td class="padding-big-left padding-big-right">
						<table cellpadding="0" cellspacing="0" class="layout">
							<tbody>
                            	<?php foreach($info['like'] as $kk => $vv){?>							
								<tr class="line-height-40">
									<td class="text-left w25"><?php echo $vv['truename'];?></td>
									<td class="text-left w50"><a href="<?php echo url("cdelete",array("id" => $vv['id'],"cid" => $info['id'])); ?>">删除</a></td>
								</tr>
                                <?php }?>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		
        <table cellpadding="0" cellspacing="0" class="border bg-white layout margin-top">
			<tbody>
				<tr class="bg-gray-white line-height-40 border-bottom">
					<th class="text-left padding-big-left">评论</th>
					<th class="text-right padding-big-right"></th>
				</tr>
				<tr class="border">
					<td class="padding-big-left padding-big-right">
						<table cellpadding="0" cellspacing="0" class="layout">
							<tbody>
                            	<?php foreach($info['comments'] as $kk => $vv){?>							
								<tr class="line-height-40">
									<td class="text-left w25"><?php if($vv['rid'] > 0){?>
								<span><?php echo $vv['truename'];?></span>回复<span><?php echo $vv['rtruename'];?>:</span>
								<?php }else{?>
								<span><?php echo $vv['truename'];?>:</span>
								<?php }?>
								<?php echo $vv['content'];?></td>
									<td class="text-left w50"><a href="<?php echo url("cdelete",array("id" => $vv['id'],"cid" => $info['id'])); ?>">删除</a></td>
								</tr>
                                <?php }?>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
		<div class="padding-tb">
			<input class="button margin-left bg-gray border-none" type="button" value="返回" />
		</div>
	</div>
<?php include template('footer','admin');?>
<script>
$('.table').resizableColumns();

$(".look-log").live('click',function(){
	if($(this).hasClass('bg-gray')) return false;
	$(this).removeClass('bg-sub').addClass('bg-gray').html("加载中...");
	var $this = $(this);
	var txt = '';
	$.getJSON('<?php echo url("order/cart/get_delivery_log") ?>', {o_d_id: $(this).attr('data-did')}, function(ret) {
		if (ret.status == 0) {
			alert(ret.message);
			return false;
		}
		if (ret.result.logs.length > 0) {
			$.each(ret.result.logs,function(k, v) {
				txt += '<p>'+ v.add_time +'&nbsp;&nbsp;&nbsp;&nbsp;'+ v.msg +'</p>';
			});
			top.dialog({
				content: '<div class="logistics-info padding-big bg-white text-small"><p class="border-bottom border-dotted padding-small-bottom margin-small-bottom"><span class="margin-big-right">物流公司：'+ret.result.delivery_name+'</span>&nbsp;&nbsp;物流单号：'+ret.result.delivery_sn+'</p>'+ txt +'</div>',
				title: '查看物流信息',
				width: 680,
				okValue: '确定',
				ok: function(){
					$this.removeClass('bg-gray').addClass('bg-sub').html("查看物流");
				},
				onclose: function(){
					$this.removeClass('bg-gray').addClass('bg-sub').html("查看物流");
				}
			})
			.showModal();
		}
	});
})
</script>
