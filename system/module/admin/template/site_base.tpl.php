<?php include template('header','admin');?>
<div class="fixed-nav layout">
    <ul>
        <li class="first">站点设置<a id="addHome" title="添加到首页快捷菜单">[+]</a></li>
        <li class="spacer-gray"></li>
        <!--li class="fixed-nav-tab"><a class="current" href="javascript:;">站点信息</a></li-->
        <li class="fixed-nav-tab"><a class="current" href="javascript:;">基本设置</a></li>
        <li class="fixed-nav-tab"><a href="javascript:;">微信设置</a></li>
		<!--li class="fixed-nav-tab"><a href="javascript:;">退货设置</a></li>
		<li class="fixed-nav-tab"><a href="javascript:;">快递设置</a></li-->
    </ul>
    <div class="hr-gray"></div>
</div>
<form action="" method="POST" enctype="multipart/form-data">
<div class="content padding-big have-fixed-nav">
    <div class="content-tabs">
        <div class="form-box clearfix">
        	<?php echo form::input('text', 'site_name', $setting['site_name'], '站点名称：', '站点名称，将显示在浏览器窗口标题等位置'); ?>
			<?php echo form::input('file', 'site_logo',$setting['site_logo'], '网站LOGO：','网站LOGO图片，此图不与会员中心LOGO共用');?>
        	<?php echo form::input('select', 'timeoffset',$setting['timeoffset'], '系统默认时区：', '当地时间与 GMT 的时差', array('items' => array("-12"=>"(GMT -12:00) 埃尼威托克岛, 夸贾林..","-11"=>"(GMT -11:00) 中途岛, 萨摩亚群岛..","-10"=>"(GMT -10:00) 夏威夷","-9"=>"(GMT -09:00) 阿拉斯加","-8"=>"(GMT -08:00) 太平洋时间(美国和加拿..","-7"=>"(GMT -07:00) 山区时间(美国和加拿大..","-6"=>"(GMT -06:00) 中部时间(美国和加拿大..","-5"=>"(GMT -05:00) 东部时间(美国和加拿大..","-4"=>"(GMT -04:00) 大西洋时间(加拿大), ..","-3.5"=>"(GMT -03:30) 纽芬兰","-3"=>"(GMT -03:00) 巴西利亚, 布宜诺斯艾..","-2"=>"(GMT -02:00) 中大西洋, 阿森松群岛,..","-1"=>"(GMT -01:00) 亚速群岛, 佛得角群岛 ..","0"=>"(GMT) 卡萨布兰卡, 都柏林, 爱丁堡, ..","1"=>"(GMT +01:00) 柏林, 布鲁塞尔, 哥本..","2"=>"(GMT +02:00) 赫尔辛基, 加里宁格勒,..","3"=>"(GMT +03:00) 巴格达, 利雅得, 莫斯..","3.5"=>"(GMT +03:30) 德黑兰","4"=>"(GMT +04:00) 阿布扎比, 巴库, 马斯..","4.5"=>"(GMT +04:30) 坎布尔","5"=>"(GMT +05:00) 叶卡特琳堡, 伊斯兰堡,..","5.5"=>"(GMT +05:30) 孟买, 加尔各答, 马德..","5.75"=>"(GMT +05:45) 加德满都","6"=>"(GMT +06:00) 阿拉木图, 科伦坡, 达..","6.5"=>"(GMT +06:30) 仰光","7"=>"(GMT +07:00) 曼谷, 河内, 雅加达..","8"=>"(GMT +08:00) 北京, 香港, 帕斯, 新..","9"=>"(GMT +09:00) 大阪, 札幌, 首尔, 东..","9.5"=>"(GMT +09:30) 阿德莱德, 达尔文..","10"=>"(GMT +10:00) 堪培拉, 关岛, 墨尔本,..","11"=>"(GMT +11:00) 马加丹, 新喀里多尼亚,..","12"=>"(GMT +12:00) 奥克兰, 惠灵顿, 斐济,.."))); ?>
			<?php echo form::input('text', 'classNumber', $setting['classNumber'], '开班人数：', '发起托班时，满足人数开班成功，否则失败');?>
			<?php echo form::input('radio', 'balance_pay', $setting['balance_pay'], '是否开启余额支付功能：', '是否开启余额支付功能，开启后请指定支持的余额充值支付方式', array('items' => array('1'=>'开启', '0'=>'关闭'), 'colspan' => 2,)); ?>
            <?php echo form::input('select', 'pay_type',$setting['pay_type'], '支付类型设置：', '请选择结算时支持的付款类型，默认为支持"在线支付"
', array('items' => array(2 => '仅在线支付'))); ?>
			<?php echo form::input('checkbox', 'pays[]', implode(',', $setting['pays']), '请选择支持的支付方式：', '设置在线支付时所支持的支付方式，需先在支付平台设置开启支付方式', array('items' => $payment)); ?>
			
			<?php echo form::input('file', 'reg_banner',$setting['reg_banner'], '注册banner：','在注册页面显示的banner图');?>
            <?php echo form::input('file', 'circle_banner',$setting['circle_banner'], '托班圈banner：','在托班圈页面显示的banner图');?>
            <?php echo form::input('file', 'qrcode_bgimg',$setting['qrcode_bgimg'], '海报背景图：','在我的推广海报页面显示的背景图');?>
        </div>
    </div>
    <div class="content-tabs hidden">
        <div class="form-box clearfix" id="buy">
        	<?php echo form::input('text', 'wxUrl', $setting['wxUrl'], '服务器地址(URL)：', '域名+/index.php?m=weixin&c=weixin&a=index'); ?>
			<?php echo form::input('text', 'Token', $setting['Token'], 'Token：', ''); ?>
			<?php echo form::input('text', 'AppId', $setting['AppId'], 'AppId：', ''); ?>
			<?php echo form::input('text', 'AppSecret', $setting['AppSecret'], 'AppSecret：', ''); ?>
			<?php echo form::input('textarea', 'attention_reply', $setting['attention_reply'], '关注回复:','关注公众号自动回复语'); ?>
        </div>
    </div>
	
    
    <div class="padding">
        <input type="submit" class="button bg-main" value="保存" />
    </div>
</div>
</form>
<?php include template('footer','admin');?>

<script>
	$(function(){
		var $val=$("input[type=text]").first().val();
		$("input[type=text]").first().focus().val($val);
		/* 支付方式 */
		if ($("input[name=pay_type]").val() == '1' || $("input[name=pay_type]").val() == '2') {
			$("input[name=pay_type]").parents('.form-group').next().show();
		} else {
			$("input[name=pay_type]").parents('.form-group').next().hide();
		}
		$("input[name=pay_type]").change(function() {
			if ($(this).val() == '1' || $(this).val() == '2') {
				$("input[name=pay_type]").parents('.form-group').next().show();
			} else {
				$("input[name=pay_type]").parents('.form-group').next().hide();
			}
		});

		$.each($("input[name=site_isclosed]"),function(){
			if($(this).attr('checked') == 'checked'){
				if($(this).val() == 0){
					$(this).parents(".form-group").next().show();
				}else{
					$(this).parents(".form-group").next().hide();
				}
			}
		})
		$.each($("input[name=balance_pay]"),function(){
			if($(this).attr('checked') == 'checked'){
				if($(this).val() == 0){
					$(this).parents(".form-group").next().hide();
				}else{
					$(this).parents(".form-group").next().show();
				}
			}
		})
		$.each($("input[name=invoice_enabled]"),function(){
			if($(this).attr('checked') == 'checked'){
				if($(this).val() == 0){
					$(this).parents(".form-box").next().hide();
				}else{
					$(this).parents(".form-box").next().show();
				}
			}
		})
	})
	$("input[name=site_isclosed]").live('click',function(){
		if($(this).val() == 0){
			$(this).parents(".form-group").next().slideDown(100);
		}else{
			$(this).parents(".form-group").next().slideUp(100);
		}
	})
	$("input[name=balance_pay]").live('click',function(){
		if($(this).val() == 0){
			$(this).parents(".form-group").next().slideUp(100);
		}else{
			$(this).parents(".form-group").next().slideDown(100);
		}
	})
	$("input[name=invoice_enabled]").live('click',function(){
		if($(this).val() == 0){
			$(this).parents(".form-box").next().slideUp(100);
		}else{
			$(this).parents(".form-box").next().slideDown(100);
		}
	})
</script>
