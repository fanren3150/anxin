<?php
hd_core::load_class('init', 'admin');
class circle_control extends init_control {
	protected $service = '';
	protected $brand;
	public function _initialize() {
		parent::_initialize();
		$this->service = $this->load->service('school/circle');
		$this->commentService = $this->load->service('school/circle_comment');
		$this->attachment_service = $this->load->service('attachment/attachment');
		helper('attachment');
	}
	
	public function index(){
		$sqlmap = $info = array();
		$limit = (isset($_GET['limit']) && is_numeric($_GET['limit'])) ? $_GET['limit'] : 20;
		$circles = $this->service->get_lists('',$_GET['page'],$limit);
		
		$count = $this->service->count($sqlmap);
		$pages = $this->admin_pages($count, $limit);
		$lists = array(
			'th' => array('title' => array('title' => '内容','length' => 20,'style' => 'content'),'comments' => array('title' => '评论量','length' => 10,'style' => 'like'),'like' => array('title' => '点赞量','length' => 10,'style' => 'like'),'user' => array('title' => '发布者','length' => 15,'style' => 'user'),'recommend'=> array('title' => '推荐','length' => 10,'style' => 'ico_up_rack'),'addtime' => array('length' => 15,'title' => '发布时间')),
			'lists' => $circles,
			'pages' => $pages,
		);
		$this->load->librarys('View')->assign('lists',$lists)->display('circle_list');
	}
	
	public function view(){
		$info = $this->service->find(array('id'=>$_GET['id']));
		$this->load->librarys('View')->assign('info',$info)->display('circle_view');		
	}
	
	public function cdelete(){
		$result = $this->commentService->delete_by_id(intval($_GET['id']));
		if(!$result){
			showmessage($this->service->error);
		}else{
			showmessage(lang('_operation_success_'),url('view',array('id'=>intval($_GET['cid']))),1);
		}
	}
	/**
	 * [ajax_del 删除品牌，可批量删除]
	 */
	public function ajax_del(){
		$result = $this->service->delete_by_id($_GET['id']);
		if(!$result){
			showmessage($this->service->error);
		}else{
			showmessage(lang('_operation_success_'),url('index'),1);
		}
	}
	
	public function ajax_status() {
		$id = $_GET['id'];
		if(empty($_GET['formhash']) || $_GET['formhash'] != FORMHASH) showmessage('_token_error_');
		if ($this->service->change_status($id)) {
			showmessage(lang('_status_success_','ads/language'), '', 1);
		} else {
			showmessage(lang('_status_error_','ads/language'), '', 0);
		}
	}
}