var class_action = (function() {

	// 参数(tpl_url:必填)、宽度(可选) 
	function alert_tpl(data , width) {
		width = parseInt(width) > 0 ? width : 280;
		top.dialog({
			url: data.tpl_url,
			title: '加载中...',
			width: width,
			data : data,
			onclose:function(){
				if(this.returnValue.status == 1) {
					window.location.href= this.returnValue.referer;
				}
			}
		}).showModal();
	};
	
	return {

		classes:function(tpl_url) {
			var chk_value =[];//定义一个数组         
			$('input[name="id"]:checked').each(function(){//遍历每一个名字为interest的复选框，其中选中的执行函数
				chk_value.push($(this).val());//将选中的值添加到数组chk_value中         
			}); 
			if (chk_value.length == 0) { 
				alert("请选择您要操作的数据！"); 
				return ; 
			} 

			var ids = chk_value.toString();
			var param = {
				tpl_url : tpl_url+"&ids="+ids,
				ids : ids,
				action:'pay'
			};
			alert_tpl(param);
		},
		
		classes1:function(tpl_url,ids) {
			var param = {
				tpl_url : tpl_url+"&ids="+ids,
				ids : ids,
				action:'pay'
			};
			alert_tpl(param);
		},
		/* 订单操作 */
		order : function(status , tpl_url) {
			var param = {
				tpl_url : tpl_url,
				sub_sn : order.sub_sn,
				status:status,
				action:'order'
			};
			if (status==4) {	// 删除订单
				$.post(tpl_url ,param ,function(ret) {
					alert(ret.message);
					if (ret.status != 1) return false;
					window.location.href = ret.referer;
				},'json');
				return false;
			} else {
				alert_tpl(param);
			}
		},

		
		
		// 初始化
		init:function() {
			
		}
	};
})();